/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.sample;

import com.novell.cloud.operation.CreateRepository;
import com.novell.cloud.operation.DeleteRepository;
import com.novell.cloud.operation.Login;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.GridException;

public class Repository {
	private ClientAgent client = null;
	
	public boolean login(){
		String xmldata = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<function name=\"Login\">" +
				"<accountName>root</accountName>" +
				"<accountPasswd>novell</accountPasswd>" +
				"<serverAddress>127.0.0.1</serverAddress>" +
			    "<serverPort>8100</serverPort>" +
				"</function>";
		Login op = new Login();
		try{
			String result = op.execute(xmldata).trim();
			//System.out.println(result);
			if(util.isSuccess(result)){
				client = op.getClient();
				return true;
			}
			return false;
				
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return false;
	}
	
	public boolean createRepository(){
		String xmldata = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
		"<function name=\"CreateRepository\">" +
		"<repName>repo001</repName>" +
	    "<description>rep001</description>" +
	    "<localDiskVG>false</localDiskVG>" +
	    "<supportHA>true</supportHA>" +
		"</function>";
		
		CreateRepository op = new CreateRepository(client);
		try{
			String result = op.execute(xmldata).trim();
			if(util.isSuccess(result)){
				return true;
			}
			return false;
		}catch(Exception ex){
			ex.printStackTrace();
		}		
		return false;
	}
	
	public boolean removeRepository(){
		String xmldata = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
		"<function name=\"DeleteRepository\">" +
		"<repName>repo001</repName>" +
		"</function>";
		
		DeleteRepository op = new DeleteRepository(client);
		try{
			String result = op.execute(xmldata).trim();
			if(util.isSuccess(result)){
				return true;
			}
			return false;
		}catch(Exception ex){
			ex.printStackTrace();
		}		
		return false;
	}
	
	public boolean logout(){
		if(null != client){
			try {
				client.logoutAndDestroyCredential();
				return true;
			} catch (GridException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}
	
	public static void main(String[] args) {
		
		// login into the pso server and get the ClientAgent
		Repository rep = new Repository();
		boolean result = rep.login();
		if(!result){
			System.err.println("Fail to login into pso server");
			System.exit(1);
			return;
		}
		System.out.println("Success to login into pso server");
		
		// create a repo
		result = rep.createRepository();
		if(!result){
			System.err.println("Fail to create repository");
			System.exit(1);
			return;
		}
		System.out.println("Success to create repository");
		
		// remove a repo
		result = rep.removeRepository();
		if(!result){
			System.err.println("Fail to remove repository");
			System.exit(1);
			return;
		}
		System.out.println("Success to remove repository");
		
		// logout the pso server
		result = rep.logout();
		if(result){
			System.out.println("Success to logout pso server");
		}
		
		System.exit(0);
		
	}

}
