/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */
package com.novell.cloud.sample;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.XMLHelper;

public class util {
	public static boolean isSuccess(String xmldata){
		try{
			Element root = XMLHelper.GetXmlRootElement(xmldata);
			//System.out.println("root element: " + root.getNodeName());
			NodeList nodes0 = root.getChildNodes();
            for (int i = 0; i < nodes0.getLength(); i++) {
                 Node node0 = nodes0.item(i);
                 if (node0.getNodeType() == Node.ELEMENT_NODE) {
                	 //System.out.println("nodename: " + node0.getNodeName());
                	 if (node0.getNodeName().equals("result")){
                		 NodeList nodes1 = node0.getChildNodes();
                         String txt="";
                         for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                              Node node1 = nodes1.item(i1);
                              if (node1.getNodeType() == Node.TEXT_NODE) {
                              txt = txt + node1.getNodeValue();
                              }
                         }
                         //System.out.println("result value:"+ txt.trim());
                         int result = Integer.parseInt(txt.trim());
                         if( result == 0){
                        	 return true;
                         }
                         return false;
                	 }
                 }
            }
	        System.err.println("not result element found");
		}catch(Exception ex){
			ex.printStackTrace();
		}		
		return false;
	}
}
