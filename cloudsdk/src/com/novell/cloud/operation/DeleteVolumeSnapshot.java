/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Arrays;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactSet;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.GridObjectNotFoundException;
import com.novell.zos.grid.WorkflowInfo;

public class DeleteVolumeSnapshot extends Op {

    private String snapshotID = null;
    private String snapshotPath = null;

	public DeleteVolumeSnapshot() {
		super("DeleteVolumeSnapshot", "delete a snapshot of a disk", 62);
	}

	public DeleteVolumeSnapshot(ClientAgent client) {
		super(client,"DeleteVolumeSnapshot", "delete a snapshot of a disk", 62);
	}

	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);

            if (root.getAttributes().getLength()>0) {
                NodeList nodes0 = root.getChildNodes();
                for (int i = 0; i < nodes0.getLength(); i++) {
                     Node node0 = nodes0.item(i);
                     if (node0.getNodeType() == Node.ELEMENT_NODE) {
                        if (node0.getNodeName().equals("sessionID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              session = txt.trim();
                        } else if (node0.getNodeName().equals("snapshot")){
                            NodeList nodes1 = node0.getChildNodes();
                            for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                Node node1 = nodes1.item(i1);
                                if (node1.getNodeType() == Node.ELEMENT_NODE) {
                                    if (node1.getNodeName().equals("snapshotID")) {
                                        NodeList nodes2 = node1.getChildNodes();
                                        String txt="";
                                        for (int i2 = 0; i2 < nodes2.getLength(); i2++) {
                                            Node node2 = nodes2.item(i2);
                                            if (node2.getNodeType() == Node.TEXT_NODE) {
                                                txt = txt + node2.getNodeValue();
                                            }
                                        }
                                        snapshotID = txt.trim();
                                    } else if (node1.getNodeName().equals("snapshotPath")){
                                        NodeList nodes2 = node1.getChildNodes();
                                        String txt="";
                                        for (int i2 = 0; i2 < nodes2.getLength(); i2++) {
                                            Node node2 = nodes2.item(i2);
                                            if (node2.getNodeType() == Node.TEXT_NODE) {
                                                txt = txt + node2.getNodeValue();
                                            }
                                        }
                                        snapshotPath = txt.trim();
                                    }
                                }
                            }
                        }
                     }
                }
            }
		return 0;
	}

	protected int checkParameters() throws Exception {

        if (IsNull(snapshotID)){
            throw new Exception( "Parameter error: snapshotID can't be empty.");
        }

        if (IsNull(snapshotPath)){
            throw new Exception("Parameter error: snapshotPath can't be empty.");
        }
        
        return 0;
	}
    
	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

        String jobName = "volumemanager";
        if( snapshotPath.startsWith("datastore://") 
            || snapshotPath.startsWith("/vmfs/volumes/") 
            || (snapshotPath.startsWith("[") && snapshotPath.indexOf("]")>0))
            jobName = "volumemanager_vs";
		JobSpec job = this.getJob(jobName);
        if (job == null){
            return throwError(errNo(3),"Job '" + jobName +"' is not deployed.");
        }
        
        Map<String,Object> params = new HashMap<String,Object>();
        Map<String,String> snapshot = new HashMap<String,String>();
        snapshot.put("snapshotID", snapshotID);
        snapshot.put("snapshotPath", snapshotPath);
        params.put("snapshot", snapshot);

		JobDef jobDef = new JobDef();
		jobDef.setName(jobName);
		jobDef.setJob(job);
		jobDef.resetImports();
        jobDef.setImport("jobargs.action", "removesnapshot");
        jobDef.setImport("jobargs.params", params);
		jobDef.setWhatComplete(true);
		jobDef.setWaitForCompletion(true);

		RunJob runJob = new RunJob(getClient());
		try {
			if (!runJob.run(jobDef)) {
				return throwError(errNo(4), "Error running job '" + jobName
						+ "'. \nJob Log:\n" + getJobLog(runJob.getJobId()));
			}
		} catch (Exception ex) {
			return throwError(errNo(5), ex.getMessage());
		}

        StringBuffer sb = new StringBuffer(XML_HEADER);
        sb.append("<results>\n");
        sb.append("    <result>" + 0 +"</result>\n");
        sb.append("</results>\n");
        
        return sb.toString();
	}
}
