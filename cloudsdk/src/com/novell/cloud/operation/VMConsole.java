/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */
package com.novell.cloud.operation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Arrays;
import java.util.Scanner;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.GridObjectNotFoundException;

public class VMConsole extends Op {

    private String instanceID = null;
    private GridObjectInfo vmNode= null;

	public VMConsole() {
		super("VMConsole", "Create a new instance", 25);
	}

	public VMConsole(ClientAgent client) {
		super(client,"VMConsole", "Create a new instance", 25);
	}

	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);

            if (root.getAttributes().getLength()>0) {
                NodeList nodes0 = root.getChildNodes();
                for (int i = 0; i < nodes0.getLength(); i++) {
                     Node node0 = nodes0.item(i);
                     if (node0.getNodeType() == Node.ELEMENT_NODE) {
                        if (node0.getNodeName().equals("sessionID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              session = txt.trim();
                        } else if (node0.getNodeName().equals("instanceID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              instanceID = txt.trim();
                        }
                     }
                }
            }
		return 0;
	}

	protected int checkParameters() throws Exception {

        if (IsNull(instanceID)){
            throw new Exception( "Parameter error: instanceID can't be empty.");
        }

        ////// Check if instance exists
        try{
            vmNode = getClient().getGridObjectInfo(TYPE_RESOURCE,instanceID);
        }catch(Exception ex){
        }

        if (vmNode == null){
            throw new Exception("VM '"+instanceID+"' doesn't exist.");
        }
		if (! vmNode.getEffectiveFact("resource.type").getStringValue().equalsIgnoreCase(TYPE_VM_INSTANCE)){
			throw new Exception("Resource: "
					+ instanceID + " is not a vm instance");
		}

        //// check vm's status ////
        Fact f= null;
        f = vmNode.getEffectiveFact("resource.provision.state");
        if (f == null){
            throw new Exception("Can't get vm:" + instanceID +"'s state.");
        }
        try {
            if (!"up".equals(f.getStringValue())){
                throw new Exception("vm:" + instanceID +" is not up.");
            }
        }
        catch(com.novell.zos.grid.FactException ex) {
            throw new Exception("Can't get vm:" + instanceID +"'s state.");
        }
        return 0;
	}

	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

        try{
            RegRemoteConsoleVNCPort(vmNode);
        }catch( Exception ex ){
			return throwError(errNo(3), ex.getMessage());
        }

        try {
            Fact f;                
            f = vmNode.getEffectiveFact("resource.vnc.ip");
            if ((f == null)||f.getStringValue().equals("")){
                return throwError(errNo(6),  "The VNC console IP address for VM '"+instanceID+"' isn't set.");
            }
            
            f = vmNode.getEffectiveFact("resource.vnc.port");
            if ((f == null)|| f.getIntegerValue() == -1){
                return throwError(errNo(7),  "The VNC console port for VM ':"+instanceID+" isn't set.");
            }        

            f = vmNode.getEffectiveFact("resource.jnlp.vncIpAddress");
            if ((f == null)||f.getStringValue().equals("")){
                return throwError(errNo(8),  "Remote console tools for VM '"+instanceID+"' aren't setup for vncIpAddress.");
            } 
            String jnlp_vncIpAddress = f.getStringValue();

            f = vmNode.getEffectiveFact("resource.jnlp.vncPort");
            if ((f == null)||f.getStringValue().equals("")){
                return throwError(errNo(9),  "Remote console tools for VM '"+instanceID+" aren't setup for vncPort.");
            }
            String jnlp_vncPort= f.getStringValue();            

            String jnlp_codebase="";
            f = vmNode.getEffectiveFact("resource.jnlp.codebase");
            if ( f != null ){
                jnlp_codebase=""+f.getStringValue();
            }

            String jnlp_certData="";
            f = vmNode.getEffectiveFact("resource.jnlp.certData");
            if ( f != null ){
                jnlp_certData=""+f.getStringValue();
            }

            StringBuffer sb = new StringBuffer("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            sb.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            sb.append("<jnlp spec=\"1.0+\" codebase=\"" + jnlp_codebase + "\">");
            sb.append("   <information>");
            sb.append("      <title>PSO VNC Application</title>");
            sb.append("      <vendor>Novell, Inc.</vendor>");
            sb.append("      <description>VNC Application</description>");
            sb.append("      <description kind=\"short\">VNC console</description>");
            sb.append("      <offline-allowed/>");
            sb.append("    </information>");
            sb.append("    <resources>");
            sb.append("      <j2se version=\"1.3+\" href=\"http://java.sun.com/products/autodl/j2se\"/>");
            sb.append("      <jar href=\"vncviewer.jar\" main=\"true\" download=\"eager\"/>");
            sb.append("    </resources>");
            sb.append("    <application-desc main-class=\"tightvnc.VncViewer\">");
            sb.append("       <argument>HOST</argument>");
            sb.append("       <argument>" + jnlp_vncIpAddress + "</argument>");
            sb.append("       <argument>PORT</argument>");
            sb.append("       <argument>" + jnlp_vncPort + "</argument>");
            sb.append("       <argument>Encoding</argument>");
            sb.append("       <argument>" + "Raw" + "</argument>");
            sb.append("       <argument>certData</argument>");
            sb.append("       <argument>" + jnlp_certData + "</argument>");
            sb.append("    </application-desc>");
            sb.append("</jnlp>");
		    return sb.toString();

        } catch (Exception ex) {
            return throwError(errNo(11), "Get fact error: "+ex.getMessage());
        }
	}

    private void RegRemoteConsoleVNCPort(GridObjectInfo vmnode) throws Exception
    {    	
    	String vmname = vmnode.getEffectiveFact("resource.id").getStringValue();
    	String vncip = vmnode.getEffectiveFact("resource.vnc.ip").getStringValue();
    	long vncport = vmnode.getEffectiveFact("resource.vnc.port").getIntegerValue();
    	try{
    		String cmdline = "/usr/sbin/regrdc reg " + vmname + " " + vncip + " " + vncport;
    		Process p = Runtime.getRuntime().exec(cmdline);
    		if(p.waitFor() == 0){
    			Scanner sc = new Scanner(p.getInputStream()); 
    			while (sc.hasNext()){
    				String line = sc.nextLine();
    				String[] fields = line.split("=",2);
    				if (line.startsWith("codebase")){
                        getClient().setFact(TYPE_RESOURCE,vmname,"resource.jnlp.codebase",com.novell.zos.grid.Fact.TYPE_STRING, fields[1].trim());
    				}else if (line.startsWith("href")){
                        getClient().setFact(TYPE_RESOURCE,vmname,"resource.jnlp.href",com.novell.zos.grid.Fact.TYPE_STRING, fields[1].trim());
    				}else if (line.startsWith("vncIpAddress")){
                        getClient().setFact(TYPE_RESOURCE,vmname,"resource.jnlp.vncIpAddress",com.novell.zos.grid.Fact.TYPE_STRING, fields[1].trim());
    				}else if (line.startsWith("vncPort")){
                        getClient().setFact(TYPE_RESOURCE,vmname,"resource.jnlp.vncPort",com.novell.zos.grid.Fact.TYPE_STRING, fields[1].trim());
    				}else if (line.startsWith("certData")){
    					String certData = vmname;
    					if(getProvisionerJob(vmnode).equalsIgnoreCase("xen"))
    						certData = "Xen_"+certData;
                        getClient().setFact(TYPE_RESOURCE,vmname,"resource.jnlp.certData",com.novell.zos.grid.Fact.TYPE_STRING, certData);
    				}
    			}
    		}else{
    			throw new Exception("Fail to run the command: " + cmdline);
    		}
    	}catch(Exception ex){
    		throw new Exception("Fail to register remote console: "+ex.getMessage());
    	}
    }
 
}
