/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */
package com.novell.cloud.operation;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.toolkit.ClientAgentImpl;

public class RegisterPhysicalHost extends Op {
	protected String hostName = null;
	protected String ipAddress = null;

	public RegisterPhysicalHost() {
		super("RegisterPhysicalHost", "register the phsical host", 4);
	} 
	
	public RegisterPhysicalHost(ClientAgent client) {
		super(client, "RegisterPhysicalHost", "register the phsical host", 4);
	}
	
	public RegisterPhysicalHost(String name, String summary,int moduleid) {
		super(name,summary,moduleid);
	}
	
	public RegisterPhysicalHost(ClientAgent client, String name, String summary,int moduleid) {
		super(client, name,summary,moduleid);
	}

	/**
	 * Overridden by commands
	 */
	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);
		if (root.getAttributes().getLength() > 0) {
			NodeList nodes0 = root.getChildNodes();
			for (int i = 0; i < nodes0.getLength(); i++) {
				Node node0 = nodes0.item(i);
				if (node0.getNodeType() == Node.ELEMENT_NODE) {
					if (node0.getNodeName().equals("sessionID")) {
						NodeList nodes1 = node0.getChildNodes();
						String txt = "";
						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
							Node node1 = nodes1.item(i1);
							if (node1.getNodeType() == Node.TEXT_NODE) {
								txt = txt + node1.getNodeValue();
							}
						}
						session = txt.trim();
					} else if (node0.getNodeName().equals("hostName")) {
						NodeList nodes1 = node0.getChildNodes();
						String txt = "";
						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
							Node node1 = nodes1.item(i1);
							if (node1.getNodeType() == Node.TEXT_NODE) {
								txt = txt + node1.getNodeValue();
							}
						}
						hostName = txt.trim();
					} else if (node0.getNodeName().equals("ipAddress")) {
						NodeList nodes1 = node0.getChildNodes();
						String txt = "";
						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
							Node node1 = nodes1.item(i1);
							if (node1.getNodeType() == Node.TEXT_NODE) {
								txt = txt + node1.getNodeValue();
							}
						}
						ipAddress = txt.trim();
					}
				}
			}
		}
		return 0;
	}

	protected int checkParameters() throws Exception {
		if (IsNull(hostName)) {
			throw new Exception("Parameter error: hostName can't be empty.");
		}
		if (IsNull(ipAddress)) {
			throw new Exception("Parameter error: ipAddress can't be empty.");
		}
		
		GridObjectInfo node = getClient().getGridObjectInfo(TYPE_RESOURCE,
				hostName);
		if (node == null) {
			throw new Exception("Could not find the host: "
				+ hostName + "in the matrix");
		}
		
		//check if it is a physical host
		if (! node.getEffectiveFact("resource.type").getStringValue().equalsIgnoreCase(TYPE_FIXED_PHYSICAL)){
			throw new Exception("The host: "
					+ hostName + " is not a physical host");
		}
			
		return 0;
	}

	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

		try {			
			String factname = "resource.AIS.registered";
			getClient().setFact(TYPE_RESOURCE, hostName, factname,
					com.novell.zos.grid.Fact.TYPE_BOOLEAN, true);

		} catch (Exception ex) {
			return throwError(errNo(9), ex.getMessage());
		}

		StringBuffer sb = new StringBuffer(XML_HEADER);
		sb.append("<results>\n");
		sb.append("    <result>0</result>\n");
		sb.append("</results>");

		return sb.toString();
	}

}
