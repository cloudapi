/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Arrays;
import java.util.Iterator;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactSet;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.GridObjectNotFoundException;
import com.novell.zos.grid.WorkflowInfo;

public class DescribeVolumeSnapshots extends Op {

    private String snapshotID = null;
    private String snapshotPath = null;
    private ArrayList<HashMap> snapSetList = new ArrayList<HashMap>();
    private ArrayList<String> snapIdList = new ArrayList<String>();

	public DescribeVolumeSnapshots() {
		super("DescribeVolumeSnapshots", "get the information of a snapshot of a disk", 63);
	}

	public DescribeVolumeSnapshots(ClientAgent client) {
		super(client,"DescribeVolumeSnapshots", "get the information of a snapshot of a disk", 63);
	}

	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);

            if (root.getAttributes().getLength()>0) {
                NodeList nodes0 = root.getChildNodes();
                for (int i = 0; i < nodes0.getLength(); i++) {
                     Node node0 = nodes0.item(i);
                     if (node0.getNodeType() == Node.ELEMENT_NODE) {    //Level 1
                        if (node0.getNodeName().equals("sessionID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                        txt = txt + node1.getNodeValue();
                                   }
                              }
                              session = txt.trim();
                        } else if (node0.getNodeName().equals("describeSnapshotSet")){
                             NodeList nodes1 = node0.getChildNodes();
                             for (int i1 = 0; i1 < nodes1.getLength(); i1++) {

                                  Node node1 = nodes1.item(i1);
                                  if (node1.getNodeType() == Node.ELEMENT_NODE) {   //Level 2

                                       if (node1.getNodeName().equals("item")){
                                            NodeList nodes2 = node1.getChildNodes();
                                            for (int i2 = 0; i2 < nodes2.getLength(); i2++){

                                                Node node2 = nodes2.item(i2);
                                                if (node2.getNodeType() ==Node.ELEMENT_NODE){    //Level 3

                                                    if (node2.getNodeName().equals("snapshot")){                                                    
                                                           NodeList nodes3 = node2.getChildNodes();
                                                           HashMap<String,String> snapSet = new HashMap<String,String>();
                                                           for (int i3 = 0; i3 < nodes3.getLength(); i3++) {

                                                                Node node3 = nodes3.item(i3);
                                                                if (node3.getNodeType() == Node.ELEMENT_NODE) {     //Level 4

                                                                          if (node3.getNodeName().equals("snapshotID")) {
                                                                                NodeList nodes4 = node3.getChildNodes();
                                                                                String txt="";
                                                                                for (int i4 = 0; i4 < nodes4.getLength(); i4++) {
                                                                                    Node node4 = nodes4.item(i4);
                                                                                    if (node4.getNodeType() == Node.TEXT_NODE) {
                                                                                       txt = txt + node4.getNodeValue();
                                                                                   }
                                                                                }
                                                                                txt = txt.trim();
                                                                                if (IsNotNull(txt)){
                                                                                    snapSet.put("snapshotID",txt);
                                                                                    snapIdList.add(txt);
                                                                                } else {
                                                                                    throw new Exception("Parameter error: bad nodes name.");
                                                                                }
                                                                           } else if (node3.getNodeName().equals("snapshotPath")){
                                                                                NodeList nodes4 = node3.getChildNodes();
                                                                                String txt="";
                                                                                for (int i4 = 0; i4 < nodes4.getLength(); i4++) {
                                                                                    Node node4 = nodes4.item(i4);
                                                                                    if (node4.getNodeType() == Node.TEXT_NODE) {
                                                                                       txt = txt + node4.getNodeValue();
                                                                                    }
                                                                                }
                                                                                snapSet.put("snapshotPath",txt.trim());
                                                                           }
                                                                }
                                                           }
                                                           snapSetList.add(snapSet);
                                                    }
                                                }
                                            }
                                       }
                                  }
                             }
                        }
                     }
                }
            }
		return 0;
	}

	protected int checkParameters() throws Exception {

        return 0;
	}
    
	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

        if (snapIdList.size()==0){
            StringBuffer sb = new StringBuffer(XML_HEADER);
            sb.append("<results>\n");
            sb.append("    <result>" + 0 +"</result>\n");
            sb.append("    <snapshotSet/>\n");
            sb.append("</results>\n");
            return sb.toString();
        }

        String jobName = "volumemanager";
		JobSpec job = this.getJob(jobName);
        if (job == null){
            return throwError(errNo(3),"Job '" + jobName +"' is not deployed.");
        }
        
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("snapshotSet", snapSetList);

		JobDef jobDef = new JobDef();
		jobDef.setName(jobName);
		jobDef.setJob(job);
		jobDef.resetImports();
        jobDef.setImport("jobargs.action", "findsnapshot");
        jobDef.setImport("jobargs.params", params);
		jobDef.setWhatComplete(true);
		jobDef.setWaitForCompletion(true);

		RunJob runJob = new RunJob(getClient());
		try {
			if (!runJob.run(jobDef)) {
				return throwError(errNo(4), "Error running job '" + jobName
						+ "'. \nJob Log:\n" + getJobLog(runJob.getJobId()));
			}
		} catch (Exception ex) {
			return throwError(errNo(5), ex.getMessage());
		}

        String jobID = runJob.getJobId();
        String username = getClient().getStatusDetail(jobID, false).getUsername();
        
        GridObjectInfo localUser = getGridObject(TYPE_USER,username);
        if (localUser == null){
            return throwError(errNo(9),"Unable to lookup user for login name '" + username + "'.");
        }
        
        String factName = "job.iwm.volsnapshots.info." + jobID;
        Fact f = localUser.getEffectiveFact(factName);

        if ( f == null){
            return throwError(errNo(10),"Can't find snapshots.\nJob Log:\n"+getJobLog(jobID));
        }

        List snapshotList = null;
        try {
            snapshotList = f.getListValue();
            localUser.getEffectiveFacts().remove(factName);    //remove fact
        } catch (FactException ex){
            return throwError(errNo(11), "Get fact error: "+ex.getMessage());
        }
        
        if (snapshotList == null){
            snapshotList = new ArrayList<HashMap>();
        }
        StringBuffer sb = new StringBuffer(XML_HEADER);
        sb.append("<results>\n");
        sb.append("    <result>" + 0 +"</result>\n");
        sb.append("    <snapshotSet>\n");
        for (int i=0; i< snapshotList.size(); i++){
            sb.append("        <item>\n");

            Map snapshotInfo = (Map) (snapshotList.get(i));
            List attachInfos = null;
            Iterator iT = snapshotInfo.keySet().iterator();
            while (iT.hasNext()) {
                String aKey = iT.next().toString();
                if ( aKey.equals("dest") || aKey.equals("snapshotSet") ){
                    // Skip internal fact
                } else if (aKey.equals("snapshot")){
                    Map asnapshot = (Map)snapshotInfo.get("snapshot");
                    sb.append("            <snapshot>\n");
                    sb.append("                <snapshotID>" + asnapshot.get("snapshotID") + "</snapshotID>\n");
                    sb.append("                <snapshotPath>" + asnapshot.get("snapshotPath") + "</snapshotPath>\n");
                    sb.append("            </snapshot>\n");
                } else {
                    sb.append("            <"+aKey+">" + snapshotInfo.get(aKey) + "</"+aKey+">\n");
                }
            }

            sb.append("        </item>\n");
        }
        sb.append("    </snapshotSet>\n");
        sb.append("</results>\n");
        
        return sb.toString();
	}
}
