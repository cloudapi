/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Arrays;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.GridObjectNotFoundException;

public class DescribeVirtDeviceOfHost extends Op {

	private String deviceType = null;
	private String deviceName = null;
	private String repName = null;
	private String hostName = null;
	private long n_deviceType = 0;
	private GridObjectInfo repo = null;
	private GridObjectInfo host = null;

	public DescribeVirtDeviceOfHost() {
		super("DescribeVirtDeviceOfHost", "get network infomation in the host",
				55);
	}

	public DescribeVirtDeviceOfHost(ClientAgent client) {
		super(client, "DescribeVirtDeviceOfHost",
				"get network infomation in the host", 55);
	}

	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);

		if (root.getAttributes().getLength() > 0) {
			NodeList nodes0 = root.getChildNodes();
			for (int i = 0; i < nodes0.getLength(); i++) {
				Node node0 = nodes0.item(i);
				if (node0.getNodeType() == Node.ELEMENT_NODE) {
					if (node0.getNodeName().equals("sessionID")) {
						NodeList nodes1 = node0.getChildNodes();
						String txt = "";
						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
							Node node1 = nodes1.item(i1);
							if (node1.getNodeType() == Node.TEXT_NODE) {
								txt = txt + node1.getNodeValue();
							}
						}
						session = txt.trim();
					} else if (node0.getNodeName().equals("deviceType")) {
						NodeList nodes1 = node0.getChildNodes();
						String txt = "";
						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
							Node node1 = nodes1.item(i1);
							if (node1.getNodeType() == Node.TEXT_NODE) {
								txt = txt + node1.getNodeValue();
							}
						}
						deviceType = txt.trim();
					} else if (node0.getNodeName().equals("deviceName")) {
						NodeList nodes1 = node0.getChildNodes();
						String txt = "";
						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
							Node node1 = nodes1.item(i1);
							if (node1.getNodeType() == Node.TEXT_NODE) {
								txt = txt + node1.getNodeValue();
							}
						}
						deviceName = txt.trim();
					} else if (node0.getNodeName().equals("repName")) {
						NodeList nodes1 = node0.getChildNodes();
						String txt = "";
						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
							Node node1 = nodes1.item(i1);
							if (node1.getNodeType() == Node.TEXT_NODE) {
								txt = txt + node1.getNodeValue();
							}
						}
						repName = txt.trim();
					} else if (node0.getNodeName().equals("hostName")) {
						NodeList nodes1 = node0.getChildNodes();
						String txt = "";
						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
							Node node1 = nodes1.item(i1);
							if (node1.getNodeType() == Node.TEXT_NODE) {
								txt = txt + node1.getNodeValue();
							}
						}
						hostName = txt.trim();
					}
				}
			}
		}
		return 0;
	}

	protected int checkParameters() throws Exception {

		if (IsNull(deviceType)) {
			throw new Exception("Parameter error: deviceType can't be empty.");
		}
		if (IsNull(deviceName)) {
			throw new Exception("Parameter error: deviceName can't be empty.");
		}
		if (IsNull(hostName)) {
			throw new Exception("Parameter error: hostName can't be empty.");
		}

		n_deviceType = Long.parseLong(deviceType);

		// //// Check if host exists ////////////
		GridObjectInfo host = getGridObject(TYPE_RESOURCE, hostName);
		if (host == null) {
			throw new Exception("Can't find host '" + hostName + "'.");
		}

		// //// TODO: check baseDevName and deviceName

		return 0;
	}

	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

		if (n_deviceType == 2) {
			return getVlanInfo();
		} else if (n_deviceType == 3 || n_deviceType == 5) {
			return getBridgeInfo();
		} else {
			return throwError(errNo(3), "Device type '" + n_deviceType
					+ "' not supported.");
		}
	}

	private String getVlanInfo() {
		try {
			Fact f = host.getEffectiveFact("resource.iwm.vlans");
			if (f == null) {
				return throwError(errNo(6), "Can't get vlan info from host '"
						+ hostName + "'.");
			}

			List vlans = f.getListValue();
			if ((vlans == null)) {
				return throwError(errNo(7), "Host '" + hostName
						+ "' has no vlans.");
			}

			StringBuffer sb = new StringBuffer(XML_HEADER);
			sb.append("<results>\n");
			sb.append("    <result>" + 0 + "</result>\n");
			sb.append("    <hostName>" + hostName + "</hostName>\n");
			sb.append("    <virtDev>\n");
			for (int i = 0; i < vlans.size(); i++) {
				Map vlan = (Map) vlans.get(i);
				sb.append("        <item>\n");
				sb.append("            <deviceType>" + n_deviceType
						+ "</deviceType>\n");
				sb.append("            <deviceName>" + vlan.get("vlan_name")
						+ "</deviceName>\n");
				sb.append("            <extraInfo>" + vlan.get("interface")
						+ "</extraInfo>\n");
				sb.append("        </item>\n");
			}
			sb.append("    </virtDev>\n");
			sb.append("</results>\n");

			return sb.toString();
		} catch (FactException ex) {
			return throwError(errNo(10), "Get fact error. " + ex.getMessage());
		}
	}

	private String getBridgeInfo() {
		try {

			String jobName = "vlanmanager";
			JobSpec job = this.getJob(jobName);
			if (job == null) {
				return throwError(errNo(3), "Job '" + jobName
						+ "' is not deployed.");
			}

			JobDef jobDef = new JobDef();
			jobDef.setName(jobName);
			jobDef.setJob(job);
			jobDef.resetImports();
			jobDef.setImport("jobargs.action", "describe");
			jobDef.setImport("jobargs.host", hostName);
			jobDef.setWhatComplete(true);
			jobDef.setWaitForCompletion(true);

			RunJob runJob = new RunJob(getClient());
			try {
				if (!runJob.run(jobDef)) {
					return throwError(errNo(4), "Error running job '" + jobName
							+ "'. \nJob Log:\n" + getJobLog(runJob.getJobId()));
				}
			} catch (Exception ex) {
				return throwError(errNo(5), ex.getMessage());
			}

			Fact f = host.getEffectiveFact("resource.iwm.vbridges");
			if (f == null) {
				return throwError(errNo(6), "Can't get vlan info from host '"
						+ hostName + "'.");
			}

			List bridges = f.getListValue();
			if (bridges == null) {
				return throwError(errNo(7), "Host '" + hostName
						+ "' has no vlans.");
			}

			StringBuffer sb = new StringBuffer(XML_HEADER);
			sb.append("<results>\n");
			sb.append("    <result>" + 0 + "</result>\n");
			sb.append("    <hostName>" + hostName + "</hostName>\n");
			sb.append("    <virtDev>\n");
			for (int i = 0; i < bridges.size(); i++) {
				Map bridge = (Map) bridges.get(i);
				Object name = bridge.get("bridge_name");
				if (null == name) {
					name = bridge.get("name");
				}
				sb.append("        <item>\n");
				sb.append("            <deviceType>" + n_deviceType
						+ "</deviceType>\n");
				sb.append("            <deviceName>" + name + "</deviceName>\n");
				sb.append("            <extraInfo>" + bridge.get("interfaces")
						+ "</extraInfo>\n");
				sb.append("        </item>\n");
			}
			sb.append("    </virtDev>\n");
			sb.append("</results>\n");

			return sb.toString();
		} catch (FactException ex) {
			return throwError(errNo(10), "Get fact error: " + ex.getMessage());
		}
	}
}
