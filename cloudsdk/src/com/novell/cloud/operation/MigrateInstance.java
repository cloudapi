/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.util.ArrayList;
import java.util.HashMap;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.GridObjectNotFoundException;

public class MigrateInstance extends Op {

    private String instanceID = null;
    private String vmhostName =null;
    private String destRepName =null;

    private ArrayList nicSet = new ArrayList();
    private ArrayList diskSet = new ArrayList();

	public MigrateInstance() {
		super("MigrateInstance", "Migrate a vm to anthor vm host", 36);
	}

	public MigrateInstance(ClientAgent client) {
		super(client,"MigrateInstance", "Migrate a vm to anthor vm host", 36);
	}

	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);

            if (root.getAttributes().getLength()>0) {
                NodeList nodes0 = root.getChildNodes();
                for (int i = 0; i < nodes0.getLength(); i++) {
                     Node node0 = nodes0.item(i);
                     if (node0.getNodeType() == Node.ELEMENT_NODE) {
                        if (node0.getNodeName().equals("sessionID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              session = txt.trim();
                        } else if (node0.getNodeName().equals("instanceID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              instanceID = txt.trim();
                        } else if (node0.getNodeName().equals("vmhostName")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              vmhostName = txt.trim();
                        } else if (node0.getNodeName().equals("destRepName")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              destRepName = txt.trim();
                        }
                     }
                }
            }
		return 0;
	}

	protected int checkParameters() throws Exception {

        if (IsNull(instanceID)){
            throw new Exception( "Parameter error: instanceID can't be empty.");
        }
        if (IsNull(vmhostName)){
            throw new Exception( "Parameter error: vmhostName can't be empty.");
        }
        
        GridObjectInfo node = getGridObject(TYPE_RESOURCE,instanceID);
        if ( node == null ){
            throw new Exception("Instance '"+instanceID+"' doesn't exist.");
        }
		if (! node.getEffectiveFact("resource.type").getStringValue().equalsIgnoreCase(TYPE_VM_INSTANCE)){
            throw new Exception( "Resource '" + instanceID +"' isn't a vm instance.");
        }
        
        node = getGridObject(TYPE_RESOURCE,vmhostName);
        if ( node == null ) {
            throw new Exception("vmhost '"+vmhostName+"' doesn't exist.");
        }
		if (! node.getEffectiveFact("resource.type").getStringValue().equalsIgnoreCase(TYPE_FIXED_PHYSICAL)){
            throw new Exception( "Resource '" + vmhostName +"' isn't a fixed physical host.");
        }
        
        if (IsNotNull(destRepName)){
            GridObjectInfo repo = getGridObject(TYPE_REPOSITORY,destRepName);
            if (repo == null){
                throw new Exception( "Destination repository '"+destRepName+"' doesn't exist.");
            }
        }

        return 0;
	}

	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

        ////// Call job to clone instance ///////
        String jobName = "vmmigrate";
		JobSpec job = this.getJob(jobName);
        if (job == null){
            return throwError(errNo(3),"Job '" + jobName +"' is not deployed.");
        }

		JobDef jobDef = new JobDef();
		jobDef.setName(jobName);
		jobDef.setJob(job);
		jobDef.resetImports();
        jobDef.setImport("jobargs.newHostName", vmhostName);
        jobDef.setImport("jobargs.vmname", instanceID);
        jobDef.setImport("jobargs.destRepName", destRepName);
		jobDef.setWhatComplete(true);
		jobDef.setWaitForCompletion(false);

		RunJob runJob = new RunJob(getClient());
		try {
			if (!runJob.run(jobDef)) {
				return throwError(errNo(3), "Error running job '" + jobName
						+ "'. \nJob Log:\n" + getJobLog(runJob.getJobId()));
			}
		} catch (Exception ex) {
			return throwError(errNo(4), ex.getMessage());
		}

        String jobID = runJob.getJobId();
		
        StringBuffer sb = new StringBuffer(XML_HEADER);
        sb.append("<results>\n");
        sb.append("    <result>"+ 0 +"</result>\n");          // result
        sb.append("    <jobID>" + jobID + "</jobID>\n");
        sb.append("</results>");
        return sb.toString();
	}
}
