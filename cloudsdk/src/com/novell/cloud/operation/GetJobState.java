/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Arrays;
import java.util.Scanner;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.HtmlEncoder;
import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.GridObjectNotFoundException;
import com.novell.zos.grid.WorkflowInfo;

public class GetJobState extends Op {

    private String[] jobIDs  = null;
    private ArrayList<String> jobIDList = new ArrayList<String>();
    private ArrayList<WorkflowInfo> jobInstanceList = new ArrayList<WorkflowInfo>();

	public GetJobState() {
		super("GetJobState", "Get the job information", 50);
	}

	public GetJobState(ClientAgent client) {
		super(client,"GetJobState", "Get the job information", 50);
	}

	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);

            if (root.getAttributes().getLength()>0) {
                NodeList nodes0 = root.getChildNodes();
                for (int i = 0; i < nodes0.getLength(); i++) {
                     Node node0 = nodes0.item(i);
                     if (node0.getNodeType() == Node.ELEMENT_NODE) {
                        if (node0.getNodeName().equals("jobSet")){
                                     NodeList nodes1 = node0.getChildNodes();
                                     for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                          Node node1 = nodes1.item(i1);
                                          if (node1.getNodeType() == Node.ELEMENT_NODE) {
                                                       if (node1.getNodeName().equals("item"))
                                                       {
                                                           NodeList nodes2 = node1.getChildNodes();
                                                           for (int i2 = 0; i2 < nodes2.getLength(); i2++) {
                                                                Node node2 = nodes2.item(i2);
                                                                if (node2.getNodeType() == Node.ELEMENT_NODE) {
                                                                          if (node2.getNodeName().equals("jobID"))
                                                                          {
                                                                            NodeList nodes3 = node2.getChildNodes();
                                                                            String txt="";
                                                                            for (int i3 = 0; i3 < nodes3.getLength(); i3++) {
                                                                                Node node3 = nodes3.item(i3);
                                                                                if (node3.getNodeType() == Node.TEXT_NODE) {
                                                                                   txt = txt + node3.getNodeValue();
                                                                                   }
                                                                                }
                                                                            jobIDList.add(txt.trim());
                                                                           }
                                                                }
                                                           }
                                                       }
                                          }
                                     }
                        } else if (node0.getNodeName().equals("sessionID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                    txt = txt + node1.getNodeValue();
                                   }
                              }
                              session = txt.trim();
                        }
                     }
                }
            }
		return 0;
	}

	protected int checkParameters() throws Exception {

        return 0;
	}

	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

        SimpleDateFormat sdf =new SimpleDateFormat( "yyyy-MM-dd'T'hh:mm:ssZ");

        StringBuffer sb = new StringBuffer(XML_HEADER);
        sb.append("<results>\n");
        sb.append("    <result>"+ 0 +"</result>\n");    
        sb.append("    <jobSet>\n");
        
        if (jobIDList.size() > 0){
            for (int i = 0; i < jobIDList.size(); i++) {
                String jobID = jobIDList.get(i).toString();
                WorkflowInfo wj= getClient().getStatusDetail(jobID,false);
                if (wj == null){
                    sb.append("        <item>\n");
                    sb.append("            <jobID>" + jobID + "</jobID>\n");
                    sb.append("            <state>" + "-2" + "</state>\n");
                    sb.append("            <statusStr>" + "Not found" + "</statusStr>\n");
                    sb.append("        </item>\n");
                } else {
                    jobInstanceList.add(wj);
                }
            }
        } else {      // Get All Job State
            boolean allJobs = true;
            boolean activeOnly = false;
            String parentWorkflowId = null;
            long submitTime = 0;
            int maxCount = 100;
            boolean detail = false;
            WorkflowInfo[] ws = getClient().getRunningJobs(allJobs,activeOnly,parentWorkflowId,submitTime,maxCount,detail);

            for (int i = 0; i < ws.length; i++) {
                jobInstanceList.add(ws[i]);
            }
        }
        
        for (int i=0; i<jobInstanceList.size(); i++){
            WorkflowInfo jobInstance = jobInstanceList.get(i);
            String jobid = jobInstance.getJobID();
            
            int status = getClient().getStatus(jobid);

            sb.append("        <item>\n");
            sb.append("            <jobID>" + jobid + "</jobID>\n");
            if(( status == WorkflowInfo.RUNNING_STATE) || ( status == WorkflowInfo.PAUSED_STATE )){
                sb.append("            <state>" + "1" + "</state>\n");
                sb.append("            <process>" + "20" + "</process>\n");
            } else if (status == WorkflowInfo.QUEUED_STATE){
                sb.append("            <state>" + "2" + "</state>\n");
                sb.append("            <process>" + "0" + "</process>\n");
            } else if (status == WorkflowInfo.FAILED_STATE){
                sb.append("            <state>" + "-1" + "</state>\n");
                sb.append("            <process>" + "100" + "</process>\n");
            } else if (status == WorkflowInfo.CANCELLED_STATE) {
                sb.append("            <state>" + "-3" + "</state>\n");
                sb.append("            <process>" + "100" + "</process>\n");
            } else if (status == WorkflowInfo.COMPLETED_STATE) {
                sb.append("            <state>" + "0" + "</state>\n");
                sb.append("            <process>" + "100" + "</process>\n");
            } else {
                sb.append("            <state>" + "-4" + "</state>\n");
                sb.append("            <process>" + "0" + "</process>\n");
            }
            sb.append("            <userName>" + jobInstance.getUsername() + "</userName>\n");
            sb.append("            <statusCode>" + status + "</statusCode>\n");
            sb.append("            <statusStr>" + jobInstance.getStateString() + "</statusStr>\n");
            sb.append("            <memo>" +jobInstance.getMemo()+ "</memo>\n");
            
            if(status == WorkflowInfo.FAILED_STATE || status == WorkflowInfo.CANCELLED_STATE){
                sb.append("            <errorInfo>" + HtmlEncoder.encode(getJobLog(jobid))+ "</errorInfo>\n");
            }
            
            try {
                sb.append("            <submitTime>" + dateTimeFormat(jobInstance.getTimeSubmittedMillis()) + "</submitTime>\n");
                sb.append("            <startTime>" + dateTimeFormat(jobInstance.getTimeStartedMillis()) + "</startTime>\n");
                sb.append("            <endTime>" + dateTimeFormat(jobInstance.getTimeCompletedMillis()) + "</endTime>\n");
            } catch (Exception e){System.err.print(e.getMessage());}
            sb.append("            <elapsedTime>" +elapsedTimeFormat(jobInstance.getTimeElapsedMillis()) + "</elapsedTime>\n");
            sb.append("            <jobInstanceName>" +jobInstance.getJobInstanceName()+ "</jobInstanceName>\n");
            sb.append("        </item>\n");
        }

        sb.append("    </jobSet>\n");
        sb.append("</results>\n");
        return sb.toString();
	}
}
