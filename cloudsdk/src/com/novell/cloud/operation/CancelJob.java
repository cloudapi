/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Arrays;
import java.util.Scanner;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.HtmlEncoder;
import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.GridObjectNotFoundException;
import com.novell.zos.grid.WorkflowInfo;

public class CancelJob extends Op {

    private String jobID_in = null;

	public CancelJob() {
		super("CancelJob", "Cancel an job", 51);
	}

	public CancelJob(ClientAgent client) {
		super(client,"CancelJob", "Cancel an job", 51);
	}

	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);

            if (root.getAttributes().getLength()>0) {
                NodeList nodes0 = root.getChildNodes();
                for (int i = 0; i < nodes0.getLength(); i++) {
                     Node node0 = nodes0.item(i);
                     if (node0.getNodeType() == Node.ELEMENT_NODE) {
                        if (node0.getNodeName().equals("sessionID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              session = txt.trim();
                        } else if (node0.getNodeName().equals("jobID")) {
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              jobID_in = txt.trim();
                        }
                     }
                }
            }
		return 0;
	}

	protected int checkParameters() throws Exception {

        if(IsNull(jobID_in)){
            throw new Exception( "Parameter error: jobID can't be empty.");
        }

        return 0;
	}

	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

        try{
            cancelJob(jobID_in);
		} catch (Exception ex) {
			return throwError(errNo(3), ex.getMessage());
		}

        StringBuffer sb = new StringBuffer(XML_HEADER);
        sb.append("<results>\n");
        sb.append("    <result>"+ 0 +"</result>\n");
        sb.append("</results>");

        return sb.toString();   
	}

    public void cancelJob(String jobid) throws Exception{
        getClient().cancelJob(jobid);
    }
}
