/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.net.InetAddress;

import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;

public class GetPhysicalHostState extends RegisterPhysicalHost {

	public GetPhysicalHostState() {
		super("GetPhysicalHostState", "Get the state the physical host", 7);
	}

	public GetPhysicalHostState(ClientAgent client) {
		super(client, "GetPhysicalHostState", "Get the state the physical host", 7);
	}

	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

		GridObjectInfo node = getClient().getGridObjectInfo(TYPE_RESOURCE,
				hostName);

		String state = null;
		try {
			state = getPhysicalHostState(node);
		} catch (Exception ex) {
			return throwError(errNo(9), ex.getMessage());
		}

		StringBuffer sb = new StringBuffer(XML_HEADER);
		sb.append("<results>\n");
		sb.append("    <result>" + 0 + "</result>\n");
		sb.append("    <state>" + state + "</state>\n");
		sb.append("</results>");

		return sb.toString();
	}

	public String getPhysicalHostState(GridObjectInfo host) throws Exception {
		Fact f;
		Fact f2;
		boolean ping = false;
		boolean online = false;
		boolean unknown = false;

		f = host.getEffectiveFact("resource.ip");
		if (f != null) {
			try {
				InetAddress adr = InetAddress.getByName(f.getStringValue());
				ping = adr.isReachable(3000);
			} catch (Exception e) {
			}
		} // No IP address set for the resource?

		f = host.getEffectiveFact("resource.online");
		if (f == null) {
			unknown = true;
		} else {
			online = f.getBooleanValue();
		}

		if (ping == false) {
			online = false;
		} else {
			if (online == false)
				unknown = true;
		}

		if (true) {
			// Also check if vSphere reports that the vmhost is online
			f = host.getEffectiveFact("resource.vmhosts");
			if (f != null) {
				String[] vmhosts = f.getStringArrayValue();

				for (int i = 0; i < vmhosts.length; i++) {
					GridObjectInfo vmhost = getClient().getGridObjectInfo(
							TYPE_VMHOST, vmhosts[i]);
					f = vmhost.getEffectiveFact("vmhost.online");
					if (f != null) {
						f2 = vmhost.getEffectiveFact("vmhost.provisioner.job");
						if (f2 != null) {
							// if (f.getBooleanValue() &&
							// "vsphere".equals(f2.getStringValue()) ) online =
							// true;
							if ("vsphere".equals(f2.getStringValue())) {
								unknown = false;
								online = ping;
							}
						}
					}
				}
			}
		}
		if (unknown) {
			return "suspect";
		} else if (online) {
			return "online";
		} else {
			return "offline";
		}
	}
}
