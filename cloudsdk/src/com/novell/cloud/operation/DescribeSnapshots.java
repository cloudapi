/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.List;
import java.util.Arrays;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.GridObjectNotFoundException;

public class DescribeSnapshots extends Op {

    private String snapshotID = null;
    private String strIdType = null;
    private int idType = 0;
    private String instanceID = null;
    private String backupDirectory = null;      //optional
    private ArrayList<Map> SnapshotsList = new ArrayList<Map>();
    private List snapList = null;

	public DescribeSnapshots() {
		super("DescribeSnapshots", "get the information of the snapshots", 45);
	}

	public DescribeSnapshots(ClientAgent client) {
		super(client,"DescribeSnapshots", "get the information of the snapshots", 45);
	}

	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);

            if (root.getAttributes().getLength()>0) {
                NodeList nodes0 = root.getChildNodes();
                for (int i = 0; i < nodes0.getLength(); i++) {
                     Node node0 = nodes0.item(i);
                     if (node0.getNodeType() == Node.ELEMENT_NODE) {
                        if (node0.getNodeName().equals("sessionID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              session = txt.trim();
                        } else if (node0.getNodeName().equals("idType")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              strIdType = txt.trim();
                        } else if (node0.getNodeName().equals("snapshotSet")){
                             NodeList nodes1 = node0.getChildNodes();
                             for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                  Node node1 = nodes1.item(i1);
                                  if (node1.getNodeType() == Node.ELEMENT_NODE) {
                                       if (node1.getNodeName().equals("item"))
                                       {
                                           NodeList nodes2 = node1.getChildNodes();
                                           HashMap<String,String> snapMap = new HashMap<String,String>();
                                           for (int i2 = 0; i2 < nodes2.getLength(); i2++) {
                                                Node node2 = nodes2.item(i2);
                                                if (node2.getNodeType() == Node.ELEMENT_NODE) {
                                                      if (node2.getNodeName().equals("snapshotID"))
                                                      {
                                                            NodeList nodes3 = node2.getChildNodes();
                                                            String txt="";
                                                            for (int i3 = 0; i3 < nodes3.getLength(); i3++) {
                                                                Node node3 = nodes3.item(i3);
                                                                if (node3.getNodeType() == Node.TEXT_NODE) {
                                                                    txt = txt + node3.getNodeValue();
                                                                }
                                                            }
                                                            if (IsNotNull(txt.trim())){
                                                                snapMap.put("snapshotID",txt.trim());
                                                            }
                                                       }
                                                       else if (node2.getNodeName().equals("backupDirectory"))
                                                       {
                                                            NodeList nodes3 = node2.getChildNodes();
                                                            String txt="";
                                                            for (int i3 = 0; i3 < nodes3.getLength(); i3++) {
                                                                Node node3 = nodes3.item(i3);
                                                                if (node3.getNodeType() == Node.TEXT_NODE) {
                                                                    txt = txt + node3.getNodeValue();
                                                                }
                                                            }
                                                            if (IsNotNull(txt.trim())){
                                                                snapMap.put("backupDirectory",txt.trim());
                                                            }
                                                       }
                                                }
                                           }
                                           SnapshotsList.add(snapMap);
                                       }
                                  }
                             }
                        } else if (node0.getNodeName().equals("instanceInfo")){
                             NodeList nodes1 = node0.getChildNodes();
                             for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                  Node node1 = nodes1.item(i1);
                                  if (node1.getNodeType() == Node.ELEMENT_NODE) {
                                       if (node1.getNodeName().equals("instanceID"))
                                       {
                                            NodeList nodes2 = node1.getChildNodes();
                                            String txt="";
                                            for (int i2 = 0; i2 < nodes2.getLength(); i2++) {
                                                Node node2 = nodes2.item(i2);
                                                if (node2.getNodeType() == Node.TEXT_NODE) {
                                                    txt = txt + node2.getNodeValue();
                                                }
                                            }
                                            instanceID = txt.trim();
                                        }
                                        else if (node1.getNodeName().equals("backupDirectory"))
                                        {
                                            NodeList nodes2 = node1.getChildNodes();
                                            String txt="";
                                            for (int i2 = 0; i2 < nodes2.getLength(); i2++) {
                                                Node node2 = nodes2.item(i2);
                                                if (node2.getNodeType() == Node.TEXT_NODE) {
                                                    txt = txt + node2.getNodeValue();
                                                }
                                            }
                                            backupDirectory = txt.trim();
                                        }
                                  }
                             }
                        }
                     }
                }
            }
		return 0;
	}

	protected int checkParameters() throws Exception {

        if (IsNull(strIdType)){
            throw new Exception("Parameter error: idType can't be empty.");
        }
        if (strIdType.equals("1")){
            idType = 1;
        } else if (strIdType.equals("2")){
            idType = 2;
        } else {
            throw new Exception("Parameter error: idType must be '1' or '2' ");
        }

        if (idType == 1){
            if (IsNull(instanceID)){
                throw new Exception("Parameter error: instanceID can't be empty.");
            }
            ////// Check if instance exists //////
            GridObjectInfo vmnode = getGridObject(TYPE_RESOURCE,instanceID);

            if (vmnode == null){
                throw new Exception("VM '"+instanceID+"' doesn't exist.");
            }
    		if (! vmnode.getEffectiveFact("resource.type").getStringValue().equalsIgnoreCase(TYPE_VM_INSTANCE)){
	    		throw new Exception("Resource: "
					+ instanceID + " is not a vm instance");
		    }
        }
        else if (idType == 2){
            if (SnapshotsList.size()==0){
                throw new Exception("Parameter error: snapshotSet can't be empty.");
            }
            for (int i=0; i<SnapshotsList.size(); i++){
                Map aSnapshot = (Map)SnapshotsList.get(i);
                if (!aSnapshot.containsKey("snapshotID")){
                    throw new Exception("Parameter error: snapshotID can't be empty.");
                }
            }
        }

        return 0;
	}

	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

        HashMap<String,Object> params = new HashMap<String,Object>();
        params.put("vmID", instanceID);
        params.put("idType", idType);
        if (idType == 1){
            Map<String,String> instanceInfo = new HashMap<String,String>();
            instanceInfo.put("instanceID", instanceID);
            instanceInfo.put("backupDirectory", backupDirectory);
            params.put("instanceInfo", instanceInfo);
        } else if (idType == 2){
            params.put("snapshotSet", SnapshotsList);
        }


        String jobName = "vmbackup";
		JobSpec job = this.getJob(jobName);
        if (job == null){
            return throwError(errNo(4),"Job '" + jobName +"' is not deployed.");
        }
        
		JobDef jobDef = new JobDef();
		jobDef.setName(jobName);
		jobDef.setJob(job);
		jobDef.resetImports();
        jobDef.setImport("jobargs.action", "find");
        jobDef.setImport("jobargs.params", params);
		jobDef.setWhatComplete(true);
		jobDef.setWaitForCompletion(true);

		RunJob runJob = new RunJob(getClient());
		try {
			if (!runJob.run(jobDef)) {
				return throwError(errNo(6), "Error running job '" + jobName
						+ "'. \nJob Log:\n" + getJobLog(runJob.getJobId()));
			}
		} catch (Exception ex) {
			return throwError(errNo(7), ex.getMessage());
		}
		
		String jobID = runJob.getJobId();

        // Now get result directly from fact
        List snapList = new ArrayList();

        String username = getClient().getStatusDetail(jobID, false).getUsername();
        
        GridObjectInfo localUser = getGridObject(TYPE_USER,username);
        if (localUser == null){
            return throwError(errNo(9),"Unable to lookup user for login name '" + username + "'.");
        }
        
        String factName = "job.iwm.snapshots.info." + jobID;
        Fact f = localUser.getEffectiveFact(factName);

        if ( f == null){
            return throwError(errNo(10),"Can't find snapshots.\nJob Log:\n"+getJobLog(jobID));
        }
        try {
            snapList = f.getListValue();
        } catch (FactException ex) {
            return throwError(errNo(11), "Get fact error: "+ex.getMessage());
        }

        StringBuffer sb = new StringBuffer(XML_HEADER);
        sb.append("<results>\n");
        sb.append("    <result>" + 0 +"</result>\n");
        sb.append("    <snapshotSet>\n");

        for (int i=0; i< snapList.size(); i++){
            sb.append("        <item>\n");
            Map snapshotMap = (Map)snapList.get(i);
            Iterator iT = snapshotMap.keySet().iterator();

            while (iT.hasNext()) {
                String aKey = iT.next().toString();
                sb.append("            <"+aKey+">" + snapshotMap.get(aKey) + "</"+aKey+">\n");
            }
            sb.append("        </item>\n");
        }
        sb.append("    </snapshotSet>\n");
        /*
        }
        else {        //Describe certain snapshots
            ArrayList matchedSnapList = new ArrayList();
            for (int i=0; i< snapList.size(); i++){
                Map snapshotMap = (Map)snapList.get(i);
                if (SnapshotsList.contains(""+snapshotMap.get("snapshotID"))){
                    matchedSnapList.add(snapshotMap);
                }
            }

            if (matchedSnapList.size()==0){
                throw new AISRunJobPOException(errNo(6), "Found no matched snapshot");
            }
            sb.append("    <result>" + 0 +"</result>\n");
            sb.append("    <snapshotSet>\n");

            for (int i=0; i< matchedSnapList.size(); i++){
                sb.append("        <item>\n");
                Map snapshotMap = (Map)matchedSnapList.get(i);
                Iterator iT = snapshotMap.keySet().iterator();

                while (iT.hasNext()) {
                    String aKey = iT.next().toString();
                    sb.append("            <"+aKey+">" + snapshotMap.get(aKey) + "</"+aKey+">\n");
                }
                sb.append("        </item>\n");
            }
            sb.append("    </snapshotSet>\n");
        }*/
        sb.append("</results>");
        return sb.toString();
	}
}
