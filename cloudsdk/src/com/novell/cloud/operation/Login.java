/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import com.novell.cloud.api.CloudProperty;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.ServiceInstance;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.Credential;
import com.novell.zos.grid.GridException;
import com.novell.zos.toolkit.CredentialFactory;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class Login extends Op {

	private String serverAddress;
	private int port;
	private String username;
	private String password;
	private boolean cacheCredential = false;
	
	public Login() {
        super("Login","Login to the PSO", 1);
		// TODO Auto-generated constructor stub
		serverAddress = CloudProperty.getServerAddress();
		port = CloudProperty.getServerPort();
		username = CloudProperty.getDefaultUser();
		password = CloudProperty.getDefaultPassword();
	}
	
	public void setCacheCredential(boolean cache){
		this.cacheCredential = cache;
	}

	/**
	 * Pre-execute() step
	 */
	public void start() {
		// We don't want the default behavour of trying to login from
		// previous login/credentials so override.
		// Otherwise, get default behavior of "Not logged in" error
	}

	/**
	 * Overridden by commands
	 */
	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);
		if (root.getAttributes().getLength() > 0) {
			NodeList nodes0 = root.getChildNodes();
			for (int i = 0; i < nodes0.getLength(); i++) {
				Node node0 = nodes0.item(i);
				if (node0.getNodeType() == Node.ELEMENT_NODE) {
					if (node0.getNodeName().equals("accountName")) {
						NodeList nodes1 = node0.getChildNodes();
						String txt = "";
						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
							Node node1 = nodes1.item(i1);
							if (node1.getNodeType() == Node.TEXT_NODE) {
								txt = txt + node1.getNodeValue();
							}
						}
						username = txt.trim();
					} else if (node0.getNodeName().equals("accountPasswd")) {
						NodeList nodes1 = node0.getChildNodes();
						String txt = "";
						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
							Node node1 = nodes1.item(i1);
							if (node1.getNodeType() == Node.TEXT_NODE) {
								txt = txt + node1.getNodeValue();
							}
						}
						password = txt.trim();
					} else if (node0.getNodeName().equals("serverAddress")) {
						NodeList nodes1 = node0.getChildNodes();
						String txt = "";
						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
							Node node1 = nodes1.item(i1);
							if (node1.getNodeType() == Node.TEXT_NODE) {
								txt = txt + node1.getNodeValue();
							}
						}
						serverAddress = txt.trim();
					}
					else if (node0.getNodeName().equals("serverPort")) {
						NodeList nodes1 = node0.getChildNodes();
						String txt = "";
						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
							Node node1 = nodes1.item(i1);
							if (node1.getNodeType() == Node.TEXT_NODE) {
								txt = txt + node1.getNodeValue();
							}
						}
						port = Integer.parseInt(txt.trim());
					}
				}
			}
		}
		// nothing to do
		return 0;
	}

	/**
	 * Overridden by commands
	 */
	protected int checkParameters() throws Exception {
		// nothing to do
		return 0;
	}

	public String execute(String xmlData) throws GridException {
		
		try{
			getParameters(xmlData);
		}catch(Exception ex){
			return throwError(errNo(1),ex.getMessage());
		}
		
//		ServiceInstance.println("Server: " + this.serverAddress);
		
		try{
			Credential credential = CredentialFactory.newPasswordCredential(username, password.toCharArray());
			
			// Do the Login
			if(cacheCredential){
				getClient(serverAddress, port).loginAndCacheCredential(credential);
			}else{
				getClient(serverAddress, port).login(credential);
			}
	        
//	        ServiceInstance.println("Logged into " + getClient(true).getGridName() + " as " + username);
	        
		}catch(Exception ex){
			return throwError(errNo(2),ex.getMessage());			
		}
		
    	String session = serverAddress + ":" + port + ":" + username + ":" + password;
		session = ServiceInstance.UTF8BASE64(session);
		// ////// Write the response /////////////
		StringBuffer sb = new StringBuffer(XML_HEADER);
		sb.append("<results>\n");
		sb.append("    <result>" + "0" + "</result>\n");
		sb.append("    <sessionID>" + session + "</sessionID>\n");
		sb.append("</results>\n");
		// System.err.println("Output:"+sb.toString());
		return sb.toString();
	}

}
