/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Arrays;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.GridObjectNotFoundException;
import com.novell.zos.grid.WorkflowInfo;

public class CreateVolume extends Op {

    private String size = null;
    private String content = null;
    private String snapshotId = null;
    private String snapshotPath = null;
    private String isopath = null;
    private String repName = null;
    private String volumeID = null;
    private long n_size = 0;
    private long n_content = 0;
    private GridObjectInfo repo = null;
    
	public CreateVolume() {
		super("CreateVolume", "Add a volumn in a repository", 56);
	}

	public CreateVolume(ClientAgent client) {
		super(client,"CreateVolume", "Add a volumn in a repository", 56);
	}

	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);

            if (root.getAttributes().getLength()>0) {
                NodeList nodes0 = root.getChildNodes();
                for (int i = 0; i < nodes0.getLength(); i++) {
                     Node node0 = nodes0.item(i);
                     if (node0.getNodeType() == Node.ELEMENT_NODE) {
                        if (node0.getNodeName().equals("sessionID")){
                              session = getTrimmedChildText(node0);
                        } else if (node0.getNodeName().equals("size")){
                              size = getTrimmedChildText(node0);
                        } else if (node0.getNodeName().equals("content")){
                              content = getTrimmedChildText(node0);
                        } else if (node0.getNodeName().equals("snapshot")){
                            NodeList nodes1 = node0.getChildNodes();
                            for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                Node node1 = nodes1.item(i1);
                                if (node1.getNodeType() == Node.ELEMENT_NODE) {
                                    if (node1.getNodeName().equals("snapshotID")) {
                                        snapshotId = getTrimmedChildText(node1);
                                    } else if (node1.getNodeName().equals("snapshotPath")){
                                        snapshotPath = getTrimmedChildText(node1);
                                    }
                                }
                            }
                        } else if (node0.getNodeName().equals("repName")){
                            repName = getTrimmedChildText(node0);
                        } else if (node0.getNodeName().equals("isopath")){
                            isopath = getTrimmedChildText(node0);
                        } else if (node0.getNodeName().equals("volumeID")){
                            volumeID = getTrimmedChildText(node0);
                        }
                     }
                }
            }
		return 0;
	}

	protected int checkParameters() throws Exception {

        if (IsNull(size)){       //Must
            throw new Exception("Parameter error: size can't be empty.");
        }
        n_size = Long.parseLong(size);
        if ((n_size < 1)||(n_size > 1024)){
            throw new Exception("Parameter size error: 1 ~ 1024.");
        }
        
        if (IsNull(repName)){        //Must
            throw new Exception("Parameter error:  repName can't be empty.");
        }
        ////// Check if repository exists ////////////
        repo = getGridObject(TYPE_REPOSITORY,repName);
        if (repo == null){
            throw new Exception("Can't find repo:" + repName + ".");
        }
        // Can not create volume on local repository
        Fact f = repo.getEffectiveFact("repository.IWM.VG.local");
        if (f != null){
            boolean bLocal = false;
            try {
                bLocal = f.getBooleanValue();
            } catch (FactException ex){
            }
            if (bLocal){
                throw new Exception( "Can't create volume on LOCAL repository: '" + repName + "'.");
            }
        }
        if(IsNotNull(volumeID)){
            boolean isDup = false;
            String volFact = "repository.AIS.volume." + volumeID;
            try{
                Fact fs = repo.getEffectiveFact(volFact);
                if (fs!=null){
                    isDup = true;
                }
            }catch(Exception ex){}
            if(isDup){
                throw new Exception("volume: '" + volumeID + "' is already existed.");
            }
        }
        if (IsNotNull(content)){        //Optional
            n_content = Long.parseLong(content);
            if ((n_content < 0)||(n_content > 2)){
                throw new Exception("Parameter error: content must be 0, 1 or 2.");
            }
        }
        if (n_content == 1){     // Filled volume with snapshot
            if (IsNull(snapshotId)){        //Must
                throw new Exception("Parameter error: snapshotID can't be empty.");
            }
            if (IsNull(snapshotPath)){        //Must
                throw new Exception("Parameter error: snapshotPath can't be empty.");
            }
            // TODO: Check snapshotID and snapshotPath
            
        } else if (n_content ==2){    //Filled volume with iso file
            if (IsNull(isopath)){        //Must
                throw new Exception("Parameter error: isopath can't be empty.");
            }
            // TODO: Check iso path
        }
        
        return 0;
	}

	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

        String jobName = "volumemanager";
        Fact jobsFact= repo.getEffectiveFact("repository.provisioner.jobs");
        if (null==jobsFact) {
            return throwError(errNo(10),"repository.provisioner.jobs fact not set on repository: " + repName);
        }
        String[] jobs= null;
        try {
            jobs = jobsFact.getStringArrayValue();
        } catch (FactException e) {
            return throwError(errNo(11),"repository.provisioner.jobs fact has invalid type on repository: " + repName);
        }
        for (String job:jobs) {
        	if ("vsphere".equals(job)) {
                jobName = "volumemanager_vs";
                break;
        	}
        }
		JobSpec job = this.getJob(jobName);
        if (job == null){
            return throwError(errNo(3),"Job '" + jobName +"' is not deployed.");
        }
        
		JobDef jobDef = new JobDef();
		jobDef.setName(jobName);
		jobDef.setJob(job);
		jobDef.resetImports();
        jobDef.setImport("jobargs.action", "create");
        
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("size",n_size);
        params.put("content",n_content);
        params.put("repName",repName);
        if (n_content ==1){
            Map<String,String> snapshot = new HashMap<String,String>();
            snapshot.put("snapshotID", snapshotId);
            snapshot.put("snapshotPath", snapshotPath);
            params.put("snapshot",snapshot);
        } else if (n_content ==2){
            params.put("isopath", isopath);
        }
        if(IsNotNull(volumeID)){
            params.put("volumeID",volumeID);
        }
        jobDef.setImport("jobargs.params", params);
		jobDef.setWhatComplete(true);
		jobDef.setWaitForCompletion(false);

		RunJob runJob = new RunJob(getClient());
		try {
			if (!runJob.run(jobDef)) {
				return throwError(errNo(4), "Error running job '" + jobName
						+ "'. \nJob Log:\n" + getJobLog(runJob.getJobId()));
			}
		} catch (Exception ex) {
			return throwError(errNo(5), ex.getMessage());
		}

        String jobID = runJob.getJobId();

        ////  get vol-snapshotID
        if(IsNull(volumeID)){
            String newVolumeId = null;
            String fname = "repository.AIS.jobvolume."+jobID;
            Fact f = null;
            int ii = 0;
            int state = 0;

            do{
                state = getClient().getStatus(jobID);
                f = repo.getEffectiveFact(fname);
                if (f!=null){
                    try {
                        newVolumeId = f.getStringValue();
                    } catch (FactException ex){}
                   if (IsNotNull(newVolumeId)){
                        break;
                    }
                }
    
                ii ++;
                if (ii > 1000){
                    break;      //timeout
                }
    
                if (state == WorkflowInfo.CANCELLED_STATE || state == WorkflowInfo.FAILED_STATE || state == WorkflowInfo.COMPLETED_STATE){
                    break;      // job paused, completed, cancelled or failed.
                }
    
                try {
                    Thread.currentThread(); Thread.sleep(300L);
                } catch (InterruptedException   e)   {}
            }while(true);
    
            if (IsNull(newVolumeId)){
                return throwError(errNo(9),"Volume creation failed.\nJob Log:\n" + getJobLog(jobID));
            }
            volumeID = newVolumeId;
        }

        StringBuffer sb = new StringBuffer(XML_HEADER);
        sb.append("<results>\n");
        sb.append("    <result>" + 0 +"</result>\n");
        sb.append("    <jobID>" + jobID +"</jobID>\n");
        sb.append("    <volumeID>" + volumeID + "</volumeID>\n");
        sb.append("</results>\n");
        
        return sb.toString();
	}
}
