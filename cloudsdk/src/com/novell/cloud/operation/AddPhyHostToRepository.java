/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.util.ArrayList;
import java.util.HashMap;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.GridObjectNotFoundException;

public class AddPhyHostToRepository extends Op {

	private String repName = null;
	private String paName = null;
	private ArrayList<String> hostNameList = new ArrayList<String>();

	public AddPhyHostToRepository() {
		super("AddPhyHostToRepository", "Add a vm host to repository", 13);
	}

	public AddPhyHostToRepository(ClientAgent client) {
		super(client, "AddPhyHostToRepository", "Add a vm host to repository",
				13);
	}

	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);

		if (root.getAttributes().getLength() > 0) {
			NodeList nodes0 = root.getChildNodes();
			for (int i = 0; i < nodes0.getLength(); i++) {
				Node node0 = nodes0.item(i);
				if (node0.getNodeType() == Node.ELEMENT_NODE) {
					if (node0.getNodeName().equals("physicalSet")) {
						NodeList nodes1 = node0.getChildNodes();
						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
							Node node1 = nodes1.item(i1);
							if (node1.getNodeType() == Node.ELEMENT_NODE) {
								if (node1.getNodeName().equals("item")) {
									NodeList nodes2 = node1.getChildNodes();
									for (int i2 = 0; i2 < nodes2.getLength(); i2++) {
										Node node2 = nodes2.item(i2);
										if (node2.getNodeType() == Node.ELEMENT_NODE) {
											if (node2.getNodeName().equals(
													"hostName")) {
												NodeList nodes3 = node2
														.getChildNodes();
												String txt = "";
												for (int i3 = 0; i3 < nodes3
														.getLength(); i3++) {
													Node node3 = nodes3
															.item(i3);
													if (node3.getNodeType() == Node.TEXT_NODE) {
														txt = txt
																+ node3.getNodeValue();
													}
												}
												hostNameList.add(txt.trim());
											}
										}
									}
								}
							}
						}
					} else if (node0.getNodeName().equals("repName")) {
						NodeList nodes1 = node0.getChildNodes();
						String txt = "";
						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
							Node node1 = nodes1.item(i1);
							if (node1.getNodeType() == Node.TEXT_NODE) {
								txt = txt + node1.getNodeValue();
							}
						}
						repName = txt.trim();
					} else if (node0.getNodeName().equals("sessionID")) {
						NodeList nodes1 = node0.getChildNodes();
						String txt = "";
						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
							Node node1 = nodes1.item(i1);
							if (node1.getNodeType() == Node.TEXT_NODE) {
								txt = txt + node1.getNodeValue();
							}
						}
						session = txt.trim();
					}
				}
			}
		}

		return 0;
	}

	protected int checkParameters() throws Exception {

		if (IsNull(repName)) {
			throw new Exception("Parameter error: repName can't be empty.");
		}

		// check if the repository exists
		GridObjectInfo repository = null;
		try {
			repository = getClient().getGridObjectInfo(this.TYPE_REPOSITORY,
					repName);
		} catch (GridObjectNotFoundException ex) {
			throw new Exception("No such repository exists: " + repName);
		}

		if (hostNameList.isEmpty()) {
			throw new Exception("Parameter error: hostName can't be empty.");
		}

		// //// Check if the host exists //////////
		String[] hostNames = (String[]) hostNameList.toArray(new String[0]);
		for (int i = 0; i < hostNames.length; i++) {
			String hostname = hostNames[i];
			try {
				GridObjectInfo nodeInfo = getClient().getGridObjectInfo(
						this.TYPE_RESOURCE, hostname);
				if (!nodeInfo.getEffectiveFact("resource.type")
						.getStringValue()
						.equalsIgnoreCase(this.TYPE_FIXED_PHYSICAL)) {
					throw new Exception("Not a physical host: " + hostname);
				}
				// check if there is a pa
				String[] vmhostNames = nodeInfo.getEffectiveFact(
						"resource.vmhosts").getStringArrayValue();
				if (vmhostNames.length == 0) {
					throw new Exception("Not vmhost in the host: " + hostname);
				}

				GridObjectInfo vmhost = getClient().getGridObjectInfo(
						this.TYPE_VMHOST, vmhostNames[0]);
				String temp_pa = vmhost.getEffectiveFact(
						"vmhost.provisioner.job").getStringValue();
				if (paName == null) {
					paName = temp_pa;
				} else if (!paName.equalsIgnoreCase(temp_pa)) {
					throw new Exception(
							"Not the same privisoner name for all the physical hosts");
				}
				String[] repoNames = nodeInfo.getEffectiveFact(
						"resource.repositories").getStringArrayValue();
				ArrayList<String> repoList = new ArrayList<String>();
				for (int k = 0; k < repoNames.length; k++) {
					repoList.add(repoNames[k]);
				}
				if (repoList.contains(repName)) {
					hostNameList.remove(hostNames[i]); // remove duplate host
														// from hostNameList
				}
			} catch (GridObjectNotFoundException ex) {
				throw new Exception("No such physical host exists: " + hostname);
			}
		}

		return 0;
	}

	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

		if (!hostNameList.isEmpty()) {
			String jobName = "repomanager";

			if ("vsphere".equals(paName)) {
				jobName = "repomanager_vs";
			}

			JobSpec job = this.getJob(jobName);
			// //// Call job to create repository /////////
			// //// Call job to associate repository with vmhost /////////
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("repository", repName);
			params.put("hosts", hostNameList);

			JobDef jobDef = new JobDef();
			jobDef.setName(jobName);
			jobDef.setJob(job);
			jobDef.resetImports();
			jobDef.setImport("jobargs.action", "addhost");
			jobDef.setImport("jobargs.params", params);
			jobDef.setWhatComplete(true);
			jobDef.setWaitForCompletion(true);

			RunJob runJob = new RunJob(getClient());
			try {
				if (!runJob.run(jobDef)) {
					return throwError(errNo(3), "Error running job '" + jobName
							+ "'. \nJob Log:\n" + getJobLog(runJob.getJobId()));
				}
			} catch (Exception ex) {
				return throwError(errNo(4), ex.getMessage());
			}

		}

		String strCon = "<and>\n<eq fact=\"resource.type\" value=\"Fixed Physical\"/>\n</and>\n";
		strCon += "<and>\n<contains fact=\"resource.repositories\" value=\""
				+ repName + "\"/>\n</and>\n";

		GridObjectInfo[] phyHostArray = null;
		try {
			phyHostArray = getClient().getGridObjects(this.TYPE_RESOURCE,
					strCon, true);
		} catch (Exception ex) {
			return throwError(errNo(4),
					"Error query the physical hosts: " + ex.getMessage());
		}

		StringBuffer sb = new StringBuffer(XML_HEADER);
		sb.append("<results>\n");
		sb.append("    <result>" + 0 + "</result>\n");

		if ((phyHostArray != null) && (phyHostArray.length > 0)) {
			sb.append("    <repHostSet>\n");
			for (int i = 0; i < phyHostArray.length; ++i) {
				sb.append("        <item>\n");
				try {
					sb.append("            <hostName>"
							+ phyHostArray[i].getEffectiveFact("resource.id")
									.getValue() + "</hostName>\n");
				} catch (com.novell.zos.grid.FactException ex) {
				}
				sb.append("        </item>\n");
			}
			sb.append("    </repHostSet>\n");
		}

		sb.append("</results>\n");
		return sb.toString();
	}

}
