/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.GridObjectNotFoundException;

public class DeregisterVMTemplate extends Op {

    private static final String RESOURCE_VM_IWM_TEMPLATE_REGISTERED = "resource.vm.IWM.template.registered";
    private String vmTemplateID = null;
    private String delTemplate = null;
    private boolean bDelTemplate = false;
    private GridObjectInfo vmNode = null;

	public DeregisterVMTemplate() {
		super("DeregisterVMTemplate", "UnRegister a vm template", 22);
	}

	public DeregisterVMTemplate(ClientAgent client) {
		super(client, "DeregisterVMTemplate", "UnRegister a vm template", 22);
	}

	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);

            if (root.getAttributes().getLength()>0) {
                NodeList nodes0 = root.getChildNodes();
                for (int i = 0; i < nodes0.getLength(); i++) {
                     Node node0 = nodes0.item(i);
                     if (node0.getNodeType() == Node.ELEMENT_NODE) {
                        if (node0.getNodeName().equals("sessionID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              session = txt.trim();
                        } else if (node0.getNodeName().equals("vmTemplateID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              vmTemplateID = txt.trim();
                        } else if (node0.getNodeName().equals("delTemplate")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              delTemplate = txt.trim();
                        }
                     }
                }
            }
		return 0;
	}

	protected int checkParameters() throws Exception {

        if (IsNull(vmTemplateID)){
            throw new Exception("Parameter error: vmTemplateID can't be empty.");
        }
        if (delTemplate != null){  // delTemplate optional
            delTemplate = delTemplate.toLowerCase();
            if (delTemplate.equals("false")){
                bDelTemplate = false;
            } else if (delTemplate.equals("true")){
                bDelTemplate = true;
            } else{
                throw new Exception( "Parameter error: delTemplate must be true or false.");
            }
        }
        
        ////// Check if template exists //
        try{
            vmNode = getClient().getGridObjectInfo(TYPE_RESOURCE,vmTemplateID);
        }catch(GridObjectNotFoundException ex) {
        }

        if (vmNode == null){
            throw new Exception("Template '"+vmTemplateID+"' doesn't exist.");
        }
		if (! vmNode.getEffectiveFact("resource.type").getStringValue().equalsIgnoreCase(TYPE_VM_TEMPLATE)){
			throw new Exception("Resource: "
					+ vmTemplateID + " is not a vm template");
		}

        Fact f = vmNode.getEffectiveFact(RESOURCE_VM_IWM_TEMPLATE_REGISTERED);
        if (f==null){
            throw new Exception("Template '"+vmTemplateID +"' is not registered.");
        }
        try {
            if (!f.getBooleanValue()){
                throw new Exception("Template '"+vmTemplateID +"' is not registered.");
            }
        } catch(FactException ex) {
            throw new Exception("Get fact error: " + ex.getMessage());
        }
        
		return 0;
	}

	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

        if(bDelTemplate == false){
            getClient().setFact(TYPE_RESOURCE,vmTemplateID,RESOURCE_VM_IWM_TEMPLATE_REGISTERED,com.novell.zos.grid.Fact.TYPE_BOOLEAN, false);
        }
        
        // alway call vmtemplate job
		String jobName = "vmtemplate";
		JobSpec job = this.getJob(jobName);

		JobDef jobDef = new JobDef();
		jobDef.setName(jobName);
		jobDef.setJob(job);
		jobDef.resetImports();
        jobDef.setImport("jobargs.templateId", vmTemplateID);
        jobDef.setImport("jobargs.delTemplate", bDelTemplate);
		jobDef.setWhatComplete(true);
		jobDef.setWaitForCompletion(true);

		RunJob runJob = new RunJob(getClient());
		try {
			if (!runJob.run(jobDef)) {
				return throwError(errNo(3), "Error running job '" + jobName
						+ "'. \nJob Log:\n" + getJobLog(runJob.getJobId()));
			}
		} catch (Exception ex) {
			return throwError(errNo(4), ex.getMessage());
		}
		
        StringBuffer sb = new StringBuffer(XML_HEADER);
        sb.append("<results>\n");
        sb.append("    <result>"+ 0 +"</result>\n");
        sb.append("</results>");
        return sb.toString();

	}
}
