/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.util.ArrayList;
import java.util.HashMap;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.GridObjectNotFoundException;

public class AddVGToRepository extends Op {
	private String repName = null;
	private String vgName = null;
    private String vgUUID = null;
    private String paName = null;

	public AddVGToRepository() {
		super("AddVGToRepository",
				"Add volumn groups to a existed repository", 16);
	}

	public AddVGToRepository(ClientAgent client) {
		super(client, "AddVGToRepository",
				"Add volumn groups to a existed repository", 16);
	}

	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);
		if (root.getAttributes().getLength()>0) {
            NodeList nodes0 = root.getChildNodes();
            for (int i = 0; i < nodes0.getLength(); i++) {
                 Node node0 = nodes0.item(i);
                 if (node0.getNodeType() == Node.ELEMENT_NODE) {
                    if (node0.getNodeName().equals("sessionID")){
                          NodeList nodes1 = node0.getChildNodes();
                          String txt="";
                          for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                               Node node1 = nodes1.item(i1);
                               if (node1.getNodeType() == Node.TEXT_NODE) {
                               txt = txt + node1.getNodeValue();
                               }
                          }
                          session = txt.trim();
                    } else if (node0.getNodeName().equals("repName")){
                          NodeList nodes1 = node0.getChildNodes();
                          String txt="";
                          for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                               Node node1 = nodes1.item(i1);
                               if (node1.getNodeType() == Node.TEXT_NODE) {
                               txt = txt + node1.getNodeValue();
                               }
                          }
                          repName = txt.trim();
                    } else if (node0.getNodeName().equals("vgName")){
                          NodeList nodes1 = node0.getChildNodes();
                          String txt="";
                          for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                               Node node1 = nodes1.item(i1);
                               if (node1.getNodeType() == Node.TEXT_NODE) {
                               txt = txt + node1.getNodeValue();
                               }
                          }
                          vgName = txt.trim();
                    } else if (node0.getNodeName().equals("vgUUID")){
                          NodeList nodes1 = node0.getChildNodes();
                          String txt="";
                          for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                               Node node1 = nodes1.item(i1);
                               if (node1.getNodeType() == Node.TEXT_NODE) {
                               txt = txt + node1.getNodeValue();
                               }
                          }
                          vgUUID = txt.trim();
                    }
                 }
            }
        }
		return 0;
	}

	protected int checkParameters() throws Exception {

		if (IsNull(repName)) {
			throw new Exception("Parameter error: repName can't be empty.");
		}

		GridObjectInfo repository = null;
		try {
			repository = getClient().getGridObjectInfo(this.TYPE_REPOSITORY,
					repName);
		} catch (GridObjectNotFoundException ex) {
			throw new Exception("No such repository exists: " + repName);
		}
		
		if(vgName == null || vgName.length() == 0){
			throw new Exception("vgName is null");
		}
		
		if(vgUUID == null || vgUUID.length() == 0){
			throw new Exception("vgName is null");
		}
		
		String[] paNames = repository.getEffectiveFact("repository.provisioner.jobs").getStringArrayValue();
		if(paNames.length > 0){
			paName = paNames[0];
		}		
	////// Check if VG exists in the repository //////////
        Fact f = repository.getEffectiveFact("repository.SAN.VGs");
        ArrayList<String> vgList = new ArrayList<String>();
        String[] vgNames = null;
        if (f!=null){
        	vgNames = f.getStringArrayValue();
            for (int ii=0; ii<vgNames.length; ii++){
                vgList.add(vgNames[ii]);
            }
            if (vgList.contains(vgName)){
                throw new Exception("VG '"+vgName+"' is already in repository: "+repName);
            }
        }

		return 0;
	}

	public String execute(String xmlData) throws GridException {
		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}
		
		String jobName = "repomanager";
		if(paName != null && "vsphere".equalsIgnoreCase(paName)){
			jobName = "repomanager_vs";
		}
		
		JobSpec job = this.getJob(jobName);
		// //// Call job to create repository /////////
		// //// Call job to associate repository with vmhost /////////
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("repository", repName);
		params.put("vgname", vgName);
        params.put("vgUUID", vgUUID);

		JobDef jobDef = new JobDef();
		jobDef.setName(jobName);
		jobDef.setJob(job);
		jobDef.resetImports();
		jobDef.setImport("jobargs.action", "addvg");
		jobDef.setImport("jobargs.params", params);
		jobDef.setWhatComplete(true);
		jobDef.setWaitForCompletion(true);

		RunJob runJob = new RunJob(getClient());
		try {
			if (!runJob.run(jobDef)) {
				return throwError(errNo(3), "Error running job '" + jobName
						+ "'. \nJob Log:\n" + getJobLog(runJob.getJobId()));
			}
		} catch (Exception ex) {
			return throwError(errNo(4), ex.getMessage());
		}


		GridObjectInfo repository = null;
		try {
			repository = getClient().getGridObjectInfo(this.TYPE_REPOSITORY,
					repName);
		} catch (GridObjectNotFoundException ex) {
			return throwError(errNo(4), "No such repository exists: " + repName);
		}
		
		Fact f = repository.getEffectiveFact("repository.SAN.VGs");
		ArrayList<String> vgList = new ArrayList<String>();
        if (f!=null){
            String[] vgNames = null;
            try{
                vgNames = (String[])f.getValue();
            } catch (com.novell.zos.grid.FactException ex){
                System.err.println(ex.getMessage());
            }
            for (int ii=0; ii<vgNames.length; ii++){
                vgList.add(vgNames[ii]);
            }
            if (vgList.contains(vgName)){
                StringBuffer sb = new StringBuffer(XML_HEADER);
                sb.append("<results>\n");
                sb.append("    <result>"+ 0 +"</result>\n");
                sb.append("</results>");
                return sb.toString();
            }else{
            	return throwError(errNo(6), "Not found '"+vgName+"' in repository '"+repName+"' failed.\nJob Log:\n" + getJobLog(runJob.getJobId()));
            }            
        }
        
        return throwError(errNo(7), "Register VG '"+vgName+"' to repository '"+repName+"' failed.\nJob Log:\n" + getJobLog(runJob.getJobId()));
	}


}
