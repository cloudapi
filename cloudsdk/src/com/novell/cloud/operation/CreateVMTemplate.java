/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.GridObjectNotFoundException;

public class CreateVMTemplate extends Op {

    private String instanceID = null;
    private String description = null;
    private String repName = null;

	public CreateVMTemplate() {
		super("CreateVMTemplate", "Create a new template base on a vm instance", 20);
	}

	public CreateVMTemplate(ClientAgent client) {
		super(client, "CreateVMTemplate", "Create a new template base on a vm instance", 20);
	}

	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);

            if (root.getAttributes().getLength()>0) {
                NodeList nodes0 = root.getChildNodes();
                for (int i = 0; i < nodes0.getLength(); i++) {
                     Node node0 = nodes0.item(i);
                     if (node0.getNodeType() == Node.ELEMENT_NODE) {
                        if (node0.getNodeName().equals("sessionID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              session = txt.trim();
                        } else if (node0.getNodeName().equals("instanceID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              instanceID = txt.trim();
                        } else if (node0.getNodeName().equals("description")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              description = txt.trim();
                        } else if (node0.getNodeName().equals("repName")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              repName = txt.trim();
                        }
                     }
                }
            }

		return 0;
	}

	protected int checkParameters() throws Exception {

		if (IsNull(instanceID)) {
			throw new Exception("Parameter error: instanceID can't be empty.");
		}

		GridObjectInfo repository = null;
		try {
			repository = getClient().getGridObjectInfo(this.TYPE_REPOSITORY,
					repName);
		} catch (GridObjectNotFoundException ex) {
			throw new Exception("No such repository exists: " + repName);
		}

		GridObjectInfo vmNode = null;
		try {
			vmNode = getClient().getGridObjectInfo(this.TYPE_RESOURCE,
					instanceID);
		} catch (GridObjectNotFoundException ex) {
		}
        
        if( vmNode == null ){
			throw new Exception("No such vm exists: " + instanceID);
        }

		//check if it is a vm instance
		if (! vmNode.getEffectiveFact("resource.type").getStringValue().equalsIgnoreCase(TYPE_VM_INSTANCE)){
			throw new Exception("Resource: "
					+ instanceID + " is not a vm instance");
		}

        Fact f = vmNode.getEffectiveFact("resource.vm.underconstruction");
		if (f != null && f.getBooleanValue()){
			throw new Exception("Resource: "
					+ instanceID + " is under construction");
		}
			
		return 0;
	}

	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

		String jobName = "vmtemplate";
		JobSpec job = this.getJob(jobName);

		JobDef jobDef = new JobDef();
		jobDef.setName(jobName);
		jobDef.setJob(job);
		jobDef.resetImports();
        jobDef.setImport("jobargs.vmname", instanceID);
        jobDef.setImport("jobargs.description", description);
        jobDef.setImport("jobargs.reponame", repName);
		jobDef.setWhatComplete(true);
		jobDef.setWaitForCompletion(false);

		RunJob runJob = new RunJob(getClient());
		try {
			if (!runJob.run(jobDef)) {
				return throwError(errNo(3), "Error running job '" + jobName
						+ "'. \nJob Log:\n" + getJobLog(runJob.getJobId()));
			}
		} catch (Exception ex) {
			return throwError(errNo(4), ex.getMessage());
		}

        String jobID = runJob.getJobId();

        String strCon = "<and>\n<eq fact=\"resource.type\" value=\"VM Template\"/>\n</and>\n";
        strCon += "<and>\n<eq fact=\"resource.vm.iwm.initial.job\" value=\"" + jobID + "\"/>\n</and>\n";

        GridObjectInfo[] nodeInfos = null;
        try{
        	nodeInfos = GetInfoFromJob(strCon,jobID);
        }catch( Exception ex){
        	return throwError(errNo(5), "Timeout to query the tempalte id from the job.\nJob Log:\n"+getJobLog(jobID) + "\n jobID:" + jobID);
        }
        
        if(nodeInfos == null || nodeInfos.length == 0){
			return throwError(errNo(5), "Create VmTemplate from instance '"+instanceID+"' failed.\nJob Log:\n"+getJobLog(jobID) + "\n jobID:" + jobID);
        }

        String vmTemplateID = null;
        try {
            vmTemplateID = nodeInfos[0].getEffectiveFact("resource.id").getStringValue();
        } catch (FactException ex) {
            return throwError(errNo(11),"Get fact error: "+ex.getMessage());
        }

        StringBuffer sb = new StringBuffer(XML_HEADER);
        sb.append("<results>\n");
        sb.append("    <result>"+ 0 +"</result>\n");          // result
        sb.append("    <jobID>" + jobID + "</jobID>\n");
        sb.append("    <vmTemplateID>" + vmTemplateID + "</vmTemplateID>\n");
        sb.append("</results>");

		return sb.toString();
	}
}
