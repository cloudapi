/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */
package com.novell.cloud.operation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.zos.constraint.TypedConstraint;
import com.novell.zos.grid.AgentListener;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.FactSet;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.Message;
import com.novell.zos.grid.WorkflowInfo;
import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.ServiceInstance;
import com.novell.cloud.api.XMLHelper;

public class RunJob extends Op implements AgentListener {

	String jobid = null; // Save the response from runJob.
	Throwable jobError = null;

	String _jobName = null;
	HashMap _jobArgs = null;
	String _policyName = null;
	String _jobPriority = "default";
	boolean _waitForCompletion = false;
	boolean _listenMessage = false;

	public RunJob() {
		super("RunJob", "Run a Job", 100);
	}
	
	public RunJob(ClientAgent client){
		super(client, "RunJob", "Run a Job", 100);		
	}
	
	public String getJobId(){
		return jobid;
	}
	
	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);
		if (root.getAttributes().getLength() > 0) {
			NodeList nodes0 = root.getChildNodes();
			for (int i = 0; i < nodes0.getLength(); i++) {
				Node node0 = nodes0.item(i);
				if (node0.getNodeType() == Node.ELEMENT_NODE) {
					if (node0.getNodeName().equals("sessionID")) {
						NodeList nodes1 = node0.getChildNodes();
						String txt = "";
						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
							Node node1 = nodes1.item(i1);
							if (node1.getNodeType() == Node.TEXT_NODE) {
								txt = txt + node1.getNodeValue();
							}
						}
						session = txt.trim();
					} else if (node0.getNodeName().equals("jobName")) {
						NodeList nodes1 = node0.getChildNodes();
						String txt = "";
						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
							Node node1 = nodes1.item(i1);
							if (node1.getNodeType() == Node.TEXT_NODE) {
								txt = txt + node1.getNodeValue();
							}
						}
						_jobName = txt.trim();
					} else if (node0.getNodeName().equals("jobArgs")) {
						_jobArgs = parseJobArgs(node0);

					} else if (node0.getNodeName().equals("jobPriority")) {
						NodeList nodes1 = node0.getChildNodes();
						String txt = "";
						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
							Node node1 = nodes1.item(i1);
							if (node1.getNodeType() == Node.TEXT_NODE) {
								txt = txt + node1.getNodeValue();
							}
						}
						_jobPriority = txt.trim();
					} else if (node0.getNodeName().equals("jobPolicyName")) {
						NodeList nodes1 = node0.getChildNodes();
						String txt = "";
						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
							Node node1 = nodes1.item(i1);
							if (node1.getNodeType() == Node.TEXT_NODE) {
								txt = txt + node1.getNodeValue();
							}
						}
						_policyName = txt.trim();
					} else if (node0.getNodeName().equals("waitForCompletion")) {
						NodeList nodes1 = node0.getChildNodes();
						String txt = "";
						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
							Node node1 = nodes1.item(i1);
							if (node1.getNodeType() == Node.TEXT_NODE) {
								txt = txt + node1.getNodeValue();
							}
						}
						txt = txt.trim();
						if (txt.length() > 0) {
							_waitForCompletion = Boolean.parseBoolean(txt);
						}
					}
				}
			}
		}
		return 0;
	}

	private HashMap<String, Object> parseJobArgs(Node e) throws Exception {
		HashMap params = new HashMap<String, Object>();
		NodeList nodes = e.getChildNodes();
		int count = nodes.getLength();
		for (int i = 0; i < count; i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				String name = getAttribute(node, "name");
				String type = getAttribute(node, "type");
				if (IsNull(name) || IsNull(type)) {
					throw new Exception("Fail to get the attributs: '" + name
							+ "' or '" + type + "'");
				}			
				String value = getAttribute(node, "value");
				if (type.equalsIgnoreCase("list")) {
					List list = parstList(node);
					params.put(name, list);
				} else if (type.equalsIgnoreCase("dictionary")) {
					HashMap subParams = parseJobArgs(node);
					params.put(name, subParams);
				} else if (type.equalsIgnoreCase("boolean")) {
					params.put(name, Boolean.valueOf(value.trim()));
				} else if (type.equalsIgnoreCase("Integer")) {
					params.put(name, Integer.valueOf(value.trim()));
				} else if (type.equalsIgnoreCase("float")) {
					params.put(name, Float.valueOf(value.trim()));
				} else if (type.equalsIgnoreCase("double")) {
					params.put(name, Double.valueOf(value.trim()));
				} else if (type.equalsIgnoreCase("string")) {
					params.put(name, value.trim());
				} else {
					params.put(name, value.trim());
				}
			}
			
		}
		return params;
	}

	private List parstList(Node e) throws Exception {
		List list = new ArrayList();
		NodeList nodes = e.getChildNodes();
		int count = nodes.getLength();
		for (int i = 0; i < count; i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				String type = getAttribute(node, "type");
				if (IsNull(type)) {
					throw new Exception("Fail to get the attributs: '" + type + "'");
				}
				String value = getAttribute(node, "value");
				if (type.equalsIgnoreCase("list")) {
					List subList = parstList(node);
					list.add(subList);
				} else if (type.equalsIgnoreCase("dictionary")) {
					HashMap subParams = parseJobArgs(node);
					list.add(subParams);
				} else if (type.equalsIgnoreCase("boolean")) {
					list.add(Boolean.valueOf(value.trim()));
				} else if (type.equalsIgnoreCase("Integer")) {
					list.add(Integer.valueOf(value.trim()));
				} else if (type.equalsIgnoreCase("float")) {
					list.add(Float.valueOf(value.trim()));
				} else if (type.equalsIgnoreCase("double")) {
					list.add(Double.valueOf(value.trim()));
				} else if (type.equalsIgnoreCase("string")) {
					list.add(value.trim());
				} else {
					list.add(value.trim());
				}
			}
			
		}
		return list;
	}

	private String getAttribute(Node node, String name) {
		NamedNodeMap attrs = node.getAttributes();
		if (attrs == null)
			return null;
		Node attr = attrs.getNamedItem(name);
		if (attr != null)
			return attr.getNodeValue();
		return null;
	}

	protected int checkParameters() throws Exception {
		if (IsNull(_jobName)) {
			throw new Exception("Parameter error: _jobName can't be empty.");
		}
		return 0;
	}

	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}
		
		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}
		
		try {
			JobSpec job = getJob(_jobName);
			if (job == null){
				return throwError(errNo(3), "Could not find the job: " + _jobName);
	        }
			JobDef jobDef = new JobDef();
			jobDef.setName(_jobName);
	        jobDef.setJob(job);
	        jobDef.resetImports();
			if(_jobArgs != null){
				Set keys = _jobArgs.keySet();
				Iterator iter = keys.iterator();
				while (iter.hasNext()) {
					String name = (String)iter.next();
					Object value =  _jobArgs.get(name);
					name = "jobargs." + name;
					jobDef.setImport(name,value);
				}				
			}
			if (_jobPriority != null)
				jobDef.setPriority(_jobPriority);
			if (_policyName != null){
				jobDef.setPolicyName(_policyName);
			}
			jobDef.setWhatComplete(true);
			jobDef.setWaitForCompletion(this._waitForCompletion);
			
			boolean success = run(jobDef);
			if( !success ){
				return throwError(errNo(4), "Error running job '" + _jobName +"'. \nJob Log:\n"+ getJobLog(jobid));
			}
			
		} catch (Exception ex) {
			return throwError(errNo(5), ex.getMessage());
		}
		
		StringBuffer sb = new StringBuffer(XML_HEADER);
		sb.append("<results>\n");
		sb.append("    <result>0</result>\n");
		sb.append("    <jobID>" + getJobId() + "</jobID>\n");
		sb.append("</results>");

		return sb.toString();
	}

	@Override
	public void handleMessage(Message msg) {
		boolean done = false;
		String displayString = null;
		displayString = msg.getTypeString();

		ServiceInstance.println("Message Received: " + displayString);
		if (msg instanceof Message.JobError) {
			done = true;
		} else if (msg instanceof Message.ServerStatus) {
			synchronized (jobid) {
				jobError = ((Message.ServerStatus) msg).getCause();
			}
			done = true;
		} else if (msg instanceof Message.JobFinished
				&& ((Message.JobFinished) msg).getJobID().equals(jobid)) {

			// don't stop if subjob finished
			done = true;
		}
		if (done && jobid != null ) {
			synchronized (jobid) {
				jobid.notifyAll();
			}
		}
	}

	public boolean run(JobDef jobDef) throws Exception {

		JobSpec jobSpec = jobDef.getJob();
		String jobName = jobDef.getJobName();

		// Handle imports...
		// Handle imports...
        HashMap params = new HashMap();
        FactSet paramsSet = jobSpec.getParameters();
        Iterator iter = paramsSet.iterator();
        while (iter.hasNext()) {
            Fact f = (Fact)iter.next();
            String importName = f.getName();

            // See if the import overridden defined in job definition
            Object customValue = jobDef.getImportValue(importName);

            /// XXX Does this have to be a String object?
            Object objectValue = null;
            try {
                Object factValue = f.getValue();
                if (factValue == null) {
                    factValue = "";
                }
                if (f.isRequired()) {
                    objectValue = (customValue != null)? customValue : factValue;
                } else {
                    objectValue = (customValue != null)? customValue : factValue;                        
                }
            } catch(FactException fe) {
                throw new Exception("Failed to setup Job: " + jobName + ": " + fe.getMessage());
            }
            
            if (objectValue != null) {
                params.put(importName, objectValue);
			} else {
				if (f.isRequired()) {
					throw new Exception("Failed to setup Job: " + jobName
							+ ": " + "Job parameter '" + importName
							+ "' was not defined.");
				}
			}
		}

		// Handle other job attributes...
		int priority = jobDef.getWhenPriorityValue();
		long startTime = jobDef.getStartTime();
		Map env = new HashMap(); // TODO - yikes, new ui needed
		String policyText = jobDef.getPolicy();
		String policyName = jobDef.getPolicyName();
		TypedConstraint constraint = null;
		this._waitForCompletion = jobDef.getWaitForCompletion();

		jobid = getClient().runJob(jobName, jobName, params, priority,
				startTime, env, policyText, policyName, constraint, false);

		// if job was not deployed, no jobid will be created
		if (_listenMessage && jobid != null) {
			// Request that all job messages get forwarded to me.
			getClient().getMessages(jobid);

			// Don't exit, but stay up and listen for messages
			synchronized (jobid) {
				try {
					jobid.wait();
				} catch (InterruptedException ie) {
				}
			}
			
			// Continue so we give the user a chance to see the log
			// file if they specified that as well!
			getClient().removeAgentListener(this);
		}
		
//		synchronized (jobid) {
//			if (jobError != null) {
//				if (jobError instanceof GridException) {
//					throw (GridException) jobError;
//				}
//				throw new GridException(
//						"Error while waiting for job messages: " + jobError,
//						jobError);
//			}
//		}
		
		if(jobid != null && this._waitForCompletion){
			//WorkflowInfo workflow = getClient().getStatusDetail(jobid, false);
			int status = getClient().getStatus(jobid);
			while (status != WorkflowInfo.CANCELLED_STATE && status != WorkflowInfo.COMPLETED_STATE && status != WorkflowInfo.FAILED_STATE ) {
			//while(workflow.isActive()){
				 try{
					 Thread.sleep(250);
				 }catch(InterruptedException ie){
					 ServiceInstance.println("cancelled");
				 }
				 status = getClient().getStatus(jobid);
			 }
			//System.out.println("job status: " + WorkflowInfo.stateString[status] + "; status value:" + status);
			return status == WorkflowInfo.COMPLETED_STATE;			
			 
		}
		return jobid != null;
	}

}
