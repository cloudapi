/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;

public class GetInstanceListOfHost extends Op {
    private String instanceID = null;
    private String hostName = null;
    private String ipAddress = null;
    private GridObjectInfo[] arrayHostNode = null;

	public GetInstanceListOfHost() {
		super("GetInstanceListOfHost", "Get the vm instance list in the host", 38);
	}

	public GetInstanceListOfHost(ClientAgent client) {
		super(client, "GetInstanceListOfHost", "Get the vm instance list in the host", 38);
	}

	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);

            if (root.getAttributes().getLength()>0) {
                NodeList nodes0 = root.getChildNodes();
                for (int i = 0; i < nodes0.getLength(); i++) {
                     Node node0 = nodes0.item(i);
                     if (node0.getNodeType() == Node.ELEMENT_NODE) {
                        if (node0.getNodeName().equals("sessionID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              session = txt.trim();
                        } else if (node0.getNodeName().equals("hostName")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              hostName = txt.trim();
                        } else if (node0.getNodeName().equals("ipAddress")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              ipAddress = txt.trim();
                        }
                     }
                }
            }
		return 0;
	}

	protected int checkParameters() throws Exception {

        if ((hostName == null)||hostName.equals("")){
            throw new Exception("Parameter error: hostName can't be empty.");
        }
        
        StringBuffer strCon = new StringBuffer("<and>\n<eq fact=\"resource.type\" value=\"Fixed Physical\"/>\n<or>\n");
        strCon.append("<eq fact=\"resource.id\" value=\"" + hostName + "\"/>\n");
        strCon.append("<eq fact=\"resource.ip\" value=\"" + ipAddress + "\"/>\n");
        strCon.append("</or>\n</and>\n");
        try {
            arrayHostNode = getClient().getGridObjects(TYPE_RESOURCE,strCon.toString(),true);
        } catch(Exception ex) {
            System.err.println("Error: "+ex.getMessage());
            throw new Exception("Get node error. "+ex.getMessage());
        }
        if ((arrayHostNode == null) ||(arrayHostNode.length == 0)){
            throw new Exception( "Can't find hostName:"+hostName+".");
        }
        return 0;
	}

	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

        List<String> repoList = new ArrayList<String>();
        String[] vmhostNames = new String[0];
        try {
            Fact fact = arrayHostNode[0].getEffectiveFact("resource.vmhosts");
            if (fact != null){
            	vmhostNames = fact.getStringArrayValue();
                for(int j=0; j< vmhostNames.length; j++){
                    GridObjectInfo vmhost = getClient().getGridObjectInfo(TYPE_VMHOST,vmhostNames[j]); // Error occurring here...
                    String[] strRepoName = vmhost.getEffectiveFact("vmhost.repositories").getStringArrayValue();
                    for (int k=0; k<strRepoName.length; k++){
                        repoList.add(strRepoName[k]);
                    }
                }
            }
        } catch(FactException ex) {
            return throwError(errNo(3), "Get fact error: "+ex.getMessage());
        }
        
        if (repoList.size()==0 || vmhostNames.length == 0){     // The host has no vmhosts and of course no vms, it isn't an error.
            StringBuffer sb = new StringBuffer(XML_HEADER);        
            sb.append("<results>\n");
            sb.append("    <result>"+ 0 +"</result>\n");
            sb.append("<results>\n");
            return sb.toString();
        }
        
        StringBuffer strCon = new StringBuffer("<and>\n<eq fact=\"resource.type\" value=\"VM\"/>\n<or>\n");
        for(int i=0; i< vmhostNames.length; i++){
            strCon.append("<eq fact=\"resource.provision.vmhost\" value=\"" + vmhostNames[i] + "\"/>\n");
            strCon.append("<eq fact=\"resource.provisioner.recommendedhost\" value=\"" + vmhostNames[i] + "\"/>\n");
        }
        strCon.append("</or>\n</and>\n");

        GridObjectInfo[] arrayOfNodeInfo = null;
        try {
            arrayOfNodeInfo = getClient().getGridObjects(TYPE_RESOURCE,strCon.toString(),true);
        } catch(Exception ex) {
            System.err.println("Error: "+ex.getMessage());
            return throwError(errNo(4), "Get node error. "+ex.getMessage());
        }
        
        if (arrayOfNodeInfo == null){        // No vms found, it isn't an error
            StringBuffer sb = new StringBuffer(XML_HEADER);        
            sb.append("<results>\n");
            sb.append("    <result>"+ 0 +"</result>\n");
            sb.append("<results>\n");
            return sb.toString();
        }

        StringBuffer sb = new StringBuffer(XML_HEADER);
        sb.append("<results>\n");
        sb.append("    <result>"+ 0 +"</result>\n");
        sb.append("    <instanceIDSet>\n");
        Fact f;
        for(int i=0; i<arrayOfNodeInfo.length; i++)
        {
            sb.append("        <item>\n");
            try {
                f = arrayOfNodeInfo[i].getEffectiveFact("resource.id");
                if ( f != null ){
                    sb.append("            <instanceID>" + f.getValue() + "</instanceID>\n");
                }
            } catch(FactException ex) {
                return throwError(errNo(6), "Get fact error: "+ex.getMessage());
            }
            sb.append("        </item>\n");
        }
        sb.append("    </instanceIDSet>\n");
        sb.append("</results>\n");

        return sb.toString();
	}

}
