/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Arrays;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.GridObjectNotFoundException;
import com.novell.zos.grid.WorkflowInfo;

public class AttachVolume extends Op {

    private String volumeId = null;
    private String instanceId = null;
    private String device = null;
    private String mode = null;
    private GridObjectInfo instance = null;
    private GridObjectInfo repo = null;
    private String repoId = null;

	public AttachVolume() {
		super("AttachVolume", "Attach a disk into a vm instance", 59);
	}

	public AttachVolume(ClientAgent client) {
		super(client,"AttachVolume", "Attach a disk into a vm instance", 59);
	}

	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);

            if (root.getAttributes().getLength()>0) {
                NodeList nodes0 = root.getChildNodes();
                for (int i = 0; i < nodes0.getLength(); i++) {
                     Node node0 = nodes0.item(i);
                     if (node0.getNodeType() == Node.ELEMENT_NODE) {
                        if (node0.getNodeName().equals("sessionID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              session = txt.trim();
                        } else if (node0.getNodeName().equals("volumeID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              volumeId = txt.trim();
                        } else if (node0.getNodeName().equals("instanceID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              instanceId = txt.trim();
                        } else if (node0.getNodeName().equals("device")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              device = txt.trim();
                        } else if (node0.getNodeName().equals("mode")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              mode = txt.trim();
                        }
                     }
                }
            }
		return 0;
	}

	protected int checkParameters() throws Exception {

        if (IsNull(volumeId)){
            throw new Exception( "Parameter error: volumeID can't be empty.");
        }

        if (IsNull(device)){       //Must
            throw new Exception( "Parameter error: device can't be empty.");
        }
        
        if (IsNotNull(mode)){
            mode = mode.toLowerCase();
            if (!( mode.equals("read")|| mode.equals("write") ) ){
                throw new Exception( "Parameter error: mode must be 'read' or 'write'.");
            }
        } else {
            mode = "write";     //default 'write' mode
        }
        
        if (IsNull(instanceId)){       //Must
            throw new Exception("Parameter error: instanceID can't be empty.");
        }

        instance = getGridObject(TYPE_RESOURCE,instanceId);
        if (instance == null){
            throw new Exception("Can't find instance '" + instanceId + "'.");
        }
		if (! instance.getEffectiveFact("resource.type").getStringValue().equalsIgnoreCase(TYPE_VM_INSTANCE)){
            throw new Exception("Resource '"+ instanceId +"' isn't a VM instance.");
        }
        
        ////// TODO: Check volumeId ////////////
        repo = FindRepositoryOfVolume(volumeId);
        if (null == repo){
            throw new Exception( "Parameter error: Can not find volume '" + volumeId + "'.");
        }

        boolean inRepo = false;
        // TODO: check if vm is in the repository
        String[] vms = repo.getEffectiveFact("repository.vmimages").getStringArrayValue();
        for(String vm: vms){
            if(vm.equals(instanceId)){
                inRepo = true;
                break;
            }
        }
            if(!inRepo){
                throw new Exception("VM can not access the volume.");
            }
        return 0;
	}

    private GridObjectInfo FindRepositoryOfVolume(String volumeID) throws Exception
    {
        String volumeFactName = "repository.AIS.volume." + volumeID;
        String cont = null;
        GridObjectInfo[] repositories = getClient().getGridObjects(TYPE_REPOSITORY,cont,true);
        for (GridObjectInfo repository : repositories){
            Fact ft = repository.getEffectiveFact(volumeFactName);
            if( null != ft ){
                return repository;
            }
        }
        return null;
    }
    
	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

        String jobName = "volumemanager";
        Fact jobsFact= repo.getEffectiveFact("repository.provisioner.jobs");
        if (null==jobsFact) {
            return throwError(errNo(10),"repository.provisioner.jobs fact not set on repository: ");
        }
        String[] jobs= null;
        try {
            jobs = jobsFact.getStringArrayValue();
        } catch (FactException e) {
            return throwError(errNo(11),"repository.provisioner.jobs fact has invalid type on repository: ");
        }
        for (String job:jobs) {
        	if ("vsphere".equals(job)) {
                jobName = "volumemanager_vs";
                break;
        	}
        }
		JobSpec job = this.getJob(jobName);
        if (job == null){
            return throwError(errNo(3),"Job '" + jobName +"' is not deployed.");
        }
        
		JobDef jobDef = new JobDef();
		jobDef.setName(jobName);
		jobDef.setJob(job);
		jobDef.resetImports();
        jobDef.setImport("jobargs.action", "attach");
        
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("volumeID",volumeId);
        params.put("instanceID",instanceId);
        params.put("device",device);
        params.put("mode",mode);
		jobDef.setWhatComplete(true);
		jobDef.setWaitForCompletion(true);

		RunJob runJob = new RunJob(getClient());
		try {
			if (!runJob.run(jobDef)) {
				return throwError(errNo(4), "Error running job '" + jobName
						+ "'. \nJob Log:\n" + getJobLog(runJob.getJobId()));
			}
		} catch (Exception ex) {
			return throwError(errNo(5), ex.getMessage());
		}

        StringBuffer sb = new StringBuffer(XML_HEADER);
        sb.append("<results>\n");
        sb.append("    <result>" + 0 +"</result>\n");
        sb.append("</results>\n");
        
        return sb.toString();
	}
}
