/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Arrays;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.GridObjectNotFoundException;

public class CreateInstance extends Op {


    private String physicalHostName= null;
    private String repName= null;
    private String vmhostName= null;
    private String vmTemplateID= null; // optional
    private String ignorePersonalize = null; // optional   
    private String rootPwd= null;
    private String cpuNum= null;
    private String frequency= null;
    private String minMem= null;
    private String maxMem= null;
    private String instanceID= null;
    private String publicKey= null;
    private String dnsIP1 = null;
    private String dnsIP2 = null;
    private ArrayList<Map<String,String>> nicSet= new ArrayList<Map<String,String>>();
    private ArrayList<Map<String,String>> diskSet= new ArrayList<Map<String,String>>();
    private GridObjectInfo vmHost= null;

	public CreateInstance() {
		super("CreateInstance", "Create a new instance", 25);
	}

	public CreateInstance(ClientAgent client) {
		super(client,"CreateInstance", "Create a new instance", 25);
	}

	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);

    		if (root.getAttributes().getLength()>0) {
    			NodeList nodes0 = root.getChildNodes();
    			for (int i = 0; i < nodes0.getLength(); i++) {
    				Node node0 = nodes0.item(i);
    				if (node0.getNodeType() == Node.ELEMENT_NODE) {
    					if (node0.getNodeName().equals("sessionID")){
    						session = getTrimmedChildText(node0);

    					} else if (node0.getNodeName().equals("phsicalHostName") ||
    							node0.getNodeName().equals("physicalHostName")){
    						physicalHostName = getTrimmedChildText(node0);

    					} else if (node0.getNodeName().equals("vmTemplateID")){
    						vmTemplateID = getTrimmedChildText(node0);

    					} else if (node0.getNodeName().equals("ignorePersonalize")){
    						ignorePersonalize = getTrimmedChildText(node0);

    					} else if (node0.getNodeName().equals("repName")){
    						repName = getTrimmedChildText(node0);

    					} else if (node0.getNodeName().equals("vmhostName")){
    						vmhostName = getTrimmedChildText(node0);

    					} else if (node0.getNodeName().equals("rootPwd")){
    						rootPwd = getTrimmedChildText(node0);

    					} else if (node0.getNodeName().equals("cpuNum")){
    						cpuNum = getTrimmedChildText(node0);

    					} else if (node0.getNodeName().equals("frequency")){
    						frequency = getTrimmedChildText(node0);

    					} else if (node0.getNodeName().equals("publicKey")){
    						publicKey = getTrimmedChildText(node0);

    					} else if (node0.getNodeName().equals("dnsIP1")){
    						dnsIP1 = getTrimmedChildText(node0);
    						
    					} else if (node0.getNodeName().equals("dnsIP2")){
    						dnsIP2 = getTrimmedChildText(node0);

    					} else if (node0.getNodeName().equals("instanceID")){
    						instanceID = getTrimmedChildText(node0);
    						
    					} else if (node0.getNodeName().equals("memInfo")){
    						NodeList nodes1 = node0.getChildNodes();
    						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
    							Node node1 = nodes1.item(i1);
    							if (node1.getNodeType() == Node.ELEMENT_NODE) {
    								if (node1.getNodeName().equals("minMem")) {
    									minMem = getTrimmedChildText(node1);

    								} else if (node1.getNodeName().equals("maxMem")) {
    									maxMem = getTrimmedChildText(node1);
    								}
    							}
    						}
    					} else if (node0.getNodeName().equals("nicSet")){
    						NodeList nodes1 = node0.getChildNodes();
    						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
    							Node node1 = nodes1.item(i1);
    							if (node1.getNodeType() == Node.ELEMENT_NODE) {
    								if (node1.getNodeName().equals("item"))
    								{
    									NodeList nodes2 = node1.getChildNodes();
    									HashMap<String,String> nicSetMap = new HashMap<String,String>();
    									for (int i2 = 0; i2 < nodes2.getLength(); i2++) {
    										Node node2 = nodes2.item(i2);
    										if (node2.getNodeType() == Node.ELEMENT_NODE) {
    											if (node2.getNodeName().equals("network")) {
    												nicSetMap.put("network", getTrimmedChildText(node2));

    											} else if (node2.getNodeName().equals("flux")) {
    												nicSetMap.put("flux", getTrimmedChildText(node2));

    											} else if (node2.getNodeName().equals("ipAddress")) {
    												nicSetMap.put("ip", getTrimmedChildText(node2));

    											} else if (node2.getNodeName().equals("ipAddrMask")) {
    												nicSetMap.put("netmask", getTrimmedChildText(node2));

    											} else if (node2.getNodeName().equals("gateways")) {
    												nicSetMap.put("gateways", getTrimmedChildText(node2));

    											} else if (node2.getNodeName().equals("mac")){
    												nicSetMap.put("mac", getTrimmedChildText(node2));
    												
    											}
    										}
    									}
    									nicSet.add(nicSetMap);
    								}
    							}
    						}
    					} else if (node0.getNodeName().equals("diskSet")){
    						NodeList nodes1 = node0.getChildNodes();
    						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
    							Node node1 = nodes1.item(i1);
    							if (node1.getNodeType() == Node.ELEMENT_NODE) {
    								if (node1.getNodeName().equals("item")) {
    									NodeList nodes2 = node1.getChildNodes();
    									HashMap<String,String> diskSetMap = new HashMap<String,String>();
    									for (int i2 = 0; i2 < nodes2.getLength(); i2++) {
    										Node node2 = nodes2.item(i2);
    										if (node2.getNodeType() == Node.ELEMENT_NODE) {
    											if (node2.getNodeName().equals("device")) {
    												diskSetMap.put("device", getTrimmedChildText(node2));

    											} else if (node2.getNodeName().equals("size")) {
    												diskSetMap.put("size", getTrimmedChildText(node2));

    											}
    										}
    									}
    									diskSet.add(diskSetMap);
    								}
    							}
    						}
    					}
    				}
    			}
    		}

		return 0;
	}

	protected int checkParameters() throws Exception {

        if (IsNull(physicalHostName)){
            throw new Exception("Parameter error: physicalHostName can't be empty.");
        }
        if (IsNull(repName)){
            throw new Exception( "Parameter error: repName can't be empty.");
        }
        if (IsNull(vmhostName)){
            throw new Exception( "Parameter error: vmhostName can't be empty.");
        }
        if (IsNull(rootPwd)){
            throw new Exception( "Parameter error: rootPwd can't be empty.");
        }
        if (IsNull(cpuNum)){
            throw new Exception( "Parameter error: cpuNum can't be empty.");
        }
        if (IsNull(frequency )){
            throw new Exception( "Parameter error: frequency can't be empty.");
        }
        if (IsNull(minMem)){
            throw new Exception( "Parameter error: minMem can't be empty.");
        }
        if (IsNull(maxMem)){
            throw new Exception( "Parameter error: maxMem can't be empty.");
        }

        ////// Check cpuNum //////
        long cpuNumL = Long.parseLong(cpuNum);
        if ((cpuNumL < 1)||(cpuNumL > 8)){
            throw new Exception( "Parameter error: value of 'cpuNum' must be a integer between 1 and 8.");
        }

        ////// Check frequency //////
        long frequencyL = Long.parseLong(frequency);
        if ((frequencyL < 512)||(frequencyL > 3072)){
            throw new Exception(  "Parameter error: value of 'frequency' must be a integer between 512 and 3072.");
        }

        ////// Check minMem and maxMem //////
        long minMemL = Long.parseLong(minMem);
        long maxMemL = Long.parseLong(maxMem);
        if (minMemL < 128){
            throw new Exception(  "Parameter error: value of 'minMem' must be greater than 128.");
        }
        if (minMemL > maxMemL){
            throw new Exception("Parameter error: value of 'maxMem' must be greater than minMem.");
        }

        ////// Check physical HostName //////
        GridObjectInfo node = null;
        try{
            node = getClient().getGridObjectInfo(TYPE_RESOURCE,physicalHostName);
        }catch(Exception ex){
        }
        if (node == null){
            throw new Exception( "host:"+ physicalHostName +" doesn't exist.");
        }
		//check if it is a vm instance
		if (! node.getEffectiveFact("resource.type").getStringValue().equalsIgnoreCase(TYPE_FIXED_PHYSICAL)){
            throw new Exception( "Resource:"+ physicalHostName +" is not a fixed physical host.");
		}
        
        ////// Don't Check vmHostName: this is a host name of new vm, not a name of vm host//////
        vmHost = getVmHostFromHost(node);
        if (vmHost == null){
            throw new Exception( "vmHost of physical host '"+ physicalHostName +"' doesn't exist.");
        }        

        ////// Check if the repository exists //////////
        GridObjectInfo repoInfo = getGridObject(TYPE_REPOSITORY,repName);
        
        if (repoInfo == null) {
            throw new Exception( "Repository '"+repName+"' doesn't exist.");
        }

        ////// vmTemplateID OPTIONAL. Check if the template exists //////////
        if (vmTemplateID != null){
            node = null;
            try{
                node = getClient().getGridObjectInfo(TYPE_RESOURCE,vmTemplateID);
            }catch(Exception ex){
            }
        
            if (node == null){
                throw new Exception( "Template '"+ vmTemplateID +"' doesn't exist.");
            }
		    if (! node.getEffectiveFact("resource.type").getStringValue().equalsIgnoreCase(TYPE_VM_TEMPLATE)){
                throw new Exception( "Resource '"+ vmTemplateID +"' is not a VM Template.");
		    }
         }
        
        ////// check nicSet //////
        if (nicSet.size() > 8){
            throw new Exception( "Parameter error: the max number of nics is limited to 8.");
        }
        if(!nicSet.isEmpty()){
        	String[] validNetwork = null;
            if(vmHost!=null){
                validNetwork = (String[]) vmHost.getEffectiveFact("vmhost.networks").getStringArrayValue();
            }
            if (validNetwork == null){
                throw new Exception("Can not get network information of host '" + physicalHostName +"'." );
            }
            List<String> validNetworkList = Arrays.asList(validNetwork);
            for(Map<String,String> nic:nicSet){
                String anetwork = (String)getValueByKey(nic, "network");
                String aflux = (String)getValueByKey(nic, "flux");
                String aip = (String)getValueByKey(nic,"ip");
                String anetmask = (String)getValueByKey(nic,"netmask");
                if(IsNull(anetwork)||IsNull(aflux)||IsNull(aip)||IsNull(anetmask)){
                    throw new Exception("Parameter error: null required field [network|flux|ipAddress|ipAddrMask] of nicSet.");
                }
                if(!validNetworkList.contains(anetwork)){
                    throw new Exception(
                    	"Parameter error: 'nicSet->network' : vmhost of physicalhost '" + physicalHostName +"' has no network named '" + anetwork +"'.");
                }
                int iFlux = Integer.parseInt(aflux);
                if( iFlux<10 || iFlux >1000 ){
                    throw new Exception("Parameter error: value of 'nicSet->flux' must between 10 to 1000.");
                }
                // TODO: check ipAddress and ipAddrMask
            }
        }
        
        ////// check diskSet //////
        if(!diskSet.isEmpty()){
            for(Map<String,String> disk:diskSet){
                String adevice = (String)getValueByKey(disk,"device");
                String asize = (String)getValueByKey(disk,"size");
                if(IsNull(adevice)||IsNull(asize)){
                    throw new Exception("Parameter error: null required field [device|size] of diskSet.");
                }
                int iSize = Integer.parseInt(asize);
                if( iSize<1 || iSize >1024 ){
                    throw new Exception("Parameter error: value of 'diskSet->size' must between 1 to 1024.");
                }
            }
        }

		return 0;
	}

	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

		String jobName = "vmcreate";
        if (null!=vmHost) {
	        Fact jobFact= vmHost.getEffectiveFact("vmhost.provisioner.job");
	        if (null==jobFact) {
                return throwError(errNo(3),"vmhost.provisioner.job fact not set on vmhost: " + vmhostName);
	        }
            String jobFactValue= null;
			try {
			    jobFactValue = jobFact.getStringValue();
			} catch (FactException e) {
				return throwError(errNo(4),"vmhost.provisioner.job fact has invalid type on vmhost: " + vmhostName);
			}
			if ("xen".equals(jobFactValue)) {
			    
			}
			else if ("vsphere".equals(jobFactValue)) {
			    jobName = "vmcreate_vs";
			}
			else {
			    return throwError(errNo(5),"vmhost.provisioner.job fact has invalid value (" + jobFactValue + ") on vmhost: " + vmhostName);
			}
			    
        }
		JobSpec job = this.getJob(jobName);

        HashMap<String,Object> params = new HashMap<String,Object>();
        params.put("host", physicalHostName);
        params.put("vmhostName", vmhostName);
        params.put("reponame", repName);
        params.put("rootPwd", rootPwd);
        params.put("cpuNum", cpuNum);
        params.put("maxMem", maxMem);
        params.put("minMem", minMem);
        params.put("nicSet", nicSet);
        params.put("diskSet", diskSet);
        params.put("vmTemplateID", vmTemplateID);
        if (IsNotNull(ignorePersonalize)){
            params.put("ignorePersonalize",ignorePersonalize);
        }       
        params.put("frequency", frequency);
        if (IsNotNull(publicKey)){
            params.put("publicKey", publicKey);
        }
        if (IsNotNull("dnsIP1")){
        	params.put("dnsIP1", dnsIP1);
        }
        if (IsNotNull("dnsIP2")){
        	params.put("dnsIP2", dnsIP2);
        }
        if (IsNotNull("instanceID")){
        	params.put("instanceID", instanceID);
        }
        
		JobDef jobDef = new JobDef();
		jobDef.setName(jobName);
		jobDef.setJob(job);
		jobDef.resetImports();
        jobDef.setImport("jobargs.params", params);
		jobDef.setWhatComplete(true);
		jobDef.setWaitForCompletion(false);

		RunJob runJob = new RunJob(getClient());
		try {
			if (!runJob.run(jobDef)) {
				return throwError(errNo(6), "Error running job '" + jobName
						+ "'. \nJob Log:\n" + getJobLog(runJob.getJobId()));
			}
		} catch (Exception ex) {
			return throwError(errNo(7), ex.getMessage());
		}

        String jobID = runJob.getJobId();

        String strCon = "<and>\n<eq fact=\"resource.type\" value=\"VM\"/>\n</and>\n";
        strCon += "<and>\n<eq fact=\"resource.vm.iwm.initial.job\" value=\"" + jobID + "\"/>\n</and>\n";

        GridObjectInfo[] nodeInfos = null;
        try{
        	nodeInfos = GetInfoFromJob(strCon,jobID);
        }catch( Exception ex){
        	return throwError(errNo(8), "Timeout to query the tempalte id from the job.\nJob Log:\n"+getJobLog(jobID) + "\n jobID:" + jobID);
        }
        
        if(nodeInfos == null || nodeInfos.length == 0){
			return throwError(errNo(9), "Create VmTemplate from instance '"+instanceID+"' failed.\nJob Log:\n"+getJobLog(jobID) + "\n jobID:" + jobID);
        }

        String vmTemplateID = null;
        try {
            vmTemplateID = nodeInfos[0].getEffectiveFact("resource.id").getStringValue();
        } catch (FactException ex) {
            return throwError(errNo(11),"Get fact error: "+ex.getMessage());
        }

        StringBuffer sb = new StringBuffer(XML_HEADER);
        sb.append("<results>\n");
        sb.append("    <result>"+ 0 +"</result>\n");          // result
        sb.append("    <jobID>" + jobID + "</jobID>\n");
        sb.append("    <vmTemplateID>" + vmTemplateID + "</vmTemplateID>\n");
        sb.append("</results>");

		return sb.toString();
	}
}
