/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Arrays;
import java.util.Iterator;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactSet;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.GridObjectNotFoundException;
import com.novell.zos.grid.WorkflowInfo;

public class DescribeVolumes extends Op {

    private String volumeId = null;
    private ArrayList<String> volumeList = new ArrayList<String>();

	public DescribeVolumes() {
		super("DescribeVolumes", "Get the infomation of a volumn", 58);
	}

	public DescribeVolumes(ClientAgent client) {
		super(client,"DescribeVolumes", "Get the infomation of a volumn", 58);
	}

	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);

            if (root.getAttributes().getLength()>0) {
                NodeList nodes0 = root.getChildNodes();
                for (int i = 0; i < nodes0.getLength(); i++) {
                     Node node0 = nodes0.item(i);
                     if (node0.getNodeType() == Node.ELEMENT_NODE) {
                        if (node0.getNodeName().equals("volumeIDSet")){
                             NodeList nodes1 = node0.getChildNodes();
                             for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                  Node node1 = nodes1.item(i1);
                                  if (node1.getNodeType() == Node.ELEMENT_NODE) {
                                       if (node1.getNodeName().equals("item")) {
                                           NodeList nodes2 = node1.getChildNodes();
                                           for (int i2 = 0; i2 < nodes2.getLength(); i2++) {
                                                Node node2 = nodes2.item(i2);
                                                if (node2.getNodeType() == Node.ELEMENT_NODE) {
                                                      if (node2.getNodeName().equals("volumeID"))
                                                      {
                                                            NodeList nodes3 = node2.getChildNodes();
                                                            String txt="";
                                                            for (int i3 = 0; i3 < nodes3.getLength(); i3++) {
                                                                Node node3 = nodes3.item(i3);
                                                                if (node3.getNodeType() == Node.TEXT_NODE) {
                                                                    txt = txt + node3.getNodeValue();
                                                                }
                                                            }
                                                            volumeList.add(txt.trim());
                                                       }
                                                }
                                           }
                                       }
                                  }
                             }
                        } else if (node0.getNodeName().equals("sessionID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              session = txt.trim();
                        }
                     }
                }
            }
		return 0;
	}

	protected int checkParameters() throws Exception {

        return 0;
	}

    private GridObjectInfo FindRepositoryOfVolume(String volumeID) throws Exception
    {
        String volumeFactName = "repository.AIS.volume." + volumeID;
        String cont = null;
        GridObjectInfo[] repositories = getClient().getGridObjects(TYPE_REPOSITORY,cont,true);
        for (GridObjectInfo repository : repositories){
            Fact ft = repository.getEffectiveFact(volumeFactName);
            if( null != ft ){
                return repository;
            }
        }
        return null;
    }
    
	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

        List<GridObjectInfo> repoList = new ArrayList<GridObjectInfo>();
        String cont = null;
        GridObjectInfo[] repositories = getClient().getGridObjects(TYPE_REPOSITORY,cont,true);
        repoList = Arrays.asList(repositories);
        if (repoList.size() == 0){
            return throwError(errNo(4), "No Repository found.");
        }

        ArrayList<Map> volumeFactList = new ArrayList<Map>();
        for (int i=0; i<repoList.size(); i++){
            GridObjectInfo repo = repoList.get(i);
            String repName = repo.getEffectiveFact("repository.id").getStringValue();
            FactSet fs = repo.getEffectiveFacts(); 
            Iterator iter = fs.iterator();
            while( iter.hasNext()){
                try{
                    Fact f = (Fact)iter.next();
                    String factname = f.getName();
                    if(factname.startsWith("repository.AIS.volume.")){
                        if (volumeList.size()>0){
                            String volname = factname.substring(22);
                            if (!volumeList.contains(volname)){
                                continue;
                            }
                        }
                        Map<String,Object> volumeFact = null;
                        try {
                            volumeFact = f.getDictionaryValue();
                        } catch (FactException ex){
                            return throwError(errNo(5), "Get fact error. " +ex.getMessage());
                        }
                        volumeFact.put("repName",repName);
                        volumeFactList.add(volumeFact);
                    }
                }catch(Exception ex){
                }
            }
        }

        if (volumeFactList.size()==0){
            return throwError(errNo(6), "No Volume found.");
        }

        StringBuffer sb = new StringBuffer(XML_HEADER);
        sb.append("<results>\n");
        sb.append("    <result>" + 0 +"</result>\n");
        sb.append("    <volumeSet>\n");
        Map volumeInfo = null;
        for (int i=0; i< volumeFactList.size(); i++){
            volumeInfo = volumeFactList.get(i);
            List attachInfos = null;

            sb.append("        <item>\n");
            Iterator iT = volumeInfo.keySet().iterator();
            while (iT.hasNext()) {
                String aKey = iT.next().toString();
                if (aKey.equals("attachInfo")){
                    attachInfos = (List)volumeInfo.get(aKey);
                } else if ( aKey.equals("lvname") || aKey.equals("snapshotSet") ){
                    // Skip internal fact
                } else if (aKey.equals("snapshot")){
                    Map snapshot = (Map)volumeInfo.get("snapshot");
                    sb.append("            <snapshot>\n");
                    sb.append("                <snapshotID>" + snapshot.get("snapshotID") + "</snapshotID>\n");
                    sb.append("                <snapshotPath>" + snapshot.get("snapshotPath") + "</snapshotPath>\n");
                    sb.append("            </snapshot>\n");
                } else {
                    sb.append("            <"+aKey+">" + volumeInfo.get(aKey) + "</"+aKey+">\n");
                }
            }
            if ((attachInfos!=null) && (attachInfos.size()>0)){
                sb.append("            <attachmentSet>\n");
                for (int j=0; j<attachInfos.size(); j++){
                    sb.append("                <item>\n");
                    Map attachInfo = (Map)attachInfos.get(j);
                    Iterator iT2 = attachInfo.keySet().iterator();
                    while (iT2.hasNext()) {
                        String aKey = iT2.next().toString();
                        if (!aKey.equals("vdisk"))
                            sb.append("                    <"+aKey+">" + attachInfo.get(aKey) + "</"+aKey+">\n");
                    }
                    sb.append("                </item>\n");
                }
                sb.append("            </attachmentSet>\n");
            }
            sb.append("        </item>\n");
        }
        sb.append("    </volumeSet>\n");
        sb.append("</results>\n");
        
        return sb.toString();
	}
}
