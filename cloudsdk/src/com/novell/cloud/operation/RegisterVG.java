/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */
package com.novell.cloud.operation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;

public class RegisterVG extends Op {
	
	 private ArrayList<Map> vgSet = new ArrayList<Map>();
	    private ArrayList<Map> vgNotFoundSet = new ArrayList<Map>();
	    private String hostName = null;
	
	public RegisterVG() {
		super("RegisterVG", "Register VG", 8);
	}
	
	public RegisterVG(ClientAgent client) {
		super(client, "RegisterVG", "Register VG", 8);
	}
	
	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);
		
		if (root.getAttributes().getLength()>0) {
            NodeList nodes0 = root.getChildNodes();
            for (int i = 0; i < nodes0.getLength(); i++) {
                 Node node0 = nodes0.item(i);
                 if (node0.getNodeType() == Node.ELEMENT_NODE) {
                    if (node0.getNodeName().equals("vgSet")){
                         NodeList nodes1 = node0.getChildNodes();
                         for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                              Node node1 = nodes1.item(i1);
                              if (node1.getNodeType() == Node.ELEMENT_NODE) {
                                   if (node1.getNodeName().equals("item"))
                                   {
                                       NodeList nodes2 = node1.getChildNodes();
                                       HashMap<String,String> vgSetMap = new HashMap<String,String>();
                                       for (int i2 = 0; i2 < nodes2.getLength(); i2++) {
                                            Node node2 = nodes2.item(i2);
                                            if (node2.getNodeType() == Node.ELEMENT_NODE) {
                                                  if (node2.getNodeName().equals("vgName"))
                                                  {
                                                    NodeList nodes3 = node2.getChildNodes();
                                                    String txt="";
                                                    for (int i3 = 0; i3 < nodes3.getLength(); i3++) {
                                                        Node node3 = nodes3.item(i3);
                                                        if (node3.getNodeType() == Node.TEXT_NODE) {
                                                           txt = txt + node3.getNodeValue();
                                                           }
                                                        }
                                                    txt = txt.trim();
                                                    if(!txt.equals("")) vgSetMap.put("vgName",txt);
                                                   }
                                                   else if (node2.getNodeName().equals("vgUUID"))
                                                   {
                                                    NodeList nodes3 = node2.getChildNodes();
                                                    String txt="";
                                                    for (int i3 = 0; i3 < nodes3.getLength(); i3++) {
                                                        Node node3 = nodes3.item(i3);
                                                        if (node3.getNodeType() == Node.TEXT_NODE) {
                                                           txt = txt + node3.getNodeValue();
                                                           }
                                                        }
                                                    txt = txt.trim();
                                                    if(!txt.equals("")) vgSetMap.put("vgUUID",txt);
                                                   }
                                            }
                                       }
                                       if(!"".equals(""+vgSetMap.get("vgName"))&& !"".equals(""+vgSetMap.get("vgUUID")))  {
                                            vgSet.add(vgSetMap);
                                        }
                                   }
                              }
                         }
                    }else if (node0.getNodeName().equals("sessionID")){
                          NodeList nodes1 = node0.getChildNodes();
                          String txt="";
                          for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                               Node node1 = nodes1.item(i1);
                               if (node1.getNodeType() == Node.TEXT_NODE) {
                               txt = txt + node1.getNodeValue();
                               }
                          }
                          session = txt.trim();
                    }else if (node0.getNodeName().equals("hostName")){
                          NodeList nodes1 = node0.getChildNodes();
                          String txt="";
                          for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                               Node node1 = nodes1.item(i1);
                               if (node1.getNodeType() == Node.TEXT_NODE) {
                               txt = txt + node1.getNodeValue();
                               }
                          }
                          hostName = txt.trim();
                    }
                 }
            }
        }
		
		return 0;
	}

}
