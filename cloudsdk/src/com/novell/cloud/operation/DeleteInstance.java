/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import com.novell.cloud.api.Instance;
import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.GridException;

public class DeleteInstance extends Instance
{
  public DeleteInstance()
  {
    super("DeleteInstance", "Delete a vm instance", 26);
  }

  public DeleteInstance(ClientAgent paramClientAgent)
  {
    super(paramClientAgent, "DeleteInstance", "Delete a vm instance", 26);
  }

  public String execute(String paramString)
    throws GridException
  {
    try
    {
      getParameters(paramString);
    }
    catch (Exception localException1)
    {
      return throwError(errNo(1), localException1.getMessage());
    }
    try
    {
      checkParameters();
    }
    catch (Exception localException2)
    {
      return throwError(errNo(2), localException2.getMessage());
    }
    String str1 = "vmdestroy";
    JobSpec localJobSpec = getJob(str1);
    JobDef localJobDef = new JobDef();
    localJobDef.setName(str1);
    localJobDef.setJob(localJobSpec);
    localJobDef.resetImports();
    localJobDef.setImport("jobargs.vmname", this.instanceID);
    localJobDef.setWhatComplete(true);
    localJobDef.setWaitForCompletion(true);
    RunJob localRunJob = new RunJob(getClient());
    try
    {
      if (!localRunJob.run(localJobDef))
        return throwError(errNo(3), "Error running job '" + str1 + "'. \nJob Log:\n" + getJobLog(localRunJob.getJobId()));
    }
    catch (Exception localException3)
    {
      return throwError(errNo(4), localException3.getMessage());
    }
    String str2 = localRunJob.getJobId();
    StringBuffer localStringBuffer = new StringBuffer("\n<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
    localStringBuffer.append("<results>\n");
    localStringBuffer.append("    <result>0</result>\n");
    localStringBuffer.append("    <jobID>" + str2 + "</jobID>\n");
    localStringBuffer.append("</results>");
    return localStringBuffer.toString();
  }
}
