/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Date;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.text.SimpleDateFormat;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;

public class DescribeInstances extends Op {
	
    private GridObjectInfo[] arrayOfNodeInfo = new GridObjectInfo[0];
    private ArrayList<String> localArrayList = new ArrayList<String>();

	public DescribeInstances() {
		super("DescribeInstances", "Get the information of the vm instance", 27);
	}
	
	public DescribeInstances(ClientAgent client) {
		super(client, "DescribeInstances", "Get the information of the vm instance", 27);
	}
	
	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);
		
            if (root.getAttributes().getLength()>0) {
                NodeList nodes0 = root.getChildNodes();
                for (int i = 0; i < nodes0.getLength(); i++) {
                     Node node0 = nodes0.item(i);
                     if (node0.getNodeType() == Node.ELEMENT_NODE) {
                        if (node0.getNodeName().equals("instanceIDSet")){
                             NodeList nodes1 = node0.getChildNodes();
                             for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                  Node node1 = nodes1.item(i1);
                                  if (node1.getNodeType() == Node.ELEMENT_NODE) {
                                       if (node1.getNodeName().equals("item")) {
                                           NodeList nodes2 = node1.getChildNodes();
                                           for (int i2 = 0; i2 < nodes2.getLength(); i2++) {
                                                Node node2 = nodes2.item(i2);
                                                if (node2.getNodeType() == Node.ELEMENT_NODE) {
                                                  if (node2.getNodeName().equals("instanceID")){
                                                      localArrayList.add(getTrimmedChildText(node2));
                                                   }
                                                }
                                           }
                                       }
                                  }
                             }
                        } else if (node0.getNodeName().equals("sessionID")){
                              session = getTrimmedChildText(node0);
                        }
                     }
                }
            }
		
		return 0;
	}
	
	protected int checkParameters() throws Exception {

        String[] instanceIDs  = (String[])localArrayList.toArray(new String[0]);
        String strCon = "";

        ////// Generate the Response Information ///////////
        if (instanceIDs.length == 0) {
            strCon = "<and>\n<eq fact=\"resource.type\" value=\"VM\"/>\n</and>\n";
        } else {
            strCon = "<and>\n<eq fact=\"resource.type\" value=\"VM\"/>\n<or>\n";
            for (int i = 0; i < instanceIDs.length; ++i) {
                strCon += "<eq fact=\"resource.id\" value=\"" + instanceIDs[i] + "\"/>\n";
            }
            strCon += "</or>\n</and>\n";
        }
        
        
        try {
            arrayOfNodeInfo = getClient().getGridObjects( TYPE_RESOURCE, strCon,true );
        } catch (Exception ex) {
            System.err.println("Can not find Nodes" + ex.getMessage());
            throw new Exception( "Can not find nodes. "+ ex.getMessage());
        }
        // need return null <vmSet/> instead of throw exception
        if ((arrayOfNodeInfo == null) || (arrayOfNodeInfo.length == 0)) {
            throw new Exception( "Can't find instance.");
        }
        return 0;
	}

	
	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}
		
        try{
            checkParameters();
        }catch(Exception ex) {
			return throwError(errNo(2), ex.getMessage());
        }

        Fact f;
        String vmId = "";

        StringBuffer sb = new StringBuffer(XML_HEADER);
        sb.append("<results>\n");
        sb.append("    <result>"+ 0 +"</result>\n");
        sb.append("    <vmSet>\n");
        try{
            for (int i = 0; i < arrayOfNodeInfo.length; ++i) {
                GridObjectInfo vm = arrayOfNodeInfo[i];
                sb.append("        <item>\n");
                SimpleDateFormat sdf =new SimpleDateFormat( "yyyy-MM-dd'T'hh:mm:ssZ");
                f = vm.getEffectiveFact("resource.id");
                if (f!= null){
                    vmId = f.getStringValue();
                    Date t=new Date(f.getLastModified());
                    sb.append("            <instanceID>" + vmId +"</instanceID>\n");
                    sb.append("            <createTime>" +sdf.format(t) +"</createTime>\n");
                }

                f = vm.getEffectiveFact("resource.vm.iwm.hostname");
                if (f!= null){
                    sb.append("            <vmhostName>" + f.getValue() +"</vmhostName>\n");
                }

                f = vm.getEffectiveFact("resource.vm.iwm.lastMigrateTime");
                if (f!= null){
                    sb.append("            <lastMigrateTime>" + f.getValue() +"</lastMigrateTime>\n");
                }

                f = vm.getEffectiveFact("resource.vm.iwm.publicKey");
                if (f!= null){
                    sb.append("            <publicKey>" + f.getValue() +"</publicKey>\n");
                }
                f = vm.getEffectiveFact("resource.vm.iwm.root.password");
                if (f!= null){
                    sb.append("            <rootPwd>" + f.getValue() +"</rootPwd>\n");
                } else {
                    sb.append("            <rootPwd></rootPwd>\n");
                }
                f = vm.getEffectiveFact("resource.vm.iwm.dnsIP1");
                if (f!= null){
                    sb.append("            <dnsIP1>" + f.getValue() +"</dnsIP1>\n");
                }
                f = vm.getEffectiveFact("resource.vm.iwm.dnsIP2");
                if (f!= null){
                    sb.append("            <dnsIP2>" + f.getValue() +"</dnsIP2>\n");
                }

                f = vm.getEffectiveFact("resource.provision.time.start");
                if (f!= null){
                    String Dstr = ""+f.getValue();
                    SimpleDateFormat sdf1 = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy");
                    Date t = new Date(0);
                    try {
                       t = sdf1.parse(Dstr);
                    } catch (java.text.ParseException ex) {
                       t=new Date(f.getLastModified());
                    }
                    sb.append("            <startTime>" + sdf.format(t) +"</startTime>\n");
                } else {
                    sb.append("            <startTime></startTime>\n");
                }

                String stateString = "unknown";
                try{
                    stateString = new GetInstanceState(getClient()).getInstanceState(vm);
                }catch(Exception ex){
                    System.err.println("ERROR: Query instance state: " + ex.getMessage());                	
                }
                sb.append("            <state>" + stateString +"</state>\n");
                boolean osV = false;
                f = vm.getEffectiveFact("resource.os.vendor.string");
                if (f != null){
                    String osVer = f.getStringValue();
                    if (!osVer.trim().equals("")){
                        osV = true;
                        sb.append("            <osVersion>" + osVer + "</osVersion>\n");
                    }
                }
                if ( osV == false){
                    f = vm.getEffectiveFact("resource.os.type");
                    if (f!= null){
                        String osVer = f.getStringValue();
                        if (osVer.trim().equals("")) osVer ="unknown";
                        sb.append("            <osVersion>" + osVer + "</osVersion>\n");
                    }
                }
                f = vm.getEffectiveFact("resource.vm.vcpu.number");
                if (f!= null){
                    sb.append("            <cpuNum>" + f.getValue() +"</cpuNum>\n");
                }
                
                // using the recommended host resource.cpu.model
                String strType = "";
                try{
                    String vmhostName = vm.getEffectiveFact("resource.provisioner.recommendedhost").getStringValue();
                    if(vmhostName.length() >0){
                        GridObjectInfo vmhost = null;
                        try{
                            vmhost = getClient().getGridObjectInfo(TYPE_VMHOST,vmhostName);
                        }catch(Exception ex){
                        }
                        if( null != vmhost){
                            f = vmhost.getEffectiveFact("vmhost.resource.cpu.model");
                            if (null != f){
                                strType = f.getStringValue();
                                sb.append("            <cpuType>" + strType + "</cpuType>\n");
                            }
                        }
                    }
                }catch(Exception ex){
                    System.err.println("ERROR: Query instance cpu type: " + ex.getMessage());                	
                }
                
                if( strType.length() == 0){
                    f = vm.getEffectiveFact("resource.vm.cpu.architecture");
                    if (f!= null){
                        strType = f.getStringValue();
                        if (strType.trim().equals(""))  strType = "unknown";
                        sb.append("            <cpuType>" + strType + "</cpuType>\n");
                    }
                }
                
                f = vm.getEffectiveFact("resource.vm.iwm.cpu.mhz");
                if (f!= null){
                    sb.append("            <frequency>" + f.getValue() + "</frequency>\n");
                } else {
                    sb.append("            <frequency>" + "-1" + "</frequency>\n");
                }

                sb.append("            <memInfo>\n");
                f = vm.getEffectiveFact("resource.vm.memory");
                if ( f != null ){
                    sb.append("                <minMem>" + f.getValue() + "</minMem>\n");
                }
                f = vm.getEffectiveFact("resource.vm.maxmem");
                if ( f != null ){
                    sb.append("                <maxMem>" + f.getValue() + "</maxMem>\n");
                }
                sb.append("            </memInfo>\n");

                f = vm.getEffectiveFact("resource.vm.vdisks");
                if ( f != null ){
                    String[] disks = f.getStringArrayValue();
                    GridObjectInfo vdisk = null;
                    sb.append("            <diskSet>\n");
                    for(int j=0; j< disks.length; j++)
                    {
                        vdisk =getClient().getGridObjectInfo(TYPE_VDISK,disks[j]);
                        if (vdisk != null)
                        {
                               sb.append("                <item>\n");
                               f = vdisk.getEffectiveFact("vdisk.iwm.location");
                               if (f!= null){
                                   sb.append("                     <device>" + f.getValue() + "</device>\n");
                               } else {
                                   f = vdisk.getEffectiveFact("vdisk.vdev");
                                   if (f!= null){
                                       sb.append("                     <device>/dev/" + f.getValue() + "</device>\n");
                                   }
                               }
                               f = vdisk.getEffectiveFact("vdisk.size");
                                if (f!= null){
                                    int size = 0;
                                    try{
                                        size = Integer.parseInt(""+f.getValue()) / 1024;
                                    } catch(NumberFormatException ex) {
                                        size = 0;
                                    }
                                    sb.append("                     <size>" + size + "</size>\n");
                                }
                               f = vdisk.getEffectiveFact("vdisk.location");                               
                                if (f!= null){
                                	// FIXME: should return the location path
                                	String volumeId = f.getStringValue();
                                    sb.append("                     <volumeID>" + volumeId + "</volumeID>\n");
                                }
                               sb.append("                </item>\n");
                        }
                    }
                    sb.append("            </diskSet>\n");
                }

                f = vm.getEffectiveFact("resource.vm.vnics");
                if ( f != null ){
                    String[] nics = ((String[])(String[])(String[])f.getValue());
                    GridObjectInfo vnic = null;
                    sb.append("            <nicSet>\n");
                    for(int j=0; j< nics.length; j++){
                        vnic =getClient().getGridObjectInfo(TYPE_VNIC,(nics[j]));
                        if (vnic != null){
                            sb.append("                <item>\n");
                            f = vnic.getEffectiveFact("vnic.network");
                            if (f!= null){
                                sb.append("                     <network>" + f.getValue() + "</network>\n");
                            }
                            f = vnic.getEffectiveFact("vnic.ip");
                            if (f != null){
                                sb.append("                     <ipAddress>" + f.getValue() + "</ipAddress>\n");
                            }
                            f = vnic.getEffectiveFact("vnic.mac");
                            if (f!= null){
                                sb.append("                     <mac>" + f.getValue() + "</mac>\n");
                            }
                            f = vnic.getEffectiveFact("vnic.rate");
                            if (f!= null){
                                sb.append("                     <flux>" + f.getValue() + "</flux>\n");
                            }
                            f = vnic.getEffectiveFact("vnic.netmask");
                            if (f!= null){
                                sb.append("                     <ipAddrMask>" + f.getValue() + "</ipAddrMask>\n");
                            }
                            f = vnic.getEffectiveFact("vnic.gateways");
                            if (f!= null){
                                sb.append("                     <gateways>" + f.getValue() + "</gateways>\n");
                            }
                            sb.append("                </item>\n");
                        }
                    }
                    sb.append("            </nicSet>\n");
                }
                sb.append("        </item>\n");
            }
        } catch(FactException ex){
            return throwError(errNo(6), "Get fact error. "+ ex.getMessage());
        }
        sb.append("    </vmSet>\n");
        sb.append("</results>\n");
        return sb.toString();

	}
}
