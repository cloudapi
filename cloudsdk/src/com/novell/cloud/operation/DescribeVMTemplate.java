/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.GridObjectNotFoundException;

public class DescribeVMTemplate extends Op {

    private static final String RESOURCE_VM_IWM_TEMPLATE_REGISTERED = "resource.vm.IWM.template.registered";
    private String vmTemplateID = null;
    private ArrayList<String> templateIDList = new ArrayList<String>();
    private GridObjectInfo[] arrayOfNodeInfo = null;

	public DescribeVMTemplate() {
		super("DescribeVMTemplate", "Get the information of a vm template", 23);
	}

	public DescribeVMTemplate(ClientAgent client) {
		super(client,"DescribeVMTemplate", "Get the information of a vm template", 23);
	}

	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);

            if (root.getAttributes().getLength()>0) {
                NodeList nodes0 = root.getChildNodes();
                for (int i = 0; i < nodes0.getLength(); i++) {
                     Node node0 = nodes0.item(i);
                     if (node0.getNodeType() == Node.ELEMENT_NODE) {
                        if (node0.getNodeName().equals("templateSet")){
                                     NodeList nodes1 = node0.getChildNodes();
                                     for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                          Node node1 = nodes1.item(i1);
                                          if (node1.getNodeType() == Node.ELEMENT_NODE) {
                                               if (node1.getNodeName().equals("item"))
                                               {
                                                   NodeList nodes2 = node1.getChildNodes();
                                                   for (int i2 = 0; i2 < nodes2.getLength(); i2++) {
                                                        Node node2 = nodes2.item(i2);
                                                        if (node2.getNodeType() == Node.ELEMENT_NODE) {
                                                            if (node2.getNodeName().equals("vmTemplateID"))
                                                            {
                                                                NodeList nodes3 = node2.getChildNodes();
                                                                String txt="";
                                                                for (int i3 = 0; i3 < nodes3.getLength(); i3++) {
                                                                    Node node3 = nodes3.item(i3);
                                                                    if (node3.getNodeType() == Node.TEXT_NODE) {
                                                                       txt = txt + node3.getNodeValue();
                                                                    }
                                                                }
                                                                if (txt.trim().length() > 0){
                                                                    templateIDList.add(txt.trim());
                                                                }
                                                            }
                                                        }
                                                   }
                                               }
                                          }
                                     }
                        } else if (node0.getNodeName().equals("sessionID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              session = txt.trim();
                        }
                     }
                }
            }
		return 0;
	}

	protected int checkParameters() throws Exception {

        ////// Check if templates exist /////////////
        String[] vmTemplateIDs  = ((String[])templateIDList.toArray(new String[0]));

        String strCon = "";
        if (vmTemplateIDs.length == 0) {
            strCon = "<and>\n<eq fact=\"resource.type\" value=\"VM Template\"/>\n</and>\n";
        } else {
            strCon = "<and>\n<eq fact=\"resource.type\" value=\"VM Template\"/>\n<or>\n";
            for (int i = 0; i < vmTemplateIDs.length; ++i) {
                strCon = strCon + "<eq fact=\"resource.id\" value=\"" + vmTemplateIDs[i] + "\"/>\n";
            }
            strCon += "</or>\n</and>\n";
        }
        
        //huawei now needs to find a vmtemplate underconstruction, returning a status of '2' (creating)
        //strCon += "<and>\n<eq fact=\"resource.vm.underconstruction\" value=\"false\"/>\n</and>\n";

        try {
            arrayOfNodeInfo = getClient().getGridObjects(TYPE_RESOURCE,strCon,true);
        } catch (Exception localException) {
            System.err.println("Cannot find Nodes" + localException.getMessage());
        }
        if ((arrayOfNodeInfo == null) || (arrayOfNodeInfo.length == 0)) {
            throw new Exception("Cannot find VM Template.");
        }
        return 0;
        
	}

	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

        Fact f = null;

        ////// Generate the Response information /////////////
        StringBuffer sb = new StringBuffer(XML_HEADER);
        sb.append("<results>\n");
        sb.append("    <result>"+ 0 +"</result>\n");
        sb.append("    <templateSet>\n");
        try {
            for (int i = 0; i < arrayOfNodeInfo.length; ++i) {
                sb.append("        <item>\n");
                f = arrayOfNodeInfo[i].getEffectiveFact("resource.id");
                if (f!= null){
                    sb.append("            <vmTemplateID>" + f.getValue() +"</vmTemplateID>\n");
                }
                f = arrayOfNodeInfo[i].getEffectiveFact("resource.vm.basepath");
                if (f!= null){
                    sb.append("            <vmTemplatePath>" + f.getValue() +"</vmTemplatePath>\n");
                }
                f = arrayOfNodeInfo[i].getEffectiveFact("resource.vm.iwm.description");
                if (f!= null){
                    sb.append("            <description>" + f.getValue() +"</description>\n");
                }
                /*else {
                    sb.append("            <description></description>\n");
                }*/

                f = arrayOfNodeInfo[i].getEffectiveFact("resource.os.type");
                if (f!= null){
                    sb.append("            <osVersion>" + f.getValue() + "</osVersion>\n");
                }
                f = arrayOfNodeInfo[i].getEffectiveFact("resource.vm.vcpu.number");
                if (f!= null){
                    sb.append("            <cpuNum>" + f.getValue() +"</cpuNum>\n");
                }
                f = arrayOfNodeInfo[i].getEffectiveFact("resource.vm.cpu.architecture");
                if (f!= null){
                    String cpuT = f.getStringValue().trim();
                    if (cpuT.equals("")){
                        sb.append("            <cpuType>" + "unknown" + "</cpuType>\n");
                    } else {
                        sb.append("            <cpuType>" + cpuT + "</cpuType>\n");
                    }
                } else {
                    sb.append("            <cpuType>" + "unknown" + "</cpuType>\n");
                }

                f = arrayOfNodeInfo[i].getEffectiveFact("resource.vm.iwm.cpu.mhz");
                if (f!=null){
                    sb.append("            <frequency>" + f.getValue() + "</frequency>\n");
                } else {
                    sb.append("            <frequency>" + "-1" + "</frequency>\n");
                }

                f = arrayOfNodeInfo[i].getEffectiveFact("resource.vm.memory");
                if ( f != null ){
                    sb.append("            <memory>" + f.getValue() + "</memory>\n");
                }

                f = arrayOfNodeInfo[i].getEffectiveFact("resource.vm.vdisks");
                if ( f != null ){
                    String[] disks = ((String[])(String[])(String[])f.getValue());
                    GridObjectInfo vdisk = null;
                    sb.append("            <diskSet>\n");
                    for(int j=0; j< disks.length; j++) {
                        vdisk = getClient().getGridObjectInfo(TYPE_VDISK,disks[j]);
                        if (vdisk != null) {
                               sb.append("                <item>\n");
                               f = vdisk.getEffectiveFact("vdisk.iwm.location");
                               if (f!= null){
                                   sb.append("                     <device>" + f.getValue() + "</device>\n");
                               } else {
                                   f = vdisk.getEffectiveFact("vdisk.vdev");
                                   if (f!= null){
                                       sb.append("                     <device>/dev/" + f.getValue() + "</device>\n");
                                   }
                               }
                               f = vdisk.getEffectiveFact("vdisk.size");
                                if (f!= null){
                                    sb.append("                     <total>" + f.getValue() + "</total>\n");
                                }
                               sb.append("                </item>\n");
                        }
                    }
                    sb.append("            </diskSet>\n");
                }

                f = arrayOfNodeInfo[i].getEffectiveFact("resource.vm.vnics");
                if ( f != null ){
                    String[] nics = ((String[])(String[])(String[])f.getValue());
                    GridObjectInfo vnic = null;
                    sb.append("            <nicSet>\n");
                    for(int j=0; j< nics.length; j++) {
                        vnic =getClient().getGridObjectInfo(TYPE_VNIC,(nics[j]));
                        if (vnic != null){
                            sb.append("                <item>\n");
                            f = vnic.getEffectiveFact("vnic.network");
                            if (f!= null){
                                sb.append("                     <device>" + f.getValue() + "</device>\n");
                            }
                            f = vnic.getEffectiveFact("vnic.rate");
                            if (f!= null){
                                sb.append("                     <flux>" + f.getValue() + "</flux>\n");
                            }
                            sb.append("                </item>\n");
                        }
                    }
                    sb.append("            </nicSet>\n");
                }
                
                // to check template state
                f = arrayOfNodeInfo[i].getEffectiveFact("resource.vm.underconstruction");
                if (f!=null){
                    boolean bUnderconstruction = f.getBooleanValue();
                    if (bUnderconstruction){
                        sb.append("            <tempState>2</tempState>\n");
                    } else {
                        f = arrayOfNodeInfo[i].getEffectiveFact(RESOURCE_VM_IWM_TEMPLATE_REGISTERED);
                        if (f!=null){
                            boolean bRegistered = f.getBooleanValue();
                            if (bRegistered){
                                sb.append("            <tempState>1</tempState>\n");
                            } else {
                                sb.append("            <tempState>0</tempState>\n");
                            }
                        } else {
                            sb.append("            <tempState>0</tempState>\n");
                        }                    
                    }
                }

                // return templateType
                f = arrayOfNodeInfo[i].getEffectiveFact("resource.provisioner.job");
                if (f!=null){

                    String templatePA = ""+f.getValue();
                    
                    sb.append("            <templatePA>" + f.getValue() +"</templatePA>\n");

                    if ("xen".equals(templatePA)){
                        sb.append("            <templateType>0</templateType>\n");
                           } else {
                        sb.append("            <templateType>1</templateType>\n");
                           }
                           
                }
                sb.append("        </item>\n");
            }
        } catch (FactException ex) {
            return throwError(errNo(5), "Get fact error. "+ex.getMessage());
        }
        sb.append("    </templateSet>\n");
        sb.append("</results>\n");
        return sb.toString();
	}
}
