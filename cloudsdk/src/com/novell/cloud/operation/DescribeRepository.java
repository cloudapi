/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;

public class DescribeRepository extends Op {

	private ArrayList<String> repNameList = new ArrayList<String>();

	public DescribeRepository() {
		super("DescribeRepository", "Get the infomation a existed repository",
				18);
	}

	public DescribeRepository(ClientAgent client) {
		super(client, "DescribeRepository",
				"Get the infomation a existed repository", 18);
	}

	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);
		if (root.getAttributes().getLength() > 0) {
			NodeList nodes0 = root.getChildNodes();
			for (int i = 0; i < nodes0.getLength(); i++) {
				Node node0 = nodes0.item(i);
				if (node0.getNodeType() == Node.ELEMENT_NODE) {
					if (node0.getNodeName().equals("repSet")) {
						NodeList nodes1 = node0.getChildNodes();
						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
							Node node1 = nodes1.item(i1);
							if (node1.getNodeType() == Node.ELEMENT_NODE) {
								if (node1.getNodeName().equals("item")) {
									NodeList nodes2 = node1.getChildNodes();
									for (int i2 = 0; i2 < nodes2.getLength(); i2++) {
										Node node2 = nodes2.item(i2);
										if (node2.getNodeType() == Node.ELEMENT_NODE) {
											if (node2.getNodeName().equals(
													"repName")) {
												NodeList nodes3 = node2
														.getChildNodes();
												String txt = "";
												for (int i3 = 0; i3 < nodes3
														.getLength(); i3++) {
													Node node3 = nodes3
															.item(i3);
													if (node3.getNodeType() == Node.TEXT_NODE) {
														txt = txt
																+ node3.getNodeValue();
													}
												}
												if (txt.trim().length() > 0) {
													repNameList.add(txt.trim());
												}
											}
										}
									}
								}
							}
						}
					} else if (node0.getNodeName().equals("sessionID")) {
						NodeList nodes1 = node0.getChildNodes();
						String txt = "";
						for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
							Node node1 = nodes1.item(i1);
							if (node1.getNodeType() == Node.TEXT_NODE) {
								txt = txt + node1.getNodeValue();
							}
						}
						session = txt.trim();
					}
				}
			}
		}
		return 0;
	}

	protected int checkParameters() throws Exception {
		return 0;
	}

	public String execute(String xmlData) throws GridException {
		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

		String strCon = "<or>\n";
		for (int i = 0; i < repNameList.size(); i++) {
			strCon += "<eq fact=\"repository.id\" value=\""
					+ repNameList.get(i) + "\"/>\n";
		}
		strCon += "</or>\n";

		if (repNameList.size() == 0) {
			strCon = "<ne fact=\"repository.id\" value=\"null\" />\n";
		}

		GridObjectInfo[] repositories = getClient().getGridObjects(
				this.TYPE_REPOSITORY, strCon, true);

		if (repositories.length == 0) {
			return throwError(errNo(3), "Cound not find any repository");
		}

		StringBuffer sb = new StringBuffer(XML_HEADER);

		indent(0, sb, "<results>");
		indent(1, sb, "<result>" + 0 + "</result>");
		indent(1, sb, "<describeRepSet>");

		for (int i = 0; i < repositories.length; i++) {
			GridObjectInfo repo = repositories[i];
			indent(2, sb, "<item>");
			try {
				Fact f;
				f = repo.getEffectiveFact("repository.id");
				if (f != null) {
					indent(3, sb, "<repName>" + f.getValue() + "</repName>");
				}
				f = repo.getEffectiveFact("repository.type");
				if (f != null) {
					indent(3, sb, "<repType>" + f.getValue() + "</repType>");
				}
				f = repo.getEffectiveFact("repository.description");
				if (f != null) {
					indent(3, sb, "<description>" + f.getValue()
							+ "</description>");
				}

				long[] tot_free = new long[] { 0, 0 };
				try {
					StringBuffer buf = reportContainedVGs(repo, 4, tot_free);
					indent(3, sb, "<total>" + tot_free[0] + "</total>");
					indent(3, sb, "<used>" + (tot_free[0] - tot_free[1])
							+ "</used>");
					indent(3, sb, "<free>" + tot_free[1] + "</free>");
					indent(3, sb, "<vgSet>");
					sb.append(buf.toString());
					indent(3, sb, "</vgSet>");
				} catch (Exception ex) {
					System.err.println("Fail to get the vg information: "
							+ ex.getMessage());
				}

				f = repo.getEffectiveFact("repository.IWM.VG.local");
				if (f != null) {
					indent(3, sb, "<localDiskVG>" + f.getBooleanValue()
							+ "</localDiskVG>");
				}
				f = repo.getEffectiveFact("repository.HA.lock");
				if (f != null) {
					indent(3, sb, "<supportHA>" + f.getBooleanValue()
							+ "</supportHA>");
				}
				f = repo.getEffectiveFact("repository.HA.bindnetaddr");
				if (f != null) {
					indent(3, sb, "<bindnetaddr>" + f.getValue()
							+ "</bindnetaddr>");
				}
				f = repo.getEffectiveFact("repository.HA.mcastaddr");
				if (f != null) {
					indent(3, sb, "<mcastaddr>" + f.getValue() + "</mcastaddr>");
				}
				f = repo.getEffectiveFact("repository.HA.mcastport");
				if (f != null) {
					indent(3, sb, "<mcastport>" + f.getValue() + "</mcastport>");
				}
			} catch (com.novell.zos.grid.FactException ex) {
			}
			indent(2, sb, "</item>");
		}

		indent(1, sb, "</describeRepSet>");
		indent(0, sb, "</results>");
		return sb.toString();
	}

	private StringBuffer reportContainedVGs(GridObjectInfo repository,
			int level, long[] tot_free) throws Exception {
		StringBuffer sb = new StringBuffer();
		List vgList = new ArrayList();

		Fact f = repository.getEffectiveFact("repository.SAN.VGs");
		if (null == f)
			return sb;

		// check provisioner
		String[] paJobs = repository.getEffectiveFact(
				"repository.provisioner.jobs").getStringArrayValue();
		boolean isVsphere = false;
		for (int i = 0; i < paJobs.length; i++) {
			if ("vsphere".equalsIgnoreCase(paJobs[i])) {
				isVsphere = true;
				break;
			}
		}

		if (isVsphere) {
			String[] sanVgs = f.getStringArrayValue();
			for (String vg : sanVgs) {
				// check if this is a vsphere vg (i.e. if vg is really a
				// datastore/repository...

				GridObjectInfo repo_vg = null;
				try {
					repo_vg = getClient().getGridObjectInfo(
							this.TYPE_REPOSITORY, vg);
				} catch (Exception e) {

				}
				if (null != repo_vg) {
					// last sanity check
					if (null != repo_vg.getEffectiveFact("repository.mor_path")) {
						f = repo_vg.getEffectiveFact("repository.AIS.vgs");
						vgList.addAll(f.getListValue()); // should be only 1
															// value in the
															// list...
					}
				}
			}
		}

		if (vgList.isEmpty()) {
			// we didn't contain any vsphere repos, so let's just process the
			// original...
			f = repository.getEffectiveFact("repository.AIS.vgs");
			if (null != f) {
				vgList.addAll(f.getListValue());
			}
		}

		// now just process whatever's in the list...
		for (int j = 0; j < vgList.size(); j++) {
			indent(level, sb, "<item>");
			Map vg = (Map) vgList.get(j);
			try {
				tot_free[0] += Long.parseLong("" + vg.get("total"));
				tot_free[1] += Long.parseLong("" + vg.get("free"));
			} catch (NumberFormatException ex) {
				// not recoverable...
				System.err.println(ex.getMessage());
			}

			for (Object item : vg.entrySet()) {
				Map.Entry entry = (Map.Entry) item;
				String key = entry.getKey().toString();
				String val = entry.getValue().toString();
				indent(level + 1, sb, "<" + key + ">" + val + "</" + key + ">");
			}
			indent(level, sb, "</item>");
		}
		return sb;
	}
}
