/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;

public class DescribePhysicalHost extends Op {
	
	private ArrayList<String> hostNameList = new ArrayList<String>();

	public DescribePhysicalHost() {
		super("DescribePhysicalHost", "Get the information of the phsical host", 6);
	}
	
	public DescribePhysicalHost(ClientAgent client) {
		super(client, "DescribePhysicalHost", "Get the information of the phsical host", 6);
	}
	
	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);
		
		if (root.getAttributes().getLength()>0) {
            NodeList nodes0 = root.getChildNodes();
            for (int i = 0; i < nodes0.getLength(); i++) {
                 Node node0 = nodes0.item(i);
                 if (node0.getNodeType() == Node.ELEMENT_NODE) {
                    if (node0.getNodeName().equals("physicalSet")){
                                 NodeList nodes1 = node0.getChildNodes();
                                 for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                      Node node1 = nodes1.item(i1);
                                      if (node1.getNodeType() == Node.ELEMENT_NODE) {
                                           if (node1.getNodeName().equals("item"))
                                           {
                                               NodeList nodes2 = node1.getChildNodes();
                                               for (int i2 = 0; i2 < nodes2.getLength(); i2++) {
                                                    Node node2 = nodes2.item(i2);
                                                    if (node2.getNodeType() == Node.ELEMENT_NODE) {
                                                          if (node2.getNodeName().equals("hostName"))
                                                          {
                                                                NodeList nodes3 = node2.getChildNodes();
                                                                String txt="";
                                                                for (int i3 = 0; i3 < nodes3.getLength(); i3++) {
                                                                    Node node3 = nodes3.item(i3);
                                                                    if (node3.getNodeType() == Node.TEXT_NODE) {
                                                                       txt = txt + node3.getNodeValue();
                                                                    }
                                                                }                                                                
                                                                hostNameList.add(txt.trim());
                                                           }
                                                    }
                                               }
                                           }
                                      }
                                 }
                    }else if (node0.getNodeName().equals("sessionID")){
                          NodeList nodes1 = node0.getChildNodes();
                          String txt="";
                          for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                               Node node1 = nodes1.item(i1);
                               if (node1.getNodeType() == Node.TEXT_NODE) {
                               txt = txt + node1.getNodeValue();
                               }
                          }
                          session = txt.trim();
                    }
                 }
            }
        }
		
		return 0;
	}
	
	
	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}
		
		//TODO: update the host info in advance
		
		StringBuffer sb = new StringBuffer(XML_HEADER);
        sb.append("<results>\n");
		
		try {			
			String[] hostNames  = ((String[])hostNameList.toArray(new String[0]));
			
			String strCon = "";           

            if(hostNames.length == 0) {
               strCon = "<and>\n<eq fact=\"resource.type\" value=\"Fixed Physical\"/>\n</and>\n";
            }
            else {
               strCon = "<and>\n<eq fact=\"resource.type\" value=\"Fixed Physical\"/>\n<or>\n";
               for (int i = 0; i < hostNames.length; ++i) {
                    strCon = strCon + "<eq fact=\"resource.id\" value=\"" + hostNames[i] + "\"/>\n";
               }
               strCon += "</or>\n</and>\n";
            }
            
            GridObjectInfo[] arrayOfNodeInfo = getClient().getGridObjects(TYPE_RESOURCE, strCon, true);
            
            if( arrayOfNodeInfo == null || arrayOfNodeInfo.length == 0){
            	throw new Exception("There is no physical hosts found");
            }            
                        
            sb.append("    <result>"+ 0 +"</result>\n");
            sb.append("    <physicalSet>\n");
            
            Fact f = null;
            
            for (int i = 0; i < arrayOfNodeInfo.length; ++i) {
            	GridObjectInfo phyNode = arrayOfNodeInfo[i];
            	f = phyNode.getEffectiveFact("resource.id");
            	String hostName = f.getStringValue();
            	sb.append("        <item>\n");
                sb.append("            <hostName>" + hostName +"</hostName>\n");
                f = phyNode.getEffectiveFact("resource.os.vendor.string");
                if ( f != null ){
                    sb.append("            <osVersion>" + f.getValue() + "</osVersion>\n");
                }
                f = phyNode.getEffectiveFact("resource.ip");
                if ( f != null ){
                    sb.append("            <ipAddress>" + f.getValue() + "</ipAddress>\n");
                }
                f = phyNode.getEffectiveFact("resource.cpu.numCpuCores");
                if ( f != null ){
                    sb.append("            <cpuNum>" + f.getValue() +"</cpuNum>\n");
                }else{
                f = phyNode.getEffectiveFact("resource.cpu.number");
                if ( f != null ){
                    sb.append("            <cpuNum>" + f.getValue() +"</cpuNum>\n");
                       }}
                f = phyNode.getEffectiveFact("resource.cpu.model");
                if ( f != null ){
                    sb.append("            <cpuType>" + f.getValue() + "</cpuType>\n");
                }

                f = phyNode.getEffectiveFact("resource.iwm.cpu.mhz");
                if ( f != null ){
                    sb.append("            <frequency>" + f.getValue() + "</frequency>\n");
                }else{
                    f = phyNode.getEffectiveFact("resource.cpu.mhz");
                    if ( f != null ){
                        sb.append("            <frequency>" + f.getValue() + "</frequency>\n");
                    }
                }
                f = phyNode.getEffectiveFact("resource.iwm.memory.physical.total");
                if ( f != null ){
                    sb.append("            <memory>" + f.getValue() + "</memory>\n");
                }else{
                    f = phyNode.getEffectiveFact("resource.memory.physical.total");
                    if ( f != null ){
                        sb.append("            <memory>" + f.getValue() + "</memory>\n");
                    }else{    // Necessary key, return '-1' if can not get real value
                        sb.append("            <memory>" + -1 + "</memory>\n");
                    }
                }

                f = phyNode.getEffectiveFact("resource.AIS.nics");
                if ( f != null ){
                    List nics = f.getListValue();
                    sb.append("            <nicSet>\n");
                    for (int j=0 ; j < nics.size(); j++)
                    {
                       sb.append("                <item>\n");
                       Map nic = (Map)nics.get(j);
                       Iterator iT = nic.keySet().iterator();
                       while (iT.hasNext()) {
                          String aKey = iT.next().toString();
                          sb.append("                    <"+aKey+">" + nic.get(aKey) + "</"+aKey+">\n");
                       }
                       sb.append("                </item>\n");
                    }
                    sb.append("            </nicSet>\n");
                }

                f = phyNode.getEffectiveFact("resource.AIS.disks");
                if ( f != null ){
                    List disks = f.getListValue();
                    sb.append("            <diskSet>\n");
                    for (int j=0 ; j < disks.size(); j++)
                    {
                       sb.append("                <item>\n");
                       Map disk = (Map)disks.get(j);
                       Iterator iT = disk.keySet().iterator();
                       while (iT.hasNext()) {
                          String aKey = iT.next().toString();
                          sb.append("                    <"+aKey+">" + disk.get(aKey) + "</"+aKey+">\n");
                       }
                       sb.append("                </item>\n");
                    }
                    sb.append("            </diskSet>\n");
                }

                f = phyNode.getEffectiveFact("resource.AIS.vgs");
                if ( f != null ){
                    List vgs = f.getListValue();
                    sb.append("            <vgSet>\n");
                    for (int j=0 ; j < vgs.size(); j++)
                    {
                       sb.append("                <item>\n");
                       Map vg = (Map)vgs.get(j);
                       Iterator iT = vg.keySet().iterator();
                       while (iT.hasNext()) {
                          String aKey = iT.next().toString();
                          sb.append("                    <"+aKey+">" + vg.get(aKey) + "</"+aKey+">\n");
                       }
                       sb.append("                </item>\n");
                    }
                    sb.append("            </vgSet>\n");
                }

                sb.append("        </item>\n");
            }
            sb.append("    </physicalSet>\n");
            sb.append("</results>\n");
		} catch (Exception ex) {
			return throwError(errNo(9), ex.getMessage());
		}

		sb.append("</results>");

		return sb.toString();
	}
}
