/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Arrays;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.GridObjectNotFoundException;

public class AddVirtDeviceToHost extends Op {

    private String deviceType = null;
    private String deviceName = null;
    private String baseDevType = null;
    private String baseDevName = null;
    private String hostName = null;
    private String vlanId = null;
    long n_deviceType = 0;
    long n_baseDevType = 0;

	public AddVirtDeviceToHost() {
		super("AddVirtDeviceToHost", "Add network in the host", 53);
	}

	public AddVirtDeviceToHost(ClientAgent client) {
		super(client,"AddVirtDeviceToHost", "Add network in the host", 53);
	}

	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);

            if (root.getAttributes().getLength()>0) {
                NodeList nodes0 = root.getChildNodes();
                for (int i = 0; i < nodes0.getLength(); i++) {
                     Node node0 = nodes0.item(i);
                     if (node0.getNodeType() == Node.ELEMENT_NODE) {
                        if (node0.getNodeName().equals("sessionID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              session = txt.trim();
                        } else if (node0.getNodeName().equals("deviceType")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              deviceType = txt.trim();
                        } else if (node0.getNodeName().equals("deviceName")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              deviceName = txt.trim();
                        } else if (node0.getNodeName().equals("baseDevType")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              baseDevType = txt.trim();
                        } else if (node0.getNodeName().equals("baseDevName")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              baseDevName = txt.trim();
                        } else if (node0.getNodeName().equals("hostName")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              hostName = txt.trim();
                        }
                        else if (node0.getNodeName().equals("pgVlanID")){
                            NodeList nodes1 = node0.getChildNodes();
                            String txt="";
                            for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                 Node node1 = nodes1.item(i1);
                                 if (node1.getNodeType() == Node.TEXT_NODE) {
                                 txt = txt + node1.getNodeValue();
                                 }
                            }
                            vlanId = txt.trim();
                      }
                     }
                }
            }
		return 0;
	}

	protected int checkParameters() throws Exception {

        if (IsNull(deviceType)){
            throw new Exception("Parameter error: deviceType can't be empty.");
        }
        if (IsNull(deviceName)){
            throw new Exception("Parameter error: deviceName can't be empty.");
        }
        if (IsNull(baseDevType)){
            throw new Exception("Parameter error: baseDevType can't be empty.");
        }
        if (IsNull(baseDevName)){
            throw new Exception("Parameter error: baseDevName can't be empty.");
        }
        if (IsNull(hostName)){
            throw new Exception("Parameter error: hostName can't be empty.");
        }
        
        n_deviceType = Long.parseLong(deviceType);
	    n_baseDevType = Long.parseLong(baseDevType);
       
        ////// Check if host exists ////////////
        GridObjectInfo host = getGridObject(TYPE_RESOURCE,hostName);
        if (host == null){
            throw new Exception("Can't find host '" + hostName + "'.");
        }
        
        ////// TODO: check baseDevName and deviceName
        
        return 0;
	}

	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

        String jobName = "vlanmanager";
		JobSpec job = this.getJob(jobName);
        if (job == null){
            return throwError(errNo(3),"Job '" + jobName +"' is not deployed.");
        }
        
		JobDef jobDef = new JobDef();
		jobDef.setName(jobName);
		jobDef.setJob(job);
		jobDef.resetImports();
        jobDef.setImport("jobargs.action", "add");
        jobDef.setImport("jobargs.deviceType", n_deviceType);
        jobDef.setImport("jobargs.deviceName", deviceName);
        jobDef.setImport("jobargs.baseDevType", n_baseDevType);
        jobDef.setImport("jobargs.baseDevName", baseDevName);
        jobDef.setImport("jobargs.host", hostName);
        if(! IsNull(vlanId))
        	jobDef.setImport("jobargs.vlanId", vlanId);
		jobDef.setWhatComplete(true);
		jobDef.setWaitForCompletion(true);

		RunJob runJob = new RunJob(getClient());
		try {
			if (!runJob.run(jobDef)) {
				return throwError(errNo(4), "Error running job '" + jobName
						+ "'. \nJob Log:\n" + getJobLog(runJob.getJobId()));
			}
		} catch (Exception ex) {
			return throwError(errNo(5), ex.getMessage());
		}

        String jobID = runJob.getJobId();
        StringBuffer sb = new StringBuffer(XML_HEADER);
        sb.append("<results>\n");
        sb.append("    <result>"+ 0 +"</result>\n");
        sb.append("</results>");

        return sb.toString();
	}
}
