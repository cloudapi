/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.Op;
import com.novell.cloud.api.Instance;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;

public class GetInstanceState extends Instance {
    private GridObjectInfo host = null;

	public GetInstanceState() {
		super("GetInstanceState", "Get the state of the vm instance", 37);
	}

	public GetInstanceState(ClientAgent client) {
		super(client,"GetInstanceState", "Get the state of the vm instance", 37);
	}

	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

	    ////// Generate the Response Information ///////////
        StringBuffer sb = new StringBuffer(XML_HEADER);
        sb.append("<results>\n");
        sb.append("    <result>"+ 0 +"</result>\n");

        Fact f;
        String provisionState = "";
        String status = "";
        String state = "";
        try {
            f = vmNode.getEffectiveFact("resource.provision.state");
            if ( f != null ){
              provisionState = f.getStringValue();
              sb.append("    <provision_state>"+ provisionState +"</provision_state>\n");
            }
            f = vmNode.getEffectiveFact("resource.provision.status");
            if ( f != null ){
              status = f.getStringValue();
              sb.append("    <provision_status>"+ status +"</provision_status>\n");
            }
            
        } catch (FactException ex){
            return throwError(errNo(3), "Get fact error: "+ex.getMessage());
        }
        
        try{
            state = getInstanceState(vmNode);
        }catch(Exception ex){
            return throwError(errNo(4), "Get state error: "+ex.getMessage());
        }
        sb.append("    <state>"+ state +"</state>\n");
        sb.append("</results>\n");
        return sb.toString();
	}

	public String getInstanceState(GridObjectInfo vm) throws Exception {
        Fact f;
        String provisionState = "";
        String status = "";
        String state = "";
        try {
            f = vm.getEffectiveFact("resource.provision.state");
            if ( f != null ){
              provisionState = f.getStringValue();
            }
            f = vm.getEffectiveFact("resource.provision.status");
            if ( f != null ){
              status = f.getStringValue();
            }
        } catch (FactException ex){
            throw new Exception("Get fact error: "+ex.getMessage());
        }
        
        if (provisionState.toLowerCase().equals("suspended") ||
            provisionState.toLowerCase().equals("paused")) {
            state = "suspend";
        } else if (status.equals("Suspending VM") ||
                   status.equals("Pausing VM")) {
            state = "suspending";
        } else if (status.equals("Resuming VM") ||
                   status.equals("Restoring")) {
            state = "unsuspending";
        } else if (status.equals("Building VM") || 
                   status.equals("Cloning VM")) {
            state = "creating";
        } else if (status.equals("VM shutting down")) {
            state = "shutdowning";
        } else if (status.equals("Starting VM")) {
            state = "starting";
        } else if (status.equals("Restarting VM")) {
            state = "restarting";
        } else if (status.equals("Resyncing VM status")) {
            state = "resyncing";
        } else if (status.equals("Migrating VM")) {
            state = "migrating";
        } else if (status.equals("Destroying VM")) {
            state = "destroying";
        } else if (status.equals("VM made standalone")) {
            state = provisionState.toLowerCase();
        } else if (!provisionState.equals("unknown")){
            if (provisionState.equals("down")) {
                state = _getInstanceState(vm);
            } else {
                state = provisionState.toLowerCase();
            }
        } else {
            state = "unknown";
        }
        return state;
    }
    
    public String _getInstanceState(GridObjectInfo vm) throws Exception {
        String hostName = "";
        Fact f = vm.getEffectiveFact("resource.provision.last.vmhost.resource");
        if ( f != null ){
            try {
                hostName = f.getStringValue();
            } catch (FactException ex){
                throw new Exception("Get fact error: "+ex.getMessage());
            }

            try{
                host = getClient().getGridObjectInfo(TYPE_RESOURCE,hostName);
            }catch(Exception ex){
            }
            if ( host == null ){
                return "unknown";
            } else {
                if ( "suspect".equals(new GetPhysicalHostState(getClient()).getPhysicalHostState(host)) ) {
                    return "unknown";
                } else {
                    return "down";
                }
            }

        } else {
            return "down";
        }
    }

}
