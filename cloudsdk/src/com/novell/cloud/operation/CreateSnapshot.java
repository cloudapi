/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Arrays;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.GridObjectNotFoundException;

public class CreateSnapshot extends Op {

    private static String SNAPSHOT_FACT_NAME = "resource.vm.iwm.snapshot.id";
    private String instanceID = null;
    private String snapshotName = null;
    private String oldSnapshotID = null;
    private String isCreateMemSnapshot = null;
    private boolean bIsCreateMemSnapshot = true;
    private String description = null;
    private String backupDirectory = null;      //optional
    private String fullBackup = null;           //optional
    private boolean bFullBackup = true;         // optional, default true
    private GridObjectInfo vmnode = null;

	public CreateSnapshot() {
		super("CreateSnapshot", "Backup the vm instance", 42);
	}

	public CreateSnapshot(ClientAgent client) {
		super(client,"CreateSnapshot", "Backup the vm instance", 42);
	}

	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);

            if (root.getAttributes().getLength()>0) {
                NodeList nodes0 = root.getChildNodes();
                for (int i = 0; i < nodes0.getLength(); i++) {
                     Node node0 = nodes0.item(i);
                     if (node0.getNodeType() == Node.ELEMENT_NODE) {
                        if (node0.getNodeName().equals("sessionID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              session = txt.trim();
                        } else if (node0.getNodeName().equals("instanceID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              instanceID = txt.trim();
                        } else if (node0.getNodeName().equals("snapshotName")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              snapshotName = txt.trim();
                        } else if (node0.getNodeName().equals("isCreateMemSnapshot")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              isCreateMemSnapshot = txt.trim();
                        } else if (node0.getNodeName().equals("description")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              description = txt.trim();
                        } else if (node0.getNodeName().equals("backupDirectory")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              backupDirectory = txt.trim();
                        } else if (node0.getNodeName().equals("fullBackup")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              fullBackup = txt.trim();
                        }
                     }
                }
            }
		return 0;
	}

	protected int checkParameters() throws Exception {

        if (IsNull(instanceID)){
            throw new Exception("Parameter error: instanceID can't be empty.");
        }

        if (IsNull(snapshotName)){
            throw new Exception( "Parameter error: snapshotName can't be empty.");
        }
        if (IsNull(isCreateMemSnapshot)){
            throw new Exception( "Parameter error: isCreateMemSnapshot can't be empty.");
        }
        if (isCreateMemSnapshot.toLowerCase().equals("true")){
            bIsCreateMemSnapshot = true;
        } else if (isCreateMemSnapshot.toLowerCase().equals("false")){
            bIsCreateMemSnapshot = false;
        } else {
            throw new Exception("Parameter error: isCreateMemSnapshot must be \"true\" or \"false\".");
        }
        if (IsNotNull(fullBackup)){
            if (fullBackup.toLowerCase().equals("true")){
                bFullBackup = true;
            } else if (fullBackup.toLowerCase().equals("false")){
                bFullBackup = false;
            } else {
                throw new Exception( "Parameter error: fullBackup must be 'true' or 'false'");
            }
        }
        ////// TODO: checke backupDirectory //////
        if (IsNotNull(backupDirectory)){
        
        }

        vmnode = getGridObject(TYPE_RESOURCE,instanceID);

        if (vmnode == null){
            throw new Exception("VM '"+instanceID+"' doesn't exist.");
        }
		if (! vmnode.getEffectiveFact("resource.type").getStringValue().equalsIgnoreCase(TYPE_VM_INSTANCE)){
			throw new Exception("Resource: "
					+ instanceID + " is not a vm instance");
		}

        Fact f = vmnode.getEffectiveFact("resource.vm.underconstruction");
		if (f != null && f.getBooleanValue()){
			throw new Exception("Resource: "
					+ instanceID + " is under construction");
		}

        try{
            getClient().setFact(TYPE_RESOURCE,instanceID,SNAPSHOT_FACT_NAME,com.novell.zos.grid.Fact.TYPE_STRING, "");
        }catch(FactException ex){
            throw new Exception("Fail to set the fact to null");
        }
		return 0;
	}

	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

        HashMap<String,Object> params = new HashMap<String,Object>();
        params.put("vmID", instanceID);
        params.put("snapshotName", snapshotName);
        params.put("isCreateMemSnapshot", bIsCreateMemSnapshot);
        params.put("description", description);
        if (IsNotNull(backupDirectory)){
            params.put("backupDirectory", backupDirectory);
        }
        if (IsNotNull(fullBackup)){
            params.put("fullBackup", bFullBackup);
        }

        String jobName = "vmbackup";
        try{
        	if(getProvisionerJob(vmnode).equalsIgnoreCase("vsphere"))
        		jobName = "vmbackup_vs";
        }catch(FactException e){
        	return throwError(errNo(3), "Get fact error. "+e.getMessage());
        }

		JobSpec job = this.getJob(jobName);
        if (job == null){
            return throwError(errNo(4),"Job '" + jobName +"' is not deployed.");
        }
        
		JobDef jobDef = new JobDef();
		jobDef.setName(jobName);
		jobDef.setJob(job);
		jobDef.resetImports();
        jobDef.setImport("jobargs.action", "backup");
        jobDef.setImport("jobargs.params", params);
		jobDef.setWhatComplete(true);
		jobDef.setWaitForCompletion(false);

		RunJob runJob = new RunJob(getClient());
		try {
			if (!runJob.run(jobDef)) {
				return throwError(errNo(6), "Error running job '" + jobName
						+ "'. \nJob Log:\n" + getJobLog(runJob.getJobId()));
			}
		} catch (Exception ex) {
			return throwError(errNo(7), ex.getMessage());
		}

        String jobID = runJob.getJobId();

        String strCon = "<and>\n<eq fact=\"resource.id\" value=\"instanceID\"/>\n</and>\n";
        strCon += "<and>\n<ne fact=\"resource.vm.iwm.snapshot.id\" value=\"\"/>\n</and>\n";

        GridObjectInfo[] nodeInfos = null;
        try{
        	nodeInfos = GetInfoFromJob(strCon,jobID);
        }catch( Exception ex){
        	return throwError(errNo(8), "Timeout to query the tempalte id from the job.\nJob Log:\n"+getJobLog(jobID) + "\n jobID:" + jobID);
        }
        
        if(nodeInfos == null || nodeInfos.length == 0){
			return throwError(errNo(9), "Create VmTemplate from instance '"+instanceID+"' failed.\nJob Log:\n"+getJobLog(jobID) + "\n jobID:" + jobID);
        }

        String snapshotID = vmnode.getEffectiveFact(SNAPSHOT_FACT_NAME).getStringValue();

        StringBuffer sb = new StringBuffer(XML_HEADER);
        sb.append("<results>\n");
        sb.append("    <result>"+ 0 +"</result>\n");          // result
        sb.append("    <jobID>" + jobID + "</jobID>\n");
        sb.append("    <snapshotID>" + snapshotID + "</snapshotID>\n");
        sb.append("</results>\n");

        return sb.toString();
	}
}
