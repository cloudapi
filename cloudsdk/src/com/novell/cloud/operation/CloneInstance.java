/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.util.ArrayList;
import java.util.HashMap;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.GridObjectNotFoundException;

public class CloneInstance extends Op {

    private String instanceID = null;
    private String phyHostName = null;
    private String repName = null;
    private String hostName = null;
    private String isStartAfterClone = null;
    private GridObjectInfo vmnode = null;

	public CloneInstance() {
		super("CloneInstance", "clone a vm instance", 34);
	}

	public CloneInstance(ClientAgent client) {
		super(client,"CloneInstance", "clone a vm instance", 34);
	}

	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);

            if (root.getAttributes().getLength()>0) {
                NodeList nodes0 = root.getChildNodes();
                for (int i = 0; i < nodes0.getLength(); i++) {
                     Node node0 = nodes0.item(i);
                     if (node0.getNodeType() == Node.ELEMENT_NODE) {
                        if (node0.getNodeName().equals("sessionID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              session = txt.trim();
                        } else if (node0.getNodeName().equals("instanceID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              instanceID = txt.trim();
                        } else if (node0.getNodeName().equals("phyHostName")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              phyHostName = txt.trim();
                        } else if (node0.getNodeName().equals("repName")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              repName = txt.trim();
                        } else if (node0.getNodeName().equals("hostName")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              hostName = txt.trim();
                        } else if (node0.getNodeName().equals("isStartAfterClone")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                   txt = txt + node1.getNodeValue();
                                   }
                              }
                              isStartAfterClone = txt.trim();
                        }
                     }
                }
            }
		return 0;
	}

	protected int checkParameters() throws Exception {

        if (IsNull(instanceID)){
            throw new Exception( "Parameter error: instanceID can't be empty.");
        }

        if (IsNull(phyHostName)){
            throw new Exception( "Parameter error: phyHostName can't be empty.");
        }

        ////// Check if instance exists
        GridObjectInfo node = getGridObject(TYPE_RESOURCE,phyHostName);
        if (node == null){
            throw new Exception("host '"+phyHostName+"' doesn't exist.");
        }
		if (! node.getEffectiveFact("resource.type").getStringValue().equalsIgnoreCase(TYPE_FIXED_PHYSICAL)){
			throw new Exception("Resource: " + phyHostName + " is not a physical host");
		}

        GridObjectInfo repoInfo = getGridObject(TYPE_REPOSITORY,repName);
        if( repoInfo == null ){
            throw new Exception("Repository '"+repName+"' doesn't exist.");
        }

        vmnode = getGridObject(TYPE_RESOURCE,instanceID);

        if (vmnode == null){
            throw new Exception("VM '"+instanceID+"' doesn't exist.");
        }
		if (! vmnode.getEffectiveFact("resource.type").getStringValue().equalsIgnoreCase(TYPE_VM_INSTANCE)){
			throw new Exception("Resource: "
					+ instanceID + " is not a vm instance");
		}

        isStartAfterClone = isStartAfterClone.toLowerCase();
        if (!(isStartAfterClone.equals("true")||isStartAfterClone.equals("false"))){
            throw new Exception("\"isStartAfterClone\" MUST be true of false.");
        }
        return 0;
	}

	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

        ////// Call job to clone instance ///////
        String jobName = "vmclone";
        try{
        	if(getProvisionerJob(vmnode).equalsIgnoreCase("vsphere"))
        		jobName = "vmclone_vs";
        }catch(FactException e){
        	return throwError(errNo(3), "Get fact error. "+e.getMessage());
        }
		JobSpec job = this.getJob(jobName);

		JobDef jobDef = new JobDef();
		jobDef.setName(jobName);
		jobDef.setJob(job);
		jobDef.resetImports();
        jobDef.setImport("jobargs.reponame", repName);
        jobDef.setImport("jobargs.vmname", instanceID);
        jobDef.setImport("jobargs.phyHost", phyHostName);
        jobDef.setImport("jobargs.newVmHostName", hostName);
        jobDef.setImport("jobargs.isStartAfterClone", isStartAfterClone);
		jobDef.setWhatComplete(true);
		jobDef.setWaitForCompletion(false);

		RunJob runJob = new RunJob(getClient());
		try {
			if (!runJob.run(jobDef)) {
				return throwError(errNo(3), "Error running job '" + jobName
						+ "'. \nJob Log:\n" + getJobLog(runJob.getJobId()));
			}
		} catch (Exception ex) {
			return throwError(errNo(4), ex.getMessage());
		}

        String jobID = runJob.getJobId();
		
        String strCon = "<and>\n<eq fact=\"resource.type\" value=\"VM\"/>\n</and>\n";
        strCon = "<and>\n<eq fact=\"resource.vm.iwm.initial.job\" value=\"" + jobID + "\"/>\n</and>\n";

        GridObjectInfo[] arrayOfNodeInfo = GetInfoFromJob(strCon, jobID);
        if ((arrayOfNodeInfo == null) || (arrayOfNodeInfo.length == 0)){
            return throwError(errNo(14), "Clone instance '"+instanceID+"' failed.\nJob Log:\n"+getJobLog(jobID));
        }

        try {
            instanceID = (String)arrayOfNodeInfo[0].getEffectiveFact("resource.id").getValue();
        }
        catch(FactException ex){
            return throwError(errNo(5), "Get fact error: "+ex.getMessage());
        }
        StringBuffer sb = new StringBuffer(XML_HEADER);

        sb.append("<results>\n");
        sb.append("    <result>"+ 0 +"</result>\n");          // result
        sb.append("    <jobID>" + jobID + "</jobID>\n");
        sb.append("    <newInstanceID>" + instanceID + "</newInstanceID>\n");
        sb.append("</results>\n");
        return sb.toString();


	}
}
