/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */
package com.novell.cloud.operation;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.GridObjectNotFoundException;

public class RegisterVMTemplate extends Op {

    private static final String RESOURCE_VM_IWM_TEMPLATE_REGISTERED = "resource.vm.IWM.template.registered";
    private String instanceID = null;
    private String description = null;
    private String vmTemplatePath = null;
    private String vm_name = null;
    private String vm_uuid = null;
    private String basepath = null;

	public RegisterVMTemplate() {
		super("RegisterVMTemplate", "Register a vm template", 21);
	}

	public RegisterVMTemplate(ClientAgent client) {
		super(client,"RegisterVMTemplate", "Register a vm template", 21);
	}

	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);

            if (root.getAttributes().getLength()>0) {
                NodeList nodes0 = root.getChildNodes();
                for (int i = 0; i < nodes0.getLength(); i++) {
                     Node node0 = nodes0.item(i);
                     if (node0.getNodeType() == Node.ELEMENT_NODE) {
                        if (node0.getNodeName().equals("sessionID")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                        txt = txt + node1.getNodeValue();
                                   }
                              }
                              session = txt.trim();
                        } else if (node0.getNodeName().equals("vmTemplatePath")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                        txt = txt + node1.getNodeValue();
                                   }
                              }
                              vmTemplatePath = txt.trim();
                        } else if (node0.getNodeName().equals("description")){
                              NodeList nodes1 = node0.getChildNodes();
                              String txt="";
                              for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                   Node node1 = nodes1.item(i1);
                                   if (node1.getNodeType() == Node.TEXT_NODE) {
                                        txt = txt + node1.getNodeValue();
                                   }
                              }
                              description = txt.trim();
                        }
                     }
                }
            }
		return 0;
	}

	protected int checkParameters() throws Exception {

		if (IsNull(vmTemplatePath)) {
			throw new Exception("Parameter error: vmTemplatePath can't be empty.");
		}

        if (!(vmTemplatePath.startsWith("grid://")||vmTemplatePath.startsWith("zcm://"))){
            throw new Exception("Parameter error: vmTemplatePath:"+vmTemplatePath+" is incorrect.");
        }

		return 0;
	}

	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

		String jobName = "vmtemplate";
		JobSpec job = this.getJob(jobName);

		JobDef jobDef = new JobDef();
		jobDef.setName(jobName);
		jobDef.setJob(job);
		jobDef.resetImports();
        jobDef.setImport("jobargs.templatePath", vmTemplatePath);
        jobDef.setImport("jobargs.description", description);
		jobDef.setWhatComplete(true);
		jobDef.setWaitForCompletion(true);

		RunJob runJob = new RunJob(getClient());
		try {
			if (!runJob.run(jobDef)) {
				return throwError(errNo(3), "Error running job '" + jobName
						+ "'. \nJob Log:\n" + getJobLog(runJob.getJobId()));
			}
		} catch (Exception ex) {
			return throwError(errNo(4), ex.getMessage());
		}
		
		String jobId = runJob.getJobId();

        String vmTemplateID = null;
        String strCon = "<and>\n<eq fact=\"resource.type\" value=\"VM Template\"/>\n</and>\n";
        strCon += "<and>\n<eq fact=\"resource.vm.basepath\" value=\"" + vmTemplatePath + "\"/>\n</and>\n";
        GridObjectInfo[] vms = null;
        try {
            vms = getClient().getGridObjects( TYPE_RESOURCE,strCon,true );
        } catch (Exception ex) {
            return throwError(errNo(5), "Can not find nodes: " + ex.getMessage());
        }
        if ((vms == null)||(vms.length == 0)){
            return throwError(errNo(6), "Can not find nodes");
        }

        try{        
            Fact f = vms[0].getEffectiveFact("resource.id");
            if (f!=null){
                vmTemplateID = f.getStringValue();
            }
            if (IsNull(vmTemplateID)){
                return throwError(errNo(7),  "Error registering vm template: no vmTemplateID found.\nJob Log:\n" + getJobLog(jobId));
            }
            f = vms[0].getEffectiveFact(RESOURCE_VM_IWM_TEMPLATE_REGISTERED);
            if (f == null || !f.getBooleanValue()){ // registered is false
                    return throwError(errNo(8),  "Error registering vm template '" + vmTemplateID + "'.\nJob Log:\n" + getJobLog(jobId));
            }
        } catch(FactException ex){
            return throwError(errNo(9),  "Get fact error: "+ex.getMessage());
        }

        StringBuffer sb = new StringBuffer(XML_HEADER);
        sb.append("<results>\n");
        sb.append("    <result>"+ 0 +"</result>\n");          // result
        //sb.append("    <vmTemplateID>" + vm_name + "</vmTemplateID>\n");
        sb.append("    <vmTemplateID>" + vmTemplateID + "</vmTemplateID>\n");
        //sb.append("    <jobID>" + jobID + "</jobID>\n");
        sb.append("</results>");
        return sb.toString();
	}
}
