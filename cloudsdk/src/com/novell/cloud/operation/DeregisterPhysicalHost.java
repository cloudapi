/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.GridException;


public class DeregisterPhysicalHost extends RegisterPhysicalHost {
	protected String hostName = null;
	protected String ipAddress = null;

	public DeregisterPhysicalHost() {
		super("DeregisterPhysicalHost", "deregister the physical host", 5);
	}
	
	public DeregisterPhysicalHost(ClientAgent client) {
		super(client, "DeregisterPhysicalHost", "deregister the physical host", 5);
	}


	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

		try {			
			String factname = "resource.AIS.registered";
			getClient().setFact(TYPE_RESOURCE, hostName, factname,
					com.novell.zos.grid.Fact.TYPE_BOOLEAN, false);

		} catch (Exception ex) {
			return throwError(errNo(9), ex.getMessage());
		}

		StringBuffer sb = new StringBuffer(XML_HEADER);
		sb.append("<results>\n");
		sb.append("    <result>0</result>\n");
		sb.append("</results>");

		return sb.toString();
	}
}
