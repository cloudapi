/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */
package com.novell.cloud.operation;

import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.Instance;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.GridObjectNotFoundException;

public class TerminateInstance extends Instance {

	public TerminateInstance() {
		super("TerminateInstance", "Stop a vm instance", 30);
	}

	public TerminateInstance(ClientAgent client) {
		super(client,"TerminateInstance", "Stop a vm instance", 30);
	}

	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

		String jobName = "vmoperator";
		JobSpec job = this.getJob(jobName);

		JobDef jobDef = new JobDef();
		jobDef.setName(jobName);
		jobDef.setJob(job);
		jobDef.resetImports();
        jobDef.setImport("jobargs.action", "shutdown");
        jobDef.setImport("jobargs.vmname", instanceID);
		jobDef.setWhatComplete(true);
		jobDef.setWaitForCompletion(false);

		RunJob runJob = new RunJob(getClient());
		try {
			if (!runJob.run(jobDef)) {
				return throwError(errNo(3), "Error running job '" + jobName
						+ "'. \nJob Log:\n" + getJobLog(runJob.getJobId()));
			}
		} catch (Exception ex) {
			return throwError(errNo(4), ex.getMessage());
		}

        String jobID = runJob.getJobId();

        UnregRemoteConsoleVNCPort(vmNode);
		
        StringBuffer sb = new StringBuffer(XML_HEADER);
        sb.append("<results>\n");
        sb.append("    <result>"+ 0 +"</result>\n");
        sb.append("    <jobID>" + jobID + "</jobID>\n");
        sb.append("</results>");
        return sb.toString();

	}

    private void UnregRemoteConsoleVNCPort(GridObjectInfo vmnode)

    {    	
    	String vmname = vmnode.getName();
    	//String vncip = vmnode.getVncIp();
    	//int vncport = vmnode.getVncPort();
    	//String cmdline = "/usr/sbin/regrdc unreg " + vmname + " " + vncip + " " + vncport;
    	String cmdline = "/usr/sbin/regrdc unreg " + vmname + " 10.10.10.0 5900";
    	try{    		
    		Process p = Runtime.getRuntime().exec(cmdline);
    		if(p.waitFor() == 0){
    			System.out.println("Success to unregister vnc port");
    			//vmnode.removeFact("resource.jnlp.vncIpAddress");
    			//vmnode.removeFact("resource.jnlp.vncPort");
    			//vmnode.removeFact("resource.jnlp.codebase");
    			//vmnode.removeFact("resource.jnlp.certData");
    		}    		
    	}catch(Exception ex){
    		System.err.println( "Fail to unregister remote console: "+ex.getMessage());
    	}
    }

}
