/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Arrays;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.GridObjectNotFoundException;

public class ModifyInstance extends Op {

    private String instanceID = null;
    private String vmhostName =null;
    private String cpuNum =null;
    private String frequency =null;
    private String minMem =null;
    private String maxMem =null;
    private ArrayList<Map> nicSet = new ArrayList<Map>();
    private ArrayList<Map> diskSet = new ArrayList<Map>();

	public ModifyInstance() {
		super("ModifyInstance", "Modify a existed instance", 35);
	}

	public ModifyInstance(ClientAgent client) {
		super(client,"ModifyInstance", "Modify a existed instance", 35);
	}

	protected int getParameters(String xmlData) throws Exception {
		Element root = XMLHelper.GetXmlRootElement(xmlData);

            if (root.getAttributes().getLength()>0) {
                NodeList nodes0 = root.getChildNodes();
                for (int i = 0; i < nodes0.getLength(); i++) {
                    Node node0 = nodes0.item(i);
                    if (node0.getNodeType() == Node.ELEMENT_NODE) {
                        if (node0.getNodeName().equals("sessionID")){
                            session = getTrimmedChildText(node0);
                              
                        } else if (node0.getNodeName().equals("instanceID")){
                            instanceID = getTrimmedChildText(node0);
                              
                        } else if (node0.getNodeName().equals("vmhostName")){
                            vmhostName = getTrimmedChildText(node0);
                              
                        } else if (node0.getNodeName().equals("cpuNum")){
                            cpuNum = getTrimmedChildText(node0);
                              
                        } else if (node0.getNodeName().equals("frequency")){
                            frequency = getTrimmedChildText(node0);
                              
                        } else if (node0.getNodeName().equals("memInfo")){
                            NodeList nodes1 = node0.getChildNodes();
                            for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                Node node1 = nodes1.item(i1);
                                if (node1.getNodeType() == Node.ELEMENT_NODE) {
                                    if (node1.getNodeName().equals("minMem")){
                                        minMem = getTrimmedChildText(node1);
                                       
                                    } else if (node1.getNodeName().equals("maxMem")){
                                        maxMem = getTrimmedChildText(node1);
                                       
                                    }
                                }
                            }
                        } else if (node0.getNodeName().equals("nicSet")){
                            NodeList nodes1 = node0.getChildNodes();
                            for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                Node node1 = nodes1.item(i1);
                                if (node1.getNodeType() == Node.ELEMENT_NODE) {
                                    if (node1.getNodeName().equals("item")){
                                        NodeList nodes2 = node1.getChildNodes();
                                        HashMap<String,String> nicSetMap = new HashMap<String,String>();
                                        for (int i2 = 0; i2 < nodes2.getLength(); i2++) {
                                            Node node2 = nodes2.item(i2);
                                            if (node2.getNodeType() == Node.ELEMENT_NODE) {
                                                if (node2.getNodeName().equals("network")){
                                                    nicSetMap.put("network", getTrimmedChildText(node2));
                                                    
                                                } else if (node2.getNodeName().equals("flux")) {
                                                    nicSetMap.put("flux",getTrimmedChildText(node2));
                                                    
                                                } else if (node2.getNodeName().equals("ipAddress")){
                                                    nicSetMap.put("ip",getTrimmedChildText(node2));
                                                    
                                                } else if (node2.getNodeName().equals("ipAddrMask")){
                                                    nicSetMap.put("netmask",getTrimmedChildText(node2));
                                                    
                                                } else if (node2.getNodeName().equals("gateways")){
                                                    nicSetMap.put("gateways",getTrimmedChildText(node2));
                                                } else if (node2.getNodeName().equals("nicOperType")){
                                                    nicSetMap.put("nicOperType",getTrimmedChildText(node2));
                                                } else if (node2.getNodeName().equals("mac")){
                                                    nicSetMap.put("mac",getTrimmedChildText(node2));
                                                }
                                                
                                            }
                                        }
                                        nicSet.add(nicSetMap);
                                    }
                                }
                            }
                        } else if (node0.getNodeName().equals("diskSet")){
                            NodeList nodes1 = node0.getChildNodes();
                            for (int i1 = 0; i1 < nodes1.getLength(); i1++) {
                                Node node1 = nodes1.item(i1);
                                if (node1.getNodeType() == Node.ELEMENT_NODE) {
                                    if (node1.getNodeName().equals("item")){
                                        NodeList nodes2 = node1.getChildNodes();
                                        HashMap<String,String> diskSetMap = new HashMap<String,String>();
                                        for (int i2 = 0; i2 < nodes2.getLength(); i2++) {
                                            Node node2 = nodes2.item(i2);
                                            if (node2.getNodeType() == Node.ELEMENT_NODE) {
                                                if (node2.getNodeName().equals("device")){
                                                    diskSetMap.put("device", getTrimmedChildText(node2));
                                                      
                                                } else if (node2.getNodeName().equals("size")) {
                                                    diskSetMap.put("size", getTrimmedChildText(node2));
                                                    
                                                } else if (node2.getNodeName().equals("volOperType")) {
                                                    diskSetMap.put("volOperType", getTrimmedChildText(node2));
                                                    
                                                } else if (node2.getNodeName().equals("volumeID")) {
                                                    diskSetMap.put("volumeID", getTrimmedChildText(node2));
                                                    
                                                }
                                            }
                                        }
                                        diskSet.add(diskSetMap);
                                    }
                                }
                            }
                        }
                    }
                }
            }
		return 0;
	}

	protected int checkParameters() throws Exception {

        if (IsNull(instanceID)){
            throw new Exception("Parameter error: instanceID can't be empty.");
        }

        if (IsNull(vmhostName)){
            throw new Exception( "Parameter error: vmhostName can't be empty.");
        }
        if (IsNull(cpuNum)){
            throw new Exception( "Parameter error: cpuNum can't be empty.");
        }
        if (IsNull(frequency )){
            throw new Exception( "Parameter error: frequency can't be empty.");
        }
        if (IsNull(minMem)){
            throw new Exception( "Parameter error: minMem can't be empty.");
        }
        if (IsNull(maxMem)){
            throw new Exception( "Parameter error: maxMem can't be empty.");
        }

        GridObjectInfo vmnode = getGridObject(TYPE_RESOURCE,instanceID);

        if (vmnode == null){
            throw new Exception("VM '"+instanceID+"' doesn't exist.");
        }
		if (! vmnode.getEffectiveFact("resource.type").getStringValue().equalsIgnoreCase(TYPE_VM_INSTANCE)){
			throw new Exception("Resource: "
					+ instanceID + " is not a vm instance");
		}

        Fact f = vmnode.getEffectiveFact("resource.vm.underconstruction");
		if (f != null && f.getBooleanValue()){
			throw new Exception("Resource: "
					+ instanceID + " is under construction");
		}
			
        ////// Check cpuNum //////
        long cpuNumL = Long.parseLong(cpuNum);
        if ((cpuNumL < 1)||(cpuNumL > 8)){
            throw new Exception( "Parameter error: value of 'cpuNum' must be a integer between 1 and 8.");
        }

        ////// Check frequency //////
        long frequencyL = Long.parseLong(frequency);
        if ((frequencyL < 512)||(frequencyL > 3072)){
            throw new Exception(  "Parameter error: value of 'frequency' must be a integer between 512 and 3072.");
        }

        ////// Check minMem and maxMem //////
        long minMemL = Long.parseLong(minMem);
        long maxMemL = Long.parseLong(maxMem);
        if (minMemL < 128){
            throw new Exception(  "Parameter error: value of 'minMem' must be greater than 128.");
        }
        if (minMemL > maxMemL){
            throw new Exception("Parameter error: value of 'maxMem' must be greater than minMem.");
        }
        
        ////// check nicSet //////
        if (nicSet.size() > 8){
            throw new Exception( "Parameter error: the max number of nics is limited to 8.");
        }
        
        ////// check diskSet //////
        if(!diskSet.isEmpty()){
            String[] opTypes = new String[]{"add","modify","delete"};
            List<String> opTypeList = Arrays.asList(opTypes);
            for(Map<String,String> disk:diskSet){
                String adevice = (String)getValueByKey(disk,"device");
                String asize = (String)getValueByKey(disk,"size");
                String avolOperType = (String)getValueByKey(disk,"volOperType");
                String avolumeID = (String)getValueByKey(disk,"volumeID");
                if(IsNull(avolOperType)||IsNull(adevice)||IsNull(asize)){
                    throw new Exception("Parameter error: null required field [volOperType|device|size] of diskSet.");
                }
                if(!opTypeList.contains(avolOperType)){
                    throw new Exception("Parameter error: value of 'diskSet->volOperType' must be 'add', 'modify' or 'delete'.");
                }
                if(!"add".equalsIgnoreCase(avolOperType) && IsNull(avolumeID)){
                    throw new Exception("Parameter error: value of 'diskSet->volumeID' can not be empty when in 'add' or 'delete' operation.");
                }
                Long iSize = Long.parseLong(asize);
                if("add".equalsIgnoreCase(avolOperType)){
                    if( iSize<1 || iSize >1024 ){
                        throw new Exception("Parameter error: value of 'diskSet->size' must between 1 to 1024.");
                    }
                }else{
                    Map<String,Long> vdiskSet = getVdiskMap(vmnode);
                    if(!vdiskSet.containsKey(avolumeID)){
                        throw new Exception("Can not found disk '" + avolumeID + "' on VM '" + instanceID + "'.");
                    }
                }
            }
        }

		return 0;
	}

    private Map<String, Long> getVdiskMap(GridObjectInfo node) throws Exception{
        HashMap<String,Long> diskMap = new HashMap<String, Long>();        
        String[] vmDisks = node.getEffectiveFact("resource.vm.vdisks").getStringArrayValue();

        for(String vmDisk : vmDisks){
            GridObjectInfo vdisk = getGridObject(TYPE_VDISK,vmDisk);
            String volumeID = vdisk.getEffectiveFact("vdisk.location").getStringValue();
            if(IsNotNull(volumeID)){
                diskMap.put(volumeID, (Long)vdisk.getEffectiveFact("vdisk.size").getLongValue());
            }
        }
        return diskMap;
    }

	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

        HashMap<String,Object> params = new HashMap<String,Object>();
        params.put("vmID", instanceID);
        params.put("vmhostName", vmhostName);
        params.put("cpuNum", cpuNum);
        params.put("maxMem", maxMem);
        params.put("minMem", minMem);
        params.put("nicSet", nicSet);
        params.put("diskSet", diskSet);
        params.put("frequency", frequency);
        params.put("saveData", "true");
        
        GridObjectInfo vmnode = getGridObject(TYPE_RESOURCE,instanceID);
        String jobName = "vmmodify";
        try{
        	if(getProvisionerJob(vmnode).equalsIgnoreCase("vsphere"))
        		jobName = "vmmodify_vs";
        }catch(FactException e){
        	return throwError(errNo(3), "Get fact error. "+e.getMessage());
        }

		JobSpec job = this.getJob(jobName);
        if (job == null){
            return throwError(errNo(7),"Job '" + jobName +"' is not deployed.");
        }
        
		JobDef jobDef = new JobDef();
		jobDef.setName(jobName);
		jobDef.setJob(job);
		jobDef.resetImports();
        jobDef.setImport("jobargs.params", params);
		jobDef.setWhatComplete(true);
		jobDef.setWaitForCompletion(false);

		RunJob runJob = new RunJob(getClient());
		try {
			if (!runJob.run(jobDef)) {
				return throwError(errNo(6), "Error running job '" + jobName
						+ "'. \nJob Log:\n" + getJobLog(runJob.getJobId()));
			}
		} catch (Exception ex) {
			return throwError(errNo(7), ex.getMessage());
		}

        String jobID = runJob.getJobId();

        StringBuffer sb = new StringBuffer(XML_HEADER);
        sb.append("<results>\n");
        sb.append("    <result>"+ 0 +"</result>\n");          // result
        sb.append("    <jobID>" + jobID + "</jobID>\n");
        sb.append("</results>\n");

        return sb.toString();
	}
}
