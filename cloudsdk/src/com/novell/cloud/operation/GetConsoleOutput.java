/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.operation;

import java.util.ArrayList;
import java.util.HashMap;

import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.io.IOException;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.novell.cloud.api.JobDef;
import com.novell.cloud.api.JobSpec;
import com.novell.cloud.api.Op;
import com.novell.cloud.api.Instance;
import com.novell.cloud.api.XMLHelper;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Fact;
import com.novell.zos.grid.FactException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.GridObjectNotFoundException;

public class GetConsoleOutput extends Instance {

    private String vmhostName = null;

	public GetConsoleOutput() {
		super("GetConsoleOutput", "Get the console log of the vm instance", 38);
	}

	public GetConsoleOutput(ClientAgent client) {
		super(client,"GetConsoleOutput", "Get the console log of the vm instance", 38);
	}

	public String execute(String xmlData) throws GridException {

		try {
			getParameters(xmlData);
		} catch (Exception ex) {
			return throwError(errNo(1), ex.getMessage());
		}

		try {
			checkParameters();
		} catch (Exception ex) {
			return throwError(errNo(2), ex.getMessage());
		}

		String jobName = "vmconsole";
		JobSpec job = this.getJob(jobName);
        if (job == null){
            return throwError(errNo(4),"Job '" +jobName + "' is not deployed.");
        }

		JobDef jobDef = new JobDef();
		jobDef.setName(jobName);
		jobDef.setJob(job);
		jobDef.resetImports();
        jobDef.setImport("jobargs.vmID", instanceID);
		jobDef.setWhatComplete(true);
		jobDef.setWaitForCompletion(true);

		RunJob runJob = new RunJob(getClient());
		try {
			if (!runJob.run(jobDef)) {
				return throwError(errNo(3), "Error running job '" + jobName
						+ "'. \nJob Log:\n" + getJobLog(runJob.getJobId()));
			}
		} catch (Exception ex) {
			return throwError(errNo(4), ex.getMessage());
		}

        String jobID = runJob.getJobId();
		
        String logfile = null;
        Fact f =  getGridObject(TYPE_MATRIX,null).getEffectiveFact("matrix.datagrid.root");
        if (f != null){
            try {
               logfile = f.getValue() + "/dataGrid/files/iwm/tmp/" +instanceID + "-console.log";
            } catch (FactException ex){
            }
        }

        File fn = new File(logfile);
        if (!fn.exists()){
           return throwError(errNo(5), "Can't open console-log file of instance '"+instanceID+"'.");
        }

        long   time=fn.lastModified();
        Calendar   cal=Calendar.getInstance();
        cal.setTimeInMillis(time);

        StringBuffer sb = new StringBuffer(XML_HEADER);
		try {
            sb.append("<results>\n");
            sb.append("    <result>"+ 0 +"</result>\n");
            SimpleDateFormat sdf =new SimpleDateFormat( "yyyy-MM-dd'T'hh:mm:ssZ");
            sb.append("    <timestamp>" + sdf.format(cal.getTime()) + "</timestamp>\n");
            sb.append("    <output><![CDATA[");

            InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream (logfile), "UTF-8" );
            BufferedReader reader = new BufferedReader (inputStreamReader);
            String tempString = null;
	        while ((tempString = reader.readLine()) != null) {
                sb.append(tempString);
                sb.append("\n");
	        }
            reader.close();

            sb.append("]]></output>\n");
            sb.append("</results>\n");
        } catch (UnsupportedEncodingException e) {
            System.err.println(e.getMessage());
         } catch (IOException e) {
            System.err.println(e.getMessage());
          	return throwError(errNo(11), "Read file error!");
        }

        return sb.toString();
	}
}
