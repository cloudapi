/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.api;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import com.novell.zos.engine.BasePriority;

public class JobDef {
	/**
     * Date format for dates entered in UI. Must match javascript format.
     * E.g. "Oct 28, 2004 [03:32 AM]"
     */
    private static final String DATE_FORMAT = "MMM dd, yyyy [hh:mm aa]";

    /** Name of deployed job */
    private JobSpec jobSpec;

    private boolean whatComplete = false;
    private boolean whereComplete = true;
    private boolean whenComplete = true;
    private boolean waitForCompletion = false;

    /** Locked and validated import parameters */
    private HashMap imports = new HashMap();

    /** Symbolic name of job as selected in ui */
    private String name;

    /** Policy fragement representing the resource constraints */
    private String wherePolicy;

    /** List of selected targets is policy is a result of explicit selection */
    private String[] whereTargets;

    /** Policy fragement representing the start and termination constraints */
    private String whenPolicy;
    
    private String startDate = "As Soon As Possible";
    private String endDate = "Job Duration";
    private String priority;
    
    private String policyName = null;

    public JobDef() {
    }

    /**
     * @return the job name which might be the library alias name
     */
    public void setJob(JobSpec job) {
        this.jobSpec = job;
    }

    /**
     * @return the job name which might be the library alias name
     */
    public JobSpec getJob() {
        return jobSpec;
    }

    /**
     * @return the job name which might be the library alias name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the job name which might be the library alias name
     */
    public String getName() {
        return name;
    }
   
    /**
     * @return the underlying job name
     */
    public String getJobName() {
        if (jobSpec != null) {
            return jobSpec.getName();
        } else {
            return "";
        }
    }
    
    public String getDisplayName() {
        if (jobSpec != null) {
            return jobSpec.getDisplayName();
        }
        
        return getName();
    }

    /**
     * @return the current value of the name import parameter
     */
    public Object getImportValue(String name) {
        return imports.get(name);
    }

    public void setImport(String name, Object value) {
        imports.put(name, value);
    }

    public void resetImports() {
        imports.clear();
    }

    /**
     * @param wherePolicy the policy fragment representing the where constraints
     */
    public void setWherePolicy(String wherePolicy) {
        this.wherePolicy = wherePolicy;
        this.whereTargets = null; // Assume no specific targets are specified
    }

    /**
     * @param slectedTargets the list of selected targets. Stored for summary
     *                       display purposes.
     */
    public void setWhereTargets(String[] selectedTargets) {
        this.whereTargets = selectedTargets;
    }

    /**
     * @return the policy fragment representing the where constraints
     */
    public String getWherePolicy() {
        return wherePolicy;
    }
    
    /**
     * @param whenPolicy the policy fragment representing misc when constraints
     */
    public void setWhenPolicy(String whenPolicy) {
        this.whenPolicy = whenPolicy;
    }

    /**
     * @return the policy fragment representing the where constraints
     */
    public String getWhenPolicy() {
        return whenPolicy;
    }

    /**
     * @return the complete policy for this job instance
     */
    public String getPolicy() {
        String endConstraint = getEndConstraint();

        if (whenPolicy == null && wherePolicy == null
                               && endConstraint == null) {
            return null;
        }

        return "<policy>\n"
               +  ((whenPolicy == null)?"" : whenPolicy)
               + ((wherePolicy == null)?"" : "<constraint type=\"resource\"> \n" + wherePolicy + "\n</constraint>")
               + ((endConstraint == null)?"" : endConstraint)
               + "</policy>\n";
    }

    public String getWhatSummary() {
        if (whatComplete) {
            if (getName().equals(getJobName())) {
                return getName();
            } else {
                return getName() + "(Job: " + getJobName() + ")";
            }
        } else {
            return "unknown";
        }
    }

    public String getWhatDescription() {
        if (whatComplete) {
            if (!imports.isEmpty() && whatComplete) {
                StringBuffer s = new StringBuffer();
                s.append("Specified Parameters: ");
                Iterator i = imports.keySet().iterator();
                while (i.hasNext()) {
                    String key = (String)i.next();
                    s.append(key);
                    if (i.hasNext()) s.append(", ");
                }
                if (s.length() < 75) {
                    return s.toString();
                } else {
                    return s.substring(0,75) + "...";
                }
            } else {
               return "(default parameters)";
            }
        } else {
           return "";
        }
    }

    public String getWhereSummary() {
        if (whereComplete) {
            if (whereTargets != null) {
                return "Explicit Selection";
            } else {
                if (wherePolicy == null || wherePolicy.equals("")) {
                    return "Any Available";
                } else {
                    return "Constrained By";
                }
            }
        } else {
            return "unknown";
        }
    }

    public String getWhereDescription() {
        String desc = "";

        if (whereComplete) {
            if (whereTargets != null) {
                String x = java.util.Arrays.asList(whereTargets).toString();
                desc = x.substring(1, x.length() - 1); // Strip [ ]
            } else {
                if (wherePolicy != null) {
                    desc = wherePolicy;
                }
            }
        }
        if (desc.length() < 75) {
            return desc;
        } else {
            return desc.substring(0,75) + "...";
        }
    }

    public String getWhenFrom() {
        if (whenComplete && startDate != null) {
            return startDate;
        } else {
            return "unknown";
        }
    }

    public String getWhenUntil() {
        if (whenComplete && endDate != null) {
            return endDate;
        } else {
            return "unknown";
        }
    }

    /** Return the start time suitable for runJob client API call */
    public long getStartTime() {
        // String may be "As Soon As Possible"
        if (startDate != null && startDate.indexOf("ossible") == -1
                              && !startDate.equals("")) {
            try {
                DateFormat df = new SimpleDateFormat(DATE_FORMAT);
                Date date = (Date)df.parse(startDate);
                return date.getTime();
            } catch (ParseException pe) {
                // TODO -- better error handling
                System.err.println("Date parse exception. Format probably out of sync with Javascript UI");
                System.err.println("Start date string:" + startDate);
            }
        }
        return 0; // Immediately
    }

    /** Return the end time suitable for use as "termination" constraint */
    public String getEndConstraint() {
        // String may be "Job Duration"
        if (endDate != null && endDate.indexOf("uration") == -1
                            && !endDate.equals("")) {
            try {
                DateFormat df = new SimpleDateFormat(DATE_FORMAT);
                Date date = (Date)df.parse(endDate);
                String javaDate = DateFormat.getInstance().format(date);
                //long millis = date.getTime();
                StringBuffer endConstraint = new StringBuffer();
                endConstraint.append("<constraint type=\"continue\">\n");
                endConstraint.append("<lt fact=\"matrix.time\" value=\""
                                     + javaDate + "\" />\n");
                endConstraint.append("</constraint>\n");
                return endConstraint.toString();
            } catch (ParseException pe) {
                // TODO -- better error handling
                System.err.println("Date parse exception. Format probably out of sync with Javascript UI");
                System.err.println("End date string:" + endDate);
            }
        }
        return null; // No end constraint
    }

    public String getWhenPriority() {
        if (whenComplete && priority != null) {
            return priority;
        } else {
            return "default";
        }
    }
    public int getWhenPriorityValue() {
        return BasePriority.getValue(getWhenPriority());
    }
    
    public void setWhenFrom(String startDate) {
        this.startDate = startDate;
    }

    public void setWhenUntil(String endDate) {
        this.endDate = endDate;
    }
    
    public void setPriority(String priority) {
        this.priority = priority;
    }
    
    /**
     * The URL to launch the application specific portal extension or
     * null if not defined.
     */
    public String getPortalExtUrl() {
        return jobSpec.getPortalExtUrl();
    }

    /**
     * Whether the job specification is complete.  I.e. ready to submit.
     */
    public boolean isComplete() {
        return isWhatComplete() && isWhereComplete() && isWhenComplete();
    }

    /**
     * Specify that the "what" parameters are complete or not.
     */
    public void setWhatComplete(boolean state) {
        whatComplete = state;
    }

    /**
     * @return true is the runjob "what" specification is complete and valid
     */
    public boolean isWhatComplete() {
        return whatComplete;
    }

    /**
     * Specify that the "where" parameters are complete or not.
     */
    public void setWhereComplete(boolean state) {
        whereComplete = state;
    }

    /**
     * @return true is the runjob "where" specification is complete and valid
     */
    public boolean isWhereComplete() {
        return whereComplete;
    }

    /**
     * Specify that the "where" parameters are complete or not.
     */
    public void setWhenComplete(boolean state) {
        whenComplete = state;
    }

    /**
     * @return true is the runjob "when" specification is complete and valid
     */
    public boolean isWhenComplete() {
        return whenComplete;
    }
    
    public boolean getWaitForCompletion(){
    	return waitForCompletion;
    }
    
    public void setWaitForCompletion(boolean state){
    	waitForCompletion = state;
    }
    
    public String getPolicyName(){
    	return policyName;
    }
    
    public void setPolicyName(String value){
    	policyName = value;
    }

}
