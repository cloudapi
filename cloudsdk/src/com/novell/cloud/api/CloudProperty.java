/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */
package com.novell.cloud.api;

import java.util.Properties;

public class CloudProperty {
	public final static Properties props = new Properties();

	private static String version = "20110525-V0.1";

	public static String getVersion() {
		return version;
	} 
	
	public static boolean getDebugArgument(){
		return false;
	}
	
	public static boolean getVerboseArgument(){
		return false;
	}
	
	public static String getServerAddress(){
		String srvAddress = "127.0.0.1";
		return srvAddress;
	}
	
	public static int getServerPort(){
		int port = 8100;
		return port;
	}
	
	public static String getDefaultUser(){
		String username = "root";
		return username;
	}
	
	public static String getDefaultPassword(){
		String password = "novell";
		return password;
	}

}
