/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.api;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.SocketAddress;

import org.apache.commons.codec.binary.Base64;

import com.novell.zos.tls.PemCertificate;
import com.novell.zos.tls.TlsCallbacks;
import com.novell.zos.tls.TlsConfiguration;

import com.novell.cloud.operation.*;

public class ServiceInstance {

	public static final String COMMAND = "ServiceInstance";
	private static TlsConfiguration tlsConfig;

	private static Op operations[] = { 
		new Login(),
		new Logout(),
		new ShakeHand(),
		new RegisterPhysicalHost(),
		new DeregisterPhysicalHost(),
		new DescribePhysicalHost(),
		new GetPhysicalHostState(),
		new CreateRepository(),
		new DeleteRepository(),
		new AddPhyHostToRepository(),
		new DeletePhyHostFromRepository(),
		new DescribePhyHostOfRepository(),
		new AddVGToRepository(),
		new DeleteVGFromRepository(),
		new DescribeRepository(),
		new ModifyRepository(),
		new CreateVMTemplate(),
		new RegisterVMTemplate(),
		new DeregisterVMTemplate(),
		new DescribeVMTemplate(),
		new DeleteVMTemplate(),
		new CreateInstance(),
		new DeleteInstance(),
		new DescribeInstances(),
		new GetInstanceListOfHost(),
		new RunInstance(),
		new TerminateInstance(),
		new RebootInstance(),
		new SuspendInstance(),
		new ResumeInstance(),
		new CloneInstance(),
		new ModifyInstance(),
		new MigrateInstance(),
		new GetInstanceState(),
		new GetConsoleOutput(),
		new GetInstanceMonitor(),
		new SetInstanceBootOption(),
		new MotionInstanceStorage(),
		new SetInstancePrior(),
		new CreateSnapshot(),
		new DeleteSnapshot(),
		new DescribeSnapshots(),
		new RestoreSnapshot(),
		new GetWarnLogCount(),
		new SearchWarnLog(),
		new VMConsole(),
		new SetInstanceVNCPassword(),
		new GetJobState(),
		new CancelJob(),
		new AddVirtDeviceToHost(),
		new DeleteVirtDeviceFromHost(),
		new DescribeVirtDeviceOfHost(),
		new AttachVolume(),
		new CreateVolumeSnapshot(),
		new DeleteVolumeSnapshot(),
		new DescribeVolumeSnapshots(),
		new CreateVolume(),
		new DeleteVolume(),
		new DescribeVolumes(),
		new DetachVolume(),
		new RunJob()};

	public static Op getOperation(String name) {
		for (int i = 0; i < operations.length; i++) {
			if (operations[i].getName().equalsIgnoreCase(name))
				return operations[i];
		}
		return null;
	}

	/**
	 * Given a list of arguments, find the operation buried among them.
	 * 
	 * @param args
	 *            the String array of arguments to search
	 * @return the String name of the operation named in the args
	 */
	private static String getOpName(String args[]) {
		if (args != null) {
			for (int i = 0; i < args.length; i++) {
				if (args[i].startsWith("-"))
					continue;
				return args[i].trim();
			}
		}

		// Default to help
		return "help";
	}

	/**
	 * Print text.
	 */
	public final static void println(String string) {
		System.out.println(string);
	}

	/**
	 * Print text.
	 */
	public final static void print(String string) {
		System.out.print(string);
	}

	/**
	 * Print an error
	 */
	public final static void errorPrintln(String string) {
		System.err.println("ERROR: " + string);
	}

	public final static TlsConfiguration getTlsConfig() {
		if (tlsConfig == null) {
			String certPath = System.getProperty("user.home");
			if ((certPath == null) || (certPath.length() < 1)) {
				ServiceInstance.errorPrintln("Can't determine home directory");
				System.exit(1);
			}
			certPath += File.separator;
			certPath += ".novell";
			certPath += File.separator;
			certPath += "cloud";
			certPath += File.separator;
			certPath += "api";
			certPath += File.separator;
			certPath += "tls";
			tlsConfig = new TlsConfiguration();
			tlsConfig.setTlsAware(true); // Using internal TLS API.
			tlsConfig.setCertificatePath(certPath);
			tlsConfig.setAgentMode(false); // Not running as an agent.
			tlsConfig.setCallbacks(new TlsCallbacks() {
				public boolean onCertificateNotFound(SocketAddress peer,
						PemCertificate peerCert) {
					return true;
				}

				public boolean onCertificateMismatch(SocketAddress peer,
						PemCertificate peerCert, PemCertificate refCert) {
					return true;
				}
			});
		}
		return tlsConfig;
	}

	public static void help(String msg) {
		if (msg == null) {
			// print general help
			println("Usage: "
					+ ServiceInstance.COMMAND
					+ " [standard-options] command [command-options-and-arguments]");
		} else {
			println(msg);
		}
	}
	
	public static String BASE64UTF8(String paramString)
    {
        if (paramString == null) return null;
  
        paramString = paramString.trim();
        String str = new String(Base64.decodeBase64(paramString.getBytes()));
  
        return str;
    }

    public static String UTF8BASE64(String paramString)
    {
        if (paramString == null) return null;
        paramString = paramString.trim();
        return  Base64.encodeBase64String(paramString.getBytes());
    }

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Op operation = null;
		// String opName = getOpName(args);
		// if (opName == null || opName.equalsIgnoreCase("help")) {
		// help(null);
		// }
		//
		// operation = getOperation(opName);
		// if (operation == null) {
		// help("Unknown command: '" + opName + "'");
		// }

		if (args.length != 2) {
			System.err.println("ServiceInstance operation xmlfile");
		}

		String opName = args[0];
		String filename = args[1];
		Op operation = getOperation(opName);
		if (operation == null) {
			System.err.println("not support operation: " + opName);
			System.exit(1);
		}
		
		String xmlData = "";
		try {
			BufferedReader in = new BufferedReader(new FileReader(filename));
			String str;
			while ((str = in.readLine()) != null) {
				xmlData = xmlData + str;
			}
			in.close();
			if(operation.getName().equalsIgnoreCase("login")){
				//use cache Credential
				Login login = (Login)operation;
				login.setCacheCredential(true);
			}
			operation.start();
			String result = operation.execute(xmlData);
			operation.finish();
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		System.exit(0);
	}
}
