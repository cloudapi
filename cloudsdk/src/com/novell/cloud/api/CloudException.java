/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */
package com.novell.cloud.api;

public class CloudException extends Exception{
	
	 public int errno = 0;
	  
	  public CloudException()
	  {
	    super("");
	  }

	  public CloudException(String message)
	  {
	    super(message);
	  }
	  
	  public CloudException(int error_no, String message)
	  {
	    super(message);
	    errno = error_no;
	  }

	  public CloudException(Throwable cause)
	  {
	    super(cause);
	  }

	  public CloudException(String message, Throwable cause)
	  {
	    super(message, cause);
	  }
	  
	  public int getErrno()
	  {
	    return this.errno;
	  }

}
