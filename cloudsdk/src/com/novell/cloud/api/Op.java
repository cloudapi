/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.api;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import com.novell.zos.engine.BaseID;
import com.novell.zos.grid.ClientAgent;
import com.novell.zos.grid.Credential;
import com.novell.zos.grid.GridConfigurationException;
import com.novell.zos.grid.GridException;
import com.novell.zos.grid.GridObjectInfo;
import com.novell.zos.grid.ID;
import com.novell.zos.grid.Job;
import com.novell.zos.grid.JobInfo;
import com.novell.zos.grid.WorkflowInfo;
import com.novell.zos.grid.Fact;
import com.novell.zos.toolkit.ClientAgentImpl;
import com.novell.zos.grid.FactException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class Op {
	protected static final String XML_HEADER = "\n<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
	protected static final String DATA_ID = "data";
	protected static String ENCODING = "UTF-8";
	protected static String TYPE_RESOURCE = "resource";
	protected static String TYPE_VMHOST = "vmhost";
	protected static String TYPE_REPOSITORY = "repository";
	protected static String TYPE_VDISK = "vdisk";
	protected static String TYPE_VNIC = "vnic";
	protected static String TYPE_MATRIX = "matrix";
	protected static String TYPE_JOB = "job";
	protected static String TYPE_JOBINSTANCE = "jobinstance";
	protected static String TYPE_USER = "user";
	protected static String TYPE_FIXED_PHYSICAL = "Fixed Physical";
	protected static String TYPE_VM_INSTANCE = "VM";
	protected static String TYPE_VM_TEMPLATE = "VM Template";
	protected static final String INDENT = "    ";
	protected static final String NL = "\n";
	protected String session = null;

	private final Format DATE_TIME_FORMAT = new SimpleDateFormat(
			"MM/dd/yyyy hh:mm:ss");

	private final Format ELAPSED_TIME_FORMAT = new SimpleDateFormat("mm:ss");

	private String name;

	private String summary;

	private int module_id;

	/**
	 * Current client agent.
	 */
	private ClientAgent client;

	/**
	 * Last good credentials.
	 */
	private Credential credential;

	protected Boolean subSessionFlag = null;

	private Op() {
		// you must use the constructor below.
	}

	private Op(ClientAgent client) {
		this.client = client;
	}

	/**
	 * Constructor
	 * 
	 * @param name
	 *            The name of command to use on command line
	 * @param s
	 *            The String summary of what this operation does.
	 */
	public Op(String name, String summary, int moduleid) {
		this.name = name;
		this.summary = summary;
		this.module_id = moduleid;
	}

	/**
	 * Constructor
	 * 
	 * @param name
	 *            The name of command to use on command line
	 * @param s
	 *            The String summary of what this operation does.
	 */
	public Op(ClientAgent client, String name, String summary, int moduleid) {
		this.client = client;
		this.name = name;
		this.summary = summary;
		this.module_id = moduleid;
	}

	/**
	 * Get the name that this operation is refered by.
	 * 
	 * @return the String name of this operation.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the String summary of what this operation does
	 */
	public String getSummary() {
		return summary;
	}

	public int getModuleId() {
		return module_id;
	}

	public void setModuleId(int id) {
		module_id = id;
	}

	protected int errNo(int errorID) {
		return -(getModuleId() * 1000 + errorID);
	}

	protected String throwError(int errno, String errMsg) {
		// Document doc = XMLHelper.createDomDocument();
		// Element root = doc.getDocumentElement();
		//
		// Element result = doc.createElement("result");
		// result.appendChild(doc.createTextNode(String.valueOf(errno)));
		// root.appendChild(result);
		//
		// Element resultInfo = doc.createElement("resultInfo");
		// resultInfo.appendChild(doc.createTextNode(errMsg));
		// root.appendChild(resultInfo);
		//
		// return XMLHelper.writeXmlString(doc);

		StringBuffer sb = new StringBuffer(XML_HEADER);
		sb.append("<results>\n");
		sb.append("    <result>" + errno + "</result>\n");
		if (errno != 0 && errMsg != null) {
			sb.append("    <resultInfo>" + HtmlEncoder.encode(errMsg)
					+ "</resultInfo>\n");
		}
		sb.append("</results>\n");
		return sb.toString();
	}

	/**
	 * Retrieve client instance using last persisted connection/credentials.
	 */
	public ClientAgent getClient() {
		return getClient(false);
	}

	/**
	 * Retrieve client instance using last persisted connection/credentials.
	 */
	protected ClientAgent getClient(boolean inlogin) {
		if (client == null) {
			boolean debug = false;
			if (CloudProperty.getDebugArgument()) {
				debug = true;
			}
			boolean verbose = false;
			if (CloudProperty.getVerboseArgument()) {
				verbose = true;
			}
			try {
				client = new ClientAgentImpl(verbose, debug);
				client.setTlsConfiguration(ServiceInstance.getTlsConfig());
			} catch (GridException ge) {
				String msg = ge.getMessage();
				if ((msg != null)
						&& (msg.toLowerCase().indexOf("cached credentials") >= 0)) {
					if (inlogin) {
						System.setProperty("com.novell.zos.client.login.error",
								"Not logged in");
					} else {
						System.setProperty("com.novell.zos.client.login.error",
								"Cannot complete requested operation: Not logged in");
					}
				}
				throw new RuntimeException(ge.getMessage());
			}
		}
		return client;
	}

	/**
	 * Create a Client instance using server and port arguments. This will
	 * ignore the previously stored login record.
	 */
	protected ClientAgent getClient(String server, int port)
			throws GridConfigurationException {
		boolean debug = false;
		if (CloudProperty.getDebugArgument()) {
			debug = true;
		}
		boolean verbose = false;
		if (CloudProperty.getVerboseArgument()) {
			verbose = true;
		}
		if (port <= 0) {
			// System default port
			client = new ClientAgentImpl(server, verbose, debug);
			client.setTlsConfiguration(ServiceInstance.getTlsConfig());
		} else {
			client = new ClientAgentImpl(server, port, verbose, debug);
			client.setTlsConfiguration(ServiceInstance.getTlsConfig());
		}

		return client;
	}

	public void setClient(ClientAgent client) {
		this.client = client;
	}

	public void start() throws GridException {
		// Note the returned credentials will be needed in all future
		// client API calls. This method will have to be overidden on
		// any commands that wish to change credentials / grid. E.g. 'login'
		if (subSessionFlag != null) {
			((ClientAgentImpl) getClient()).setSubSessionFlag(subSessionFlag);
		}
		getClient().login(); // Use cached login
	}

	/**
	 * Overridden by commands
	 */
	public String execute(String xmlData) throws GridException {
		return throwError(-1, "execute not implemented: " + getName() + ": "
				+ xmlData);
	}

	public void finish() throws GridException {
		if (client == null) {
			// nothing to do. we didn't successfully login in the first place.
			return;
		}
		// XXX iterate through clients
		getClient().logout();
		getClient().shutdown();
	}

	/**
	 * Overridden by commands
	 */
	protected int getParameters(String xmlData) throws Exception {
		// nothing to do
		return 0;
	}

	/**
	 * Overridden by commands
	 */
	protected int checkParameters() throws Exception {
		// nothing to do
		return 0;
	}

	public boolean IsNull(String str) {
		if (str == null)
			return true;
		if (str.equals(""))
			return true;
		return false;
	}

	public boolean IsNotNull(String str) {
		if (str == null)
			return false;
		if (str.equals(""))
			return false;
		return true;
	}

	/**
	 * Used to retrieve the JobSpec for a given named job.
	 * 
	 * @return details of the job with given name or null if job does not exist.
	 */
	public JobSpec getJob(String name) {
		JobSpec jobSpec = null;
		try {
			JobInfo jobInfo = getClient().getJobInfo(name);
			if (jobInfo != null) {
				jobSpec = new JobSpec(jobInfo);
			}
		} catch (GridException ex) {
			// This is ok, just return null
		}
		return jobSpec;
	}

	public String getJobLog(String jobInstanceId) {
		if (jobInstanceId == null) {
			return "Error job instance id: null";
		}
		String joblog = "Error retrieving log";
		try {
			WorkflowInfo workflow = getClient().getStatusDetail(jobInstanceId,
					false);
			joblog = catJobFile(workflow.getUsername(), jobInstanceId,
					"job.log");
		} catch (GridException e) {
			// TODO Auto-generated catch block
			System.err.println("getJobLog exception: " + e.getMessage());
		}
		return joblog;
	}

	// client api version.
	public String catJobFile(String username, String jobInstanceId,
			String fileName) {

		String path = "grid:///users/" + username + "/" + jobInstanceId + "/"
				+ fileName;
		return catDgFile(path);
	}

	// client api version.
	public void tailJobFile(String username, String jobInstanceId,
			String fileName, OutputStream os) {

		String path = "grid:///users/" + username + "/" + jobInstanceId + "/"
				+ fileName;
		tailDGFile(jobInstanceId, path, os);
	}

	public String catDgFile(String dgUrl) {

		boolean urlFilter = true;
		String catText = "Error retrieving file";

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		try {
			getClient().dataGridCancel();

			boolean exists = getClient().cat(dgUrl, baos);
			if (exists) {
				// Return the contents as a String
				catText = baos.toString();
			} else {
				// File may have been removed by datagrid cleanup
				catText = "File not available.";
			}
		} catch (GridException ge) {
			Throwable t = ge.getCause();
			if (t instanceof IOException) {
				// silently ignore
				System.err.println("catfile exception: " + t);
			} else {
				// unexpected. we need to see what this is.
				// ge.printStackTrace();
			}
		}
		return catText;
	}

	public void tailDGFile(String jobInstanceId, String dgUrl, OutputStream os) {

		Long arg_count = null;
		boolean flag_quiet = false;

		// to do tail -F -J
		boolean flag_retry = true;
		boolean flag_byName = true;
		boolean flag_countLines = true;
		boolean flag_offsetLines = true;
		Long arg_refresh = new Long(10000);
		Long arg_timeout = null;
		Long arg_offset = new Long(-100000);

		Long arg_interval = new Long(-1);
		Long arg_maxUnch = new Long(-1);

		try {
			getClient().dataGridCancel();

			getClient().follow(dgUrl, os, os, jobInstanceId, arg_maxUnch,
					arg_offset, arg_count, arg_interval, arg_refresh,
					arg_timeout, flag_offsetLines, flag_retry, flag_quiet,
					flag_byName, flag_countLines);

		} catch (GridException ge) {
			Throwable t = ge.getCause();
			if (t instanceof IOException) {
				// silently ignore
				System.err.println("catfile exception: " + t);
			} else {
				// unexpected. we need to see what this is.
				// ge.printStackTrace();
			}

		}
	}

	public static StringBuffer indent(int level, StringBuffer buf, String data) {
		return indent(level, buf, data, true);
	}

	public static StringBuffer indent(int level, StringBuffer buf, String data,
			boolean newLine) {
		for (int i = 0; i < level; i++)
			buf.append(INDENT);

		buf.append(data);

		if (newLine)
			buf.append(NL);

		return buf;
	}

	// return if the info is found, or on 300 seconds timeout, job failed,
	// completed, cancelled
	public GridObjectInfo[] GetInfoFromJob(String strCon, String jobID) {
		GridObjectInfo[] arrayOfNodeInfo = null;

		try {
			for (int ii = 0; ii < 3000; ii++) {
				arrayOfNodeInfo = getClient().getGridObjects(TYPE_RESOURCE,
						strCon, true);
				if (arrayOfNodeInfo != null && arrayOfNodeInfo.length != 0) {
					break; // Got the right nodeInfo
				}

				if (getClient().getStatus(jobID) >= WorkflowInfo.COMPLETED_STATE) {
					break; // job completed, cancelled or failed.
				}
				try {
					Thread.currentThread();
					Thread.sleep(300L);
				} catch (InterruptedException e) {
				}
			}
		} catch (Exception ex) {
			arrayOfNodeInfo = null;
		}
		return arrayOfNodeInfo;
	}

	protected boolean isStringAinB(String a, String b) {
		if (a.contains(b))
			return true;
		else
			return false;
	}

	public String getTrimmedChildText(Node node) {
		String txt = "";
		NodeList children = node.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Node child = children.item(i);
			if (child.getNodeType() == Node.TEXT_NODE) {
				txt += child.getNodeValue();
			}
		}
		return txt.trim();
	}

	public Object getValueByKey(Map m, String key) {
		return m.containsKey(key) ? m.get(key) : null;
	}

	public GridObjectInfo getVmHostFromHost(GridObjectInfo host)
			throws Exception {
		GridObjectInfo vmhost = null;
		Fact vmhostsFact = host.getEffectiveFact("resource.vmhosts");
		if (null == vmhostsFact) {
			return vmhost;
		}
		String[] vmhosts = null;
		String vmHostName = null;
		try {
			vmhosts = vmhostsFact.getStringArrayValue();
			vmHostName = vmhosts[0];
		} catch (Exception e) {
			return vmhost;
		}
		vmhost = getClient().getGridObjectInfo(TYPE_VMHOST, vmHostName);
		return vmhost;
	}

	public GridObjectInfo getGridObject(String type, String gridObjectName) {
		GridObjectInfo object = null;
		try {
			object = getClient().getGridObjectInfo(type, gridObjectName);
		} catch (Exception ex) {
		}
		return object;
	}

	static public String getProvisionerJob(GridObjectInfo vmnode)
			throws FactException {
		Fact f = vmnode.getEffectiveFact("resource.provisioner.job");
		if (null != f) {
			return f.getStringValue();
		}
		return null;

	}

	/**
	 * Format a time in millis into a standard date time format.
	 */
	protected String dateTimeFormat(long millis) {
		if (millis <= 0) {
			return "n/a";
		} else {
			Date date = new Date(millis);
			return DATE_TIME_FORMAT.format(date);
		}
	}

	/**
	 * Format a time in millis into a standard elapsed time format.
	 */
	protected String elapsedTimeFormat(long millis) {
		StringBuffer buff = new StringBuffer();
		long hours = millis / (60 * 60 * 1000);
		buff.append("" + hours);
		buff.append(":");
		buff.append(ELAPSED_TIME_FORMAT.format(new Date(millis)));
		return buff.toString();
	}

}
