/*-----------------------------------------------------------------------------
 * Copyright (C) 2011 Unpublished Work of Novell, Inc. All Rights Reserved.
 *
 * THIS IS AN UNPUBLISHED WORK OF NOVELL, INC.  IT CONTAINS NOVELL'S
 * CONFIDENTIAL, PROPRIETARY, AND TRADE SECRET INFORMATION.  NOVELL RESTRICTS
 * THIS WORK TO NOVELL EMPLOYEES WHO NEED THE WORK TO PERFORM THEIR ASSIGNMENTS
 * AND TO THIRD PARTIES AUTHORIZED BY NOVELL IN WRITING.  THIS WORK MAY NOT
 * BE USED, COPIED, DISTRIBUTED, DISCLOSED, ADAPTED, PERFORMED, DISPLAYED,
 * COLLECTED, COMPILED, OR LINKED WITHOUT NOVELL'S PRIOR WRITTEN CONSENT.
 * USE OR EXPLOITATION OF THIS WORK WITHOUT AUTHORIZATION COULD SUBJECT THE
 * PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
 *-----------------------------------------------------------------------------
 * $Id: pflin@novell.com
 *-----------------------------------------------------------------------------
 */

package com.novell.cloud.api;

import com.novell.zos.grid.FactSet;
import com.novell.zos.grid.JobInfo;

public class JobSpec {
	 static public String REMOTE_DESKTOP_LUNCH_URL = "/remoteDesktop/Launch.po";
	    

	    /** Name of deployed job */
	    private JobInfo jobInfo;

	    public JobSpec(JobInfo jobInfo) {
	        this.jobInfo = jobInfo;
	    }

	    public String getName() {
	        return jobInfo.getName();
	    }
	    
	    public String getDisplayName() {
	        return jobInfo.getDisplayName();
	    }

	    public FactSet getParameters() {
	        return jobInfo.getParameters();
	    }

	    public String getDescription() {
	        return jobInfo.getDescription();
	    }
	    
	    public String getPortalExtUrl() {
	        if (getName().toLowerCase().indexOf("remotedesktop") != -1) {
	            return REMOTE_DESKTOP_LUNCH_URL;
	        } else {
	            return null;
	        }
	    }
}
